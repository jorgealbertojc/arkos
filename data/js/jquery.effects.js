// Efectos agregados a la pagina Web que  ayudan a jQuery

//Funcion para los avisos
$(document).ready(function(){
	setTimeout(function(){ $(".mensajes").fadeOut(2000);}, 8000);
});

//Initialize Arrow Side Menu:
ddaccordion.init({
	headerclass: "menuheaders", //Shared CSS class name of headers group
	contentclass: "menucontents", //Shared CSS class name of contents group
	revealtype: "clickgo", //Reveal content when user clicks or onmouseover the header? Valid value: "click", or "mouseover"
	mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
	collapseprev: true, //Collapse previous content (so only one open at any time)? true/false 
	defaultexpanded: [0], //index of content(s) open by default [index1, index2, etc]. [] denotes no content.
	onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
	animatedefault: false, //Should contents open by default be animated into view?
	persiststate: true, //persist state of opened contents within browser session?
	toggleclass: ["unselected", "selected"], //Two CSS classes to be applied to the header when it\'s collapsed and expanded, respectively ["class1", "class2"]
	togglehtml: ["none", "", ""], //Additional HTML added to the header when it\'s collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
	animatespeed: 800, //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
	oninit:function(expandedindices){ //custom code to run when headers have initalized
		//do nothing
	},
	onopenclose:function(header, index, state, isuseractivated){ //custom code to run whenever a header is opened or closed
		//do nothing
	}
});

$(document).ready(function () {
		//en tablas
		$('table#data-filter tbody tr').quicksearch({
			position: 'before',
			attached: 'table#data-filter',
			stripeRowClass: ['odd', 'even'],
			labelText: '<span style="margin-right: 10px; font-size: 10px; font-weight: normal; display: inline-block;">Buscar en la tabla:</span> ',
			inputClass: 'input-down" style="width: 200px;',
			labelClass: 'label-class',
			formId: 'form-id',
			loaderText: '',
			delay: 100//,
			//loaderImg: '../images/preload.gif" style="margin-top: 5px; height: 10px; width: 10px;'
		});
		$('ul#list_example li').quicksearch({
			position: 'before',
			attached: 'ul#list_example',
			loaderText: '',
			delay: 100
		});
		
		$('.mylinks a[title]').qtip({
			style: {
				border: {
					width: 0,
					radius: 4
				},
				padding: '3px 9px', 
				textAlign: 'left',
				tip: {
					corner: true,
					color: false,
					size: {
						width: 6,
						height: 3
					},
					opacity: 0
				},
				name: 'blue'
			},
			position: {
				corner: {
					tooltip: 'bottomRight',
					target: 'topMiddle'
				}
		   }
		});
});


$(function() {
        $('.show_gallery ul li #image_box a').lightBox();
});
