<?php
	
	//configuracion del index
	$index = substr($_SERVER['PHP_SELF'],0,strrpos($_SERVER['PHP_SELF'],'/'));
	$index = substr($index,0,strrpos($index,'/')) . '/index/';
	define(INDEX,$index);
	
	// configuracion del index de la parte admin
	$index_admin = substr($_SERVER['PHP_SELF'],0,strrpos($_SERVER['PHP_SELF'],'/'));
	$index_admin = substr($index_admin,0,strrpos($index_admin,'/')) . '/admin/';
	define(INDEX_ADMIN,$index_admin);
	
	
	// configuracion del index del sitio
	$site_index = substr($_SERVER['PHP_SELF'],0,strrpos($_SERVER['PHP_SELF'],'/'));
	$site_index = substr($site_index,0,strrpos($site_index,'/')) . '/';
	define(SITE_INDEX,$site_index);

	// configuracion de mysql
	$mysql_config=array(
						'mysql-server'=>'localhost',
						'mysql-user'=>'',
						'mysql-passwd'=>'',
						'mysql-db'=>'arkos'
						);
						
	
	// Definicion de los caracteres especiales
	$car_esp=array("�","�","�","�","�","�","�","�","�","�","�","�","<",">",'"',"�","!","�","?","�","'","\'","�","�",'|');
	$car_hex=array("&#225;","&#233;","&#237;","&#243;","&#250;","&#193;","&#201;","&#205;","&#211;","&#218;","&#241;","&#209;","&#60;","&#62;","&#34;","&#176;","&#33;","&#161;","&#63;","&#191;","&#39;","&#39;","&#252;","&#220;",'&#124;');
	
	// Definicion de caracteres con acento
	$acent = array("�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�",'|','(',')','$');
	$no_acent = array('&#225;','&#233;','&#237;','&#243;','&#250;','&#193;','&#201;','&#205;','&#211;','&#218;','&#241;','&#209;','&#252;','&#220;','&#161;','&#191;','&#176;','&#124;','&#40;','&#41;','&#36;');
	
	//Configuracion del menu principal
	define(GALLERY,'?option=gallery');
	define(HOME,'');
	define(PORTFOLIO,'');
	define(CONTACT,'?option=coact');
	define(SITE_MAP,'');

?>
