<?php

	$max_per_file = ini_get('upload_max_filesize');
	$max_upload_filesize = (float )substr(strtolower($max_per_file),0,-1) * 1024;
	if ( $_POST['upload'] == true ){
		$sql_upload_dir = 'SELECT gallery_dir FROM web_gallery WHERE gallery_id=' . $_POST['gallery_id'] . ' LIMIT 1';
		$res_upload_dir = exeQuery($sql_upload_dir);
		if ( mysql_num_rows($res_upload_dir) > 0 ){
			$target = mysql_fetch_array($res_upload_dir);
			$target_dir = $target['gallery_dir'] . '/';
		}
		else{
			$target_dir = '../gallery/images/';
		}
		$j = 0;
		for ( $i = 0 ; $i < 5 ; $i++ ){
			if ( $_FILES['use_settings']['name'][$i] != '' ){
				$file_name = $_FILES['use_settings']['name'][$i];
				$file_type = $_FILES['use_settings']['type'][$i];
				$file_tmp = $_FILES['use_settings']['tmp_name'][$i];
				$file_size = $_FILES['use_settings']['size'][$i];
				if ( ($file_size/1024) < $max_upload_filesize){
					if ( $file_type == 'image/gif' || $file_type == 'image/jpeg' || $file_type == 'image/png' || $file_type == 'image/tiff' ){
						if ( move_uploaded_file($file_tmp,$target_dir . strtolower($file_name) ) ){
							$sql_save_image = 'INSERT INTO web_image_gallery VALUES(
																				NULL,
																				' . $_SESSION['session_user_id'] . ',
																				' . $_POST['gallery_id']  . ',
																				"' . $target_dir . strtolower($file_name) . '",
																				"",
																				NULL
																				);';
							exeQuery($sql_save_image);
							$j++;
						}
					}
				}
			}
		}
		echo '<span class="type-info">Uploadig images, please wait...</span>';
		echo '<script type="text/javascript">window.location.href=\'' . INDEX_ADMIN . '?action=optImages&page=gallery&site=upload&adv=1&type=gallery&opt=upImage&res=1&files=' . $j . '&idGallery=' . $_POST['gallery_id'] . '\';</script>';
	}
	echo '<h2 class="admin">Select the gallery for the upload</h2>';
	$sql_galleries = 'SELECT gallery_id,gallery_name,gallery_published FROM web_gallery';
	$res_galleries = exeQuery($sql_galleries);
	if ( mysql_num_rows($res_galleries) > 0 ){
		echo '<form method="post" action="' . INDEX_ADMIN . '?action=optImages&page=gallery&site=upload" name="upload_images"  id="main_form" enctype="multipart/form-data">';
			echo '<select name="gallery_id" class="input-text" style="width: 250px;">';
				while ( $galleries = mysql_fetch_array($res_galleries) ){
					echo '<option value="' . $galleries['gallery_id'] . '" ';
						if ( $_GET['idGallery'] == $galleries['gallery_id'] )
							echo 'selected="selected"';
					echo '>';
						echo $galleries['gallery_name'];
					echo '</option>';
				}
			echo '</select>';
			
			// Beg: upload_images
			echo '<span class="add-category" style="padding: 5px; margin-bottom: 10px; float: left;">';
			echo '<input type="FILE" name="use_settings" />';
			echo '<span style="display: block;"><button type="submit" name="upload" class="submit-button" value="true"><span class="in-submit-left"><span class="in-submit-right">Upload images</span></span></button></span>';
			echo '</span>';
			// End: upload_images
		echo '</form>';
	}
	else{
		echo '<span class="type-error">No galleries found to upload images, first create one gallery before upload images.</span>';
	}
	
?>
