<?php

	//Beg: traza
	echo '<span class="index-content">';
		echo '<div id="traza">';
			echo '<a href="' . INDEX_ADMIN . '" class="item">Dashboard</a>';
			echo '<span class="middle"></span>';
			echo '<a href="' . INDEX_ADMIN . '?action=optRoll" class="item">Blog Roll</a>';
			echo '<span class="right"></span>';
			echo '</div>';
			// Advertices function
			admin_advertices();
	echo '</span>';
	
	//End: traza

	echo '<span class="index-content">';
	// Beg: PHP executions
		if ( $_POST['add_roll'] == true ){
			if ( strlen($_POST['roll_name']) < 2 || strlen($_POST['roll_description']) < 5 || $_POST['roll_url'] == 'http://' ){
				echo '<span class="type-error"><ul>';
					if ( strlen($_POST['roll_name']) < 2 )
						echo '<li>The Roll Title is very short.</li>';
					if ( strlen($_POST['roll_description']) < 5 )
						echo '<li>The Roll Description is very short.</li>';
					if ( $_POST['roll_url'] == 'http://' )
						echo '<li>Please put the url.</li>';
				echo '</ul></span>';
			}
			else{
				if ( strlen($_POST['roll_name']) >= 2 && strlen($_POST['roll_description']) >= 5 && $_POST['roll_url'] != 'http://' ){
					if ( substr($_POST['roll_url'],0,7) == 'http://' )
						$roll_user_url = $_POST['roll_url'];
					else
						$roll_user_url = 'http://' . $_POST['roll_url'];
						
					$car_del = array('<','>');
					$sql_save_roll = 'INSERT INTO web_blog_roll VALUES(
																	NULL,
																	' . $_SESSION['session_user_id'] . ',
																	"' . str_replace($car_esp,$car_hex,$_POST['roll_name']) . '",
																	"' . str_replace($car_esp,$car_hex,$_POST['roll_description']) . '",
																	"' . str_replace($car_del,'_',$roll_user_url) . '",
																	NULL,
																	1
																	);';
					exeQuery($sql_save_roll);
					echo '<span class="type-info">Please wait...</span>';
					echo '<script type="text/javascript">window.location.href=\'' . INDEX_ADMIN . '?action=optRoll&adv=1&type=roll&opt=published&res=1\'</script>';
				}
			}
		}
	// End: PHP executions
	echo '</span>';
	
	
	// Beg: actions
	echo '<span class="index-content">';
		echo 'Actions : ';
		echo '<a href="#mb_add_blog_roll" rel="lightbox[500 400]" class="admin-account"><span class="actions add-blog-roll">Add Blog Roll</span></a>';
	echo '</span>';
	
	
	echo '<div id="mb_add_blog_roll" style="display: none;">';
		echo '<span class="add-category">';
			echo '<h2 class="admin">Add Blog-Roll</h2>';
			echo '<form method="post" action="' . INDEX_ADMIN . '?action=optRoll" name="add_blog_roll">';
				echo '<table cellpadding="0" cellspacing="0" border="0">';
					// Name
					echo '<tr>';
						echo '<td><span style="display: block; width: 130px;">Title</span></td>';
						echo '<td><span style="display: block; width: 315px;"><input type="text" class="input-text" style="width: 315px;" name="roll_name" ';
							if ( $_POST['add_roll'] == true )
								echo 'value="' . $_POST['roll_name'] . '"';
						echo ' /></span></td>';
					echo '</tr>';
					echo '<tr><td colspan="2" style="height: 10px;"></td></tr>';
					// URL
					echo '<tr>';
						echo '<td><span style="display: block; width: 130px;">URL</span></td>';
						echo '<td><span style="display: block; width: 315px;"><input type="text" class="input-text" style="width: 315px;" name="roll_url" ';
							if ( $_POST['add_roll'] == true )
								echo 'value="' . $_POST['roll_url'] . '"';
							else
								echo 'value="http://"';
						echo ' /></span></td>';
					echo '</tr>';
					echo '<tr><td colspan="2" style="height: 10px;"></td></tr>';
					// Description
					echo '<tr>';
						echo '<td valign="top"><span style="display: block; width: 130px;">Description</span></td>';
						echo '<td><span style="display: block; width: 315px;"><textarea class="input-text" style="width: 315px; height: 200px;" name="roll_description">';
							if ( $_POST['add_roll'] == true )
								echo $_POST['roll_description'];
						echo '</textarea></span></td>';
					echo '</tr>';
					echo '<tr><td colspan="2" style="height: 10px;"></td></tr>';
					// Actions of the form
					echo '<tr><td colspan="2" align="right">';
						echo '<button type="submit" name="add_roll" value="true" class="submit-button"><span class="in-submit-left"><span class="in-submit-right">Save Blog-Roll</span></span></button>';
						echo '<button type="reset" name="add_roll" value="false" class="submit-button"><span class="in-submit-left"><span class="in-submit-right">Reset</span></span></button>';
						echo '<a href="javascript:void(0);" onClick="Mediabox.close();" class="submit-button"><span class="in-submit-left"><span class="in-submit-right">Cancel</span></span></button>';
					echo '</td></tr>';
				echo '</table>';
			echo '</form>';
		echo '</span>';
	echo '</div>';
	
	// End: actions
	
	echo '<span class="index-content">';
		echo '<table cellpadding="0" cellspacing="0" border="0" class="blog-entry">';
			echo '<thead>';
				echo '<tr>';
					echo '<td><span style="display: block; width: 30px;">&nbsp;</span></td>';
					echo '<td><span style="display: block; width: 500px;">Name/Description</span></td>';
					echo '<td><span style="display: block; width: 100px;">Autor</span></td>';
					echo '<td><span style="display: block; width: 150px;">Date</span></td>';
					echo '<td align="center"><span style="display: block; width: 34px;"><img src="../images/icn/admin-16-general-public.png" title="public/private" /></span></td>';
					echo '<td colspan="2"><span style="display: block; width: 150px;">Actions</span></td>';
				echo '</tr>';
			echo '</thead>';
			echo '<tbody>';
			
			$sql_all_blog_roll = 'SELECT * FROM web_blog_roll ORDER BY roll_date_created DESC';
			$res_all_blog_roll = exeQuery($sql_all_blog_roll);
			if ( mysql_num_rows($res_all_blog_roll) > 0 ){
				$class_toggle = 0;
				while ( $all_blog_roll = mysql_fetch_array($res_all_blog_roll) ){
					echo '<tr class="';
						if ( $class_toggle == 0 ){
							$class_toggle = 1;
							echo 'on';
						}
						else{
							if ( $class_toggle == 1 ){
								$class_toggle = 0;
								echo 'off';
							}
						}
					echo '">';
					// id roll
					echo '<td align="center"><span style="display: block; width: 30px;">' . $all_blog_roll['roll_id'] . '</span></td>';
					// Title/Description roll
					echo '<td><span style="display: block; width: 500px;">';
						echo '<a href="' . $all_blog_roll['roll_url'] . '" style="display: block; margin-top: 10px;" target="_blank">' . $all_blog_roll['roll_title'] . '</a>';
						echo '<span class="roll-description">' . nl2br($all_blog_roll['roll_description']) . '</span>';
					echo '</span></td>';
					// User roll
					echo '<td><span style="display: block; width: 100px;">' . user_id($all_blog_roll['roll_user']) . '</span></td>';
					// date roll
					echo '<td><span style="display: block; width: 150px;">';
						echo substr(date_month($all_blog_roll['roll_date_created']),0,3) . ' ' . date_day($all_blog_roll['roll_date_created']) . ' &#124; ' . date_year($all_blog_roll['roll_date_created']);
					echo '</span></td>';
					// Public/Private roll
					echo '<td align="center"><span style="display: block; width: 34px;"><a href="' . INDEX_ADMIN . 'publish-blog-roll.php?action=changePubMode&idBlogRoll=' . $all_blog_roll['roll_id'] . '"><img src="../images/icn/';
						if ( $all_blog_roll['roll_published'] ){
							echo 'admin-16-green-ball.png';
							$title = 'public';
						}
						else{
							echo 'admin-16-red-ball.png';
							$title = 'private';
						}
					echo '" title="' . $title . '" style="border: 0px;" /></a></span></td>';
					echo '<td align="center"><span style="display: block; width:60px;"><a href="javascript:void(0);"><img src="../images/icn/admin-16-edit-icn.png" title="Edit blog roll" style="border: 0px;" /></a></span></td>';
					echo '<td align="center"><span style="display: block; width:60px;"><a href="javascript:void(0);"><img src="../images/icn/admin-16-cross.png" title="Edit blog roll" style="border: 0px;" /></a></span></td>';
					echo '</tr>';
				}
			}
			else
				echo '<tr class="off"><td colspan="7" align="center">No Blog Roll found.</td></tr>';
			
			echo '</tbody>';
		echo '</table>';
	echo '</span>';
?>
