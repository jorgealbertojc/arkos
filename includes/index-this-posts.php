<?php


// Entrada especifica

$comments = 0;
if ( isset($_GET['post_id']) && $_GET['post_id']!='' && $_GET['post_id'] > 0 ){
	$sql_posts = 'SELECT * FROM web_posts WHERE post_id=' . $_GET['post_id'] . ' LIMIT 1';
	$res_posts = exeQuery($sql_posts);
	if ( mysql_num_rows($res_posts) > 0 ){
		$comments = 1;
		$posts = mysql_fetch_array($res_posts);
		echo '<span class="post-title">' . $posts['post_title'] . '</span>';
		echo '<span class="post-info">';
			$sql_category = 'SELECT category_name FROM web_categories WHERE category_id=' . $posts['post_category'] . ' LIMIT 1';
			$res_category = exeQuery($sql_category);
			if ( mysql_num_rows($res_category) > 0 ){
				$category = mysql_fetch_array($res_category);
				echo $category['category_name'];
			}
			else
				echo 'Sin categoria';												
		echo ' &#124; Publicado por <b>' . user_id($posts['post_user']) . '</b> en ' . date_month($posts['post_date_created']) . ' ' . date_day($posts['post_date_created']) . ' de ' . date_year($posts['post_date_created']) . ' a las ' . date_hour($posts['post_date_created']) . 'hrs</span>';
		echo '<span class="post-content">' . str_replace('<div style="page-break-after: always;"><span style="display: none;">&nbsp;</span></div>','',$posts['post_content']) . '</span>';
	}
	else{
		echo '<span class="type-info post-title">No hay posts</span>';
	}
}
else{
	echo '<span class="type-info post-title">No hay post</span>';
}

if ( $comments ){
	echo '<a name="comments-index"></a>';
	
	$sql_show_comments = 'SELECT * FROM web_comments WHERE com_post=' . $posts['post_id'] . ' ORDEr BY com_id DESC';
	$res_show_comments = exeQuery($sql_show_comments);
	if ( mysql_num_rows($res_show_comments) > 0 ){
		$class = 0;
		echo '<h2 class="comments-in">Comentarios</h2>';
		echo '<span class="comments">';
		while ( $show_comments = mysql_fetch_array($res_show_comments) ){
			if ( $class == 0 ){
				$class_type = 'on';
				$class = 1;
			}
			else{
				if ( $class == 1 ){
					$class_type = 'off';
					$class = 0;
				}
			}
			echo '<span class="' . $class_type . '">';
				echo '<a name="comment-id=' . $show_comments['com_id'] . '"></a>';
				echo '<table cellpadding="0" cellspacing="0" border="0">';
					echo '<tr>';
						$grv_mail = $show_comments['com_email'];
						//$grv_init = '../images/img/spacer.gif';
						$grv_size = 40;
						
						$grv_http = 'http://www.gravatar.com/avatar.php';
						$grv_http .= '?gravatar_id=' . md5($grv_mail);
						//$grv_http .= '&default=' . urlencode($grv_init);
						$grv_http .= '&size=' . $grv_size;
						 
						 $no_br=array('\r\n','\r','\n');
						echo '<td valign="top" align="center" style="width: 62px;"><img src="' . $grv_http . '" style="width: 40px; height: 40px; border: 1px solid #abc; margin-right: 10px; padding: 1px;" height="40" width="40" /></td>';
						echo '<td>';
							echo '<span class="com-date">Publicado en ' . date_month($show_comments['com_date_created']) . ' ' . date_day($show_comments['com_date_created']) . ' de ' . date_year($show_comments['com_date_created']) . ' a las ' . date_hour($show_comments['com_date_created']) . '</span>';
							echo '<span class="com-autor">' . $show_comments['com_autor'] . '</span>';
							echo '<span class="com-comment">' . str_replace($no_br,'<br />',nl2br($show_comments['com_comment'])) . '</span>';
						echo '</td>';
					echo '</tr>';
				echo '</table>';
			echo '</span>';
			
		}
		
		
		
		echo '</span>';
	}
	//Formulario de comentarios
	echo '<span class="comments-form">';
	echo '<h2>Deja tu comentario</h2>';
	if ( $_POST['comment_submit'] ){
		if ( (strlen(str_replace(' ','',$_POST['comment_user'])) < 2) || (!is_email($_POST['comment_email'])) || (strlen(str_replace(' ','',$_POST['comment_content'])) < 5) ){
			echo '<div class="type-error mensajes"><ul style="list-style-type: none;">';
			
				if ( strlen(str_replace(' ','',$_POST['comment_user'])) < 2 )
					echo '<li>Debes dar un nombre de usuario valido.</li>';
					
				if (!is_email($_POST['comment_email']))
					echo '<li>Debes dar una direccion de correo valida.</li>';
					
				if (strlen(str_replace(' ','',$_POST['comment_content'])) < 5)
					echo '<li>Debes escribir un mensaje.</li>';
					
			echo '</ul></div>';
		}
		else{
			if ( (strlen(str_replace(' ','',$_POST['comment_user'])) >= 2) && (is_email($_POST['comment_email'])) || (strlen(str_replace(' ','',$_POST['comment_content'])) >= 5) ){
				$sql_insert_comment = 'INSERT INTO web_comments VALUES(
																	NULL,
																	' . $_POST['comment_post'] . ',
																	"' . str_replace($car_esp,$car_hex,$_POST['comment_user']) . '",
																	"' . str_replace($car_esp,$car_hex,$_POST['comment_content']) . '",
																	"' . $_POST['comment_email'] . '",
																	"' . getRealIp() . '",
																	NULL
																	);';
				$res_insert_comment = exeQuery($sql_insert_comment);
				$subject = $_POST['comment_user'] . ' | New comment';
				$headers = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";
				$headers .= 'From: "Web-Blog ' . str_replace($car_hex,$car_esp,SITE_NAME) . '" <no-reply@' . $_SERVER['SERVER_NAME'] . '>' . "\r\n";
				$headers .= 'Reply-To: "Web-Blog ' . str_replace($car_hex,$car_esp,SITE_NAME) . '" <no-reply@' . $_SERVER['SERVER_NAME'] . '>' . "\r\n";
				
				$message = '
<html>
	<body>
		<div align="center">
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td bgcolor="#000000" background="http://' . $_SERVER['SERVER_NAME'] . '/images/bgs/admin-bg-nav-bar.png"><span style="display: block; width: 700px; height: 40px; padding-top: 10px; padding-bottom: 10px;"><img src="http://' . $_SERVER['SERVER_NAME'] . '/images/bnr/admin-mail-title.png" /></span></td>
				</tr>
				<tr>
					<td>
						<span style="display: block; width: 700px; background-color: #fff;"><span style="display: block; padding: 20px; border-left: 1px solid #f4f4f4; border-right: 1px solid #f4f4f4; font-family: Lucida Sans Unicode, Trebuchet MS, Arial, Sans-serif; font-size: 11px; color: #345;">
							Dear <b>' . $_POST['comment_user'] . '</b>.<br /><br />
							You left the following comment on Web-Blog ' . SITE_NAME . ':
							<span style="display: block; margin-top: 10px; margin-bottom: 10px; padding: 20px; background-color: #fafafa; border: 1px solid #eaeaea;">
								' . nl2br($_POST['comment_content']) . '
							</span>
							<span style="display: block; margin-top: 20px; padding-top: 10px; border-top: 1px solid #f0f0f0; font-family: Trebuchet MS, Arial, Sans-serif; font-size: 10px; text-align: center;">
								<a href="http://' . $_SERVER['SERVER_NAME'] . '/index/" style="text-decoration: none"><span style="color: #9ab;">Index / Home</span></a>
								&nbsp; &#124; &nbsp; <a href="http://' . $_SERVER['SERVER_NAME'] . '/index/?option=gallery" style="text-decoration: none"><span style="color: #9ab;">Galleria</span></a>
								&nbsp; &#124; &nbsp; <a href="http://' . $_SERVER['SERVER_NAME'] . '/index/" style="text-decoration: none"><span style="color: #9ab;">Portafolio</span></a>
								&nbsp; &#124; &nbsp; <a href="http://' . $_SERVER['SERVER_NAME'] . '/index/?option=contact" style="text-decoration: none"><span style="color: #9ab;">Contacto</span></a>
								&nbsp; &#124; &nbsp; <a href="http://' . $_SERVER['SERVER_NAME'] . '" style="text-decoration: none"><span style="color: #9ab;">Web Development CMS</span></a>
								&nbsp; &#124; &nbsp; <a href="http://' . $_SERVER['SERVER_NAME'] . '" style="text-decoration: none"><span style="color: #9ab;">LEAF Professionals</span></a>
							</span>
						</span></span>
					</td>
				</tr>
				<tr>
					<td bgcolor="#304050"><span style="display: block; width: 700px; height: 20px;"></span></td>
				</tr>
			</table>
			<span style=" display: block; width: 700px;font-family: Trebuchet MS, Arial, Sans-serif; font-size: 10px; color: #89a; text-align: center; margin-top: 30px;">
				You are receiving this message from ' . SITE_NAME . ', because you are one of our valued members, or you left a comment. ' . SITE_NAME . ' respects your privacy.
			</span>
		</div>
	</body>
</html>';
				$message_to_owner = '
<html>
	<body>
		<div align="center">
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td bgcolor="#000000" background="http://' . $_SERVER['SERVER_NAME'] . '/images/bgs/admin-bg-nav-bar.png"><span style="display: block; width: 700px; height: 40px; padding-top: 10px; padding-bottom: 10px;"><img src="http://' . $_SERVER['SERVER_NAME'] . '/images/bnr/admin-mail-title.png" /></span></td>
				</tr>
				<tr>
					<td>
						<span style="display: block; width: 700px; background-color: #fff;"><span style="display: block; padding: 20px; border-left: 1px solid #f4f4f4; border-right: 1px solid #f4f4f4; font-family: Lucida Sans Unicode, Trebuchet MS, Arial, Sans-serif; font-size: 11px; color: #345;">
							' . str_replace($car_esp,$car_hex,$_POST['comment_user']) . ' has left the following comment on Web-Blog ' . SITE_NAME . ':
							<span style="display: block; margin-top: 10px; margin-bottom: 10px; padding: 20px; background-color: #fafafa; border: 1px solid #eaeaea;">
								' . nl2br($_POST['comment_content']) . '
							</span>
							<span style="display: block; margin-top: 20px; padding-top: 10px; border-top: 1px solid #f0f0f0; font-family: Trebuchet MS, Arial, Sans-serif; font-size: 10px; text-align: center;">
								<a href="http://' . $_SERVER['SERVER_NAME'] . '/index/" style="text-decoration: none"><span style="color: #9ab;">Index / Home</span></a>
								&nbsp; &#124; &nbsp; <a href="http://' . $_SERVER['SERVER_NAME'] . '/index/?option=gallery" style="text-decoration: none"><span style="color: #9ab;">Galleria</span></a>
								&nbsp; &#124; &nbsp; <a href="http://' . $_SERVER['SERVER_NAME'] . '/index/" style="text-decoration: none"><span style="color: #9ab;">Portafolio</span></a>
								&nbsp; &#124; &nbsp; <a href="http://' . $_SERVER['SERVER_NAME'] . '/index/?option=contact" style="text-decoration: none"><span style="color: #9ab;">Contacto</span></a>
								&nbsp; &#124; &nbsp; <a href="http://' . $_SERVER['SERVER_NAME'] . '" style="text-decoration: none"><span style="color: #9ab;">Web Development CMS</span></a>
								&nbsp; &#124; &nbsp; <a href="http://' . $_SERVER['SERVER_NAME'] . '" style="text-decoration: none"><span style="color: #9ab;">LEAF Professionals</span></a>
							</span>
						</span></span>
					</td>
				</tr>
				<tr>
					<td bgcolor="#304050"><span style="display: block; width: 700px; height: 20px;"></span></td>
				</tr>
			</table>
			<span style=" display: block; width: 700px;font-family: Trebuchet MS, Arial, Sans-serif; font-size: 10px; color: #89a; text-align: center; margin-top: 30px;">
				You are receiving this message from ' . SITE_NAME . ', because you are one of our valued members, or you left a comment. ' . SITE_NAME . ' respects your privacy.
			</span>
		</div>
	</body>
</html>';
				
				
				
				/*
				$mensaje = '<html>
								<head>
									<title>Mensaje</title>
								</head>
							<body>
								<b>' . str_replace($car_esp,$car_hex,$_POST['comment_user']) . '</b> ha dejado el siguiente mensaje en el Blog.<br />
								' . $_POST['comment_content'] . '<br />
								Hora de envio : ' . date('r') . '<br />
								Enviale una respuesta a su correo : ' . $_POST['comment_email'] . '
							</body>
							</html>';
				*/
				if ( !@mail($_POST['comment_email'],$subject,$message,$headers) ){
					echo '<div class="type-error mensajes">no se ha podido enviar la notificacion a Arkos Noem Arenom</div>';
				}
				@mail(SITE_ADMIN_EMAIL,$subject,$message_to_owner,$headers);
				echo '<div class="type-info mensajes">Publicando comentario... Por favor espera un momento</div>';
				echo '<meta http-equiv="refresh" content="0;url=' . INDEX . '?option=index&call=show_post&post_id=' . $_POST['comment_post'] . '&adv=1&type=comment&published=1" />';
			}
		}
	}
	echo '<form method="post" action="' . INDEX . '?option=index&call=show_post&post_id=' . $posts['post_id'] . '#comments-index" name="comments_form" class="niceform">';
		echo '<input type="hidden" name="comment_post" value="' . $_GET['post_id'] . '" />';
		echo '<table cellpadding="0" cellspacing="0" border="0">';
			echo '<tr>';
				echo '<td align="right" valign="top" style="width: 60px;">Nombre :</td>';
				echo '<td style="width:10px;"></td>';
				echo '<td style="width: 480px"><input type="text" style="width: 320px;" name="comment_user"';
					if ( $_POST['comment_submit'] )
						echo ' value="' . $_POST['comment_user'] . '"';
				echo ' /><span class="form-info">(Obligatorio)</span></td>';
			echo '</tr>';
			echo '<tr><td style="height: 10px;" colspan="3"></td></tr>';
			echo '<tr>';
				echo '<td align="right" valign="top" style="width: 60px;">Correo :</td>';
				echo '<td style="width:10px;"></td>';
				echo '<td style="width: 480px"><input type="text" style="width: 320px;" name="comment_email"';
					if ( $_POST['comment_submit'] )
						echo ' value="' . $_POST['comment_email'] . '"';
				echo ' /><span class="form-info">(Obligatorio, no se publicara)</span></td>';
			echo '</tr>';
			echo '<tr><td style="height: 10px;" colspan="3"></td></tr>';
			echo '<tr>';
				echo '<td align="right" valign="top" style="width: 60px;">Mensaje :</td>';
				echo '<td style="width:10px;"></td>';
				echo '<td style="width: 480px"><textarea style="width: 480px; height: 150px;" rows="15" cols="101" name="comment_content">';
					if ( $_POST['comment_submit'] )
						echo $_POST['comment_content'];
				echo '</textarea></td>';
			echo '</tr>';
			echo '<tr><td style="height: 10px;" colspan="3"></td></tr>';
			echo '<tr><td colspan="3" align="right"><button type="submit" name="comment_submit" value="1">Publicar comentario</button> <button type="reset">Borrar todo</button></td></tr>';
		echo '</table>';
	//echo '<input type="text" /><br />';
	//echo '<textarea></textarea>';
	echo '</form>';
	echo '</span>';
	
	//Fin formulario de comentarios
}
// Fin de entrada especifica
?>
