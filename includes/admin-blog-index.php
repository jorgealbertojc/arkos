<?php
	echo '<span class="index-content">';
		echo '<div id="traza">';
			echo '<a href="' . INDEX_ADMIN . '" class="item">Dashboard</a>';
			echo '<span class="middle"></span>';
			echo '<a href="' . INDEX_ADMIN . '?action=optBlog&page=index" class="item">Blog</a>';
			if ( isset($_GET['page']) && $_GET['page'] != '' ){
				switch($_GET['page']){
					case 'index':
						$b_link = 'index';
						$b_title = 'Edit Posts';
						break;
					case 'addPost':
						$b_link = 'addPost';
						$b_title = 'Add Post';
						break;
					case 'category':
						$b_link = 'category';
						$b_title = 'Category';
						break;
					default:
						$b_link = 'index';
						$b_title = 'Edit Posts';
				}
				echo '<span class="middle"></span>';
				echo '<a href="' . INDEX_ADMIN . '?action=optBlog&page=' . $b_link . '" class="item">' . $b_title . '</a>';
				echo '<span class="right"></span>';
			}
			else{
				echo '<span class="right"></span>';
			}
			echo '</div>';
			// Advertices function
			admin_advertices();
	echo '</span>';
	
	
	echo '<span class="page-actions">';
		echo '<a href="' . INDEX_ADMIN . '?action=optBlog&page=index"';
			if ( $_GET['page'] == 'index' )
				echo ' class="this"';
		echo '><span class="menu edit-posts">Edit Post</span></a>';
		echo '<a href="' . INDEX_ADMIN . '?action=optBlog&page=addPost"';
			if ( $_GET['page'] == 'addPost' )
				echo ' class="this"';
		echo '><span class="menu add-posts">Add New Post</span></a>';
		echo '<a href="' . INDEX_ADMIN . '?action=optBlog&page=category"';
			if ( $_GET['page'] == 'category' )
				echo ' class="this"';
		echo '><span class="menu category-posts">Categories</span></a>';
	echo '</span>';
	echo '<span class="index-content">';
	
		switch($_GET['page']){
			case 'editPost':
				include_once 'admin-blog-edit-this-post.php';
				break;
			case 'category':
				include_once 'admin-blog-category.php';
				break;
			case 'addPost':
				include_once 'admin-blog-create-post.php';
				break;
			default:
				include_once 'admin-blog-edit-post.php';
		}
		
	echo '</span>';
?>
