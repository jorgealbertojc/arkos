<?php
	echo '<table cellpadding="0" cellspacing="0" border="0">';
		echo '<tr>';
			echo '<td valign="top" rowspan="3">';
				echo '<div id="main-content">';
					if ( $_POST['submit_button'] ){
						if ( (strlen($_POST['mess_name']) < 3) || !is_email($_POST['mess_email']) || (strlen($_POST['mess_subject']) < 3) || (strlen($_POST['mess_message']) < 3) ){
							echo '<div class="type-error mensajes"><ul style="list-style: none;">';
							if (strlen($_POST['mess_name']) < 3)
								echo '<li>Debes dar un nombre valido.</li>';
							
							if ( !is_email($_POST['mess_email']) )
								echo '<li>Debes dar una direcci&oacute;n de correo valida.</li>';
							
							if (strlen($_POST['mess_subject']) < 3)
								echo '<li>Debes decir el asunto de tu mensaje.</li>';
							
							if (strlen($_POST['mess_message']) < 3)
								echo '<li>Debes escribir un mensaje mas largo.</li>';
							echo '</ul></div>';
						}
						else{
							if ( (strlen($_POST['mess_name']) >= 3) && is_email($_POST['mess_email']) && (strlen($_POST['mess_subject']) >= 3) && (strlen($_POST['mess_message']) >= 3) ){
								$to = '"Arkos Noem Arenom" <arkos.noem.arenom@gmail.com';
								$subject = 'Contacto Web | ' . $_POST['mess_subject'];
								$headers = "From: \"Arkos Noem Arenom\" <arkos.noem.arenom@gmail.com>\r\n";
								$headers .= "Reply-To: ". strip_tags($_POST['mess_email']) . "\r\n";
								//$headers .= "CC: chris@css-tricks.com\r\n";
								$headers .= "MIME-Version: 1.0\r\n";
								$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
								
								$message = '<html><body>';
								$message .= '<h1>Mensaje enviado en : ' . date('r') . '</h1>';
								$message .= 'El mensaje es :<br />';
								$message .= nl2br($_POST['mess_message']);
								$message .= '<br /><b>Datos</b><br />';
								$message .= 'Autor : ' . $_POST['mess_name'] . '<br />';
								$message .= 'Correo : ' . $_POST['mess_email'];
								if ( strlen($_POST['mess_url']) > 7 )
									$message .= 'URL : ' . $_POST['mess_url'] . '<br />';
								$message .= '</body></html>';
								if ( @mail($to,$subject,$message,$headers) ){
									echo '<meta http-equiv="refresh" content="0;url=' . INDEX . CONTACT . '&adv=1&type=contact&send=1" />';
								}
								else{
									echo '<meta http-equiv="refresh" content="0;url=' . INDEX . CONTACT . '&adv=1&type=contact&send=0" />';
								}
							}
						}
					}
					echo '<form method="post" action="' . INDEX . CONTACT . '" name="send_message" class="niceform">';
						echo '<table cellpadding="0" cellspacing="0" border="0">';
							echo '<tr>';
								echo '<td align="right" style="width: 60px;" valign="top">Nombre : </td>';
								echo '<td style="width: 10px;"></td>';
								echo '<td><input type="text" name="mess_name" style="width: 300px;"';
									if ( $_POST['submit_button'] )
										echo ' value="' . $_POST['mess_name'] . '"';
								echo ' /><span class="form-info">Obligatorio</span></td>';
							echo '</tr>';
							echo '<tr><td colspan="3" style="height: 10px;"></td></tr>';
							echo '<tr>';
								echo '<td align="right" style="width: 60px;" valign="top">Correo : </td>';
								echo '<td style="width: 10px;"></td>';
								echo '<td><input type="text" name="mess_email" style="width: 300px;"';
									if ( $_POST['submit_button'] )
										echo ' value="' . $_POST['mess_email'] . '"';
								echo ' /><span class="form-info">Obligatorio</span></td>';
							echo '</tr>';
							echo '<tr><td colspan="3" style="height: 10px;"></td></tr>';
							echo '<tr>';
								echo '<td align="right" style="width: 60px;" valign="top">Asunto : </td>';
								echo '<td style="width: 10px;"></td>';
								echo '<td><input type="text" name="mess_subject" style="width: 300px;"';
									if ( $_POST['submit_button'] )
										echo ' value="' . $_POST['mess_subject'] . '"';
								echo ' /><span class="form-info">Obligatorio</span></td>';
							echo '</tr>';
							echo '<tr><td colspan="3" style="height: 10px;"></td></tr>';
							echo '<tr>';
								echo '<td align="right" style="width: 60px;" valign="top">URL : </td>';
								echo '<td style="width: 10px;"></td>';
								echo '<td><input type="text" name="mess_url" style="width: 300px;"';
									if ( $_POST['submit_button'] )
										echo ' value="' . $_POST['mess_url'] . '"';
								echo ' /></td>';
							echo '</tr>';
							echo '<tr><td colspan="3" style="height: 10px;"></td></tr>';
							echo '<tr>';
								echo '<td align="right" style="width: 60px;" valign="top">Mensaje : </td>';
								echo '<td style="width: 10px;"></td>';
								echo '<td><textarea name="mess_message" style="width: 480px; height: 300px;">';
									if ( $_POST['submit_button'] )
										echo $_POST['mess_message'];
								echo '</textarea></td>';
							echo '</tr>';
							echo '<tr><td colspan="3" style="height: 10px;"></td></tr>';
							echo '<tr><td colspan="3" align="right"><button type="submit" name="submit_button" value="1">Enviar mensaje</button><button type="reset">Reiniciar</button></td></tr>';
						echo '</table>';
					echo '</form>';
				echo '</div>';
			echo '</td>';
			echo '<td style="width: 10px; background-image: url(../images/bgs/bg-content-left-archives.png); background-repeat: repeat-y; background-position: 0px 0px;" valign="top"><img src="../images/bdr/bdr-top-margin.png" /></td>';
			// Celda central
			echo '<td valign="top" rowspan="3"><span class="center-content"><span class="write-space">';
				
					// Inicio comentarios
					$sql_comments_autors = 'SELECT com_autor,com_id,com_post,com_date_created FROM web_comments ORDEr BY com_id DESC LIMIT 15';
					$res_comments_autors = exeQuery($sql_comments_autors);
					if ( mysql_num_rows($res_comments_autors) > 0 ){
						echo '<span class="table-title"><span>Comentarios recientes</span></span>';
						echo '<span class="list-links">';
						while ( $comments_autors = mysql_fetch_array($res_comments_autors) ){
							echo '<a href="' . INDEX . '?option=index&call=show_post&post_id=' . $comments_autors['com_post'] . '#comment-id=' . $comments_autors['com_id'] . '">';
								echo '<span>' . substr(date_month($comments_autors['com_date_created']),0,3) . ' ' . date_day($comments_autors['com_date_created']) . ' &#124; </span>';
								echo substr($comments_autors['com_autor'],0,20);
								if ( strlen($comments_autors['com_autor']) > 20 ){
									echo '&#133;';
								}
							echo '</a>';
						}
						//echo '<a href="javascript: void(0);"><span>09 Feb 13 &#124; </span>Saul</a>';
						echo '</span>';
					}
					// Fin comentarios
					
					//inicio titulos de posts
					$sql_title_posts = 'SELECT post_id,post_title,post_date_created FROM web_posts ORDER BY post_id DESC LIMIT 15';
					$res_title_posts = exeQuery($sql_title_posts);
					if ( mysql_num_rows($res_title_posts) > 0 ){
						echo '<span class="table-title"><span>Posts recientes</span></span>';
						echo '<span class="list-links">';
							while ( $title_posts = mysql_fetch_array($res_title_posts) ){
								echo '<a href="' . INDEX . '?option=index&call=show_post&post_id=' . $title_posts['post_id'] . '">';
									echo '<span>' . substr(date_month($title_posts['post_date_created']),0,3) . ' ' . date_day($title_posts['post_date_created']) . ' &#124; </span>';
									echo substr($title_posts['post_title'],0,20);
									if ( strlen($title_posts['post_title']) > 20 ){
									echo '&#133;';
								}
								echo '</a>';
							}
						echo '</span>';
					}
					//fin titulos de post
					
					// Inicio archivos de Blog
						$sql_blog_archive = 'SELECT DISTINCT post_year_month FROM web_posts ORDER BY post_year_month DESC';
						$res_blog_archive = exeQuery($sql_blog_archive);
						if ( mysql_num_rows($res_blog_archive) > 0 ){
							// Inicio archivos de Blog
							echo '<span class="table-title"><span>Archivos de Blog</span></span>';
							echo '<span class="list-links">';
							while ( $blog_archive = mysql_fetch_array($res_blog_archive) ){
								echo '<div class="menuheaders"><a href="javascript:void(0);" title="' . date_year($blog_archive['post_year_month']) . ' &#124; ' . date_month($blog_archive['post_year_month']) . ' "><span>' . date_year($blog_archive['post_year_month']) . ' </span>' . date_month($blog_archive['post_year_month']) . '</a></div>';
								echo '<ul class="menucontents">';
									$sql_date_archives = 'SELECT post_id,post_title,post_date_created FROM web_posts WHERE post_year_month="' . $blog_archive['post_year_month'] . '" ORDER BY post_date_created DESC';
									$res_date_archives = exeQuery($sql_date_archives);
									while ( $date_archives = mysql_fetch_array($res_date_archives) ){
										echo '<li><a href="' . INDEX . '?option=index&call=show_post&post_id=' . $date_archives['post_id'] . '"><span>' . date_day($date_archives['post_date_created']) . ' &#124; </span>' . substr($date_archives['post_title'],0,20);
											if ( strlen($date_archives['post_title']) > 20 )
												echo '&#133;';
										echo '</a></li>';
									}
								echo '</ul>'; 
							}
							echo '<span>';
						}
					//Fin archivos de blog
					
			echo '</span></span></td>';
			echo '<td style="width: 10px; background-image: url(../images/bgs/bg-content-right-archives.png); background-repeat: repeat-y; background-position: 0px 0px;" valign="top"><img src="../images/bdr/bdr-top-margin.png" /></td>';
			echo '<td valign="top"><span class="right-content">';
			
			
				// Inicio categorias
				$sql_show_categories = 'SELECT category_id,category_name FROM web_categories ORDER BY category_name ASC';
				$res_show_categories = exeQuery($sql_show_categories);
				if ( mysql_num_rows($res_show_categories) > 0 ){
					echo '<span class="table-title"><span>Categorias</span></span>';
					echo '<span class="list-links">';
						while ( $show_categories = mysql_fetch_array($res_show_categories) ){
							echo '<a href="' . INDEX . '?option=index&call=category&category=' . $show_categories['category_id'] . '">';
								echo '<span>';
									$sql_num_this = 'SELECT post_id FROM web_posts WHERE post_category=' . $show_categories['category_id'] . '';
									$res_num_this = exeQuery($sql_num_this);
									if ( mysql_num_rows($res_num_this) < 10 )
										echo '0'.mysql_num_rows($res_num_this);
									else
										echo mysql_num_rows($res_num_this);
								echo ' &#124; </span>';
								echo $show_categories['category_name'];
							echo '</a>';
						}
					echo '</span>';
				}
				// Fin categorias
				
				//Acerca de mi
				/*echo '<span class="table-title"><span>Acerca de mi</span></span>';
				echo '<span class="about-me">';
					echo '<img src="../images/img/arkosnoemarenom.jpg" style="float: left; padding-right: 2px; margin-right: 5px; margin-bottom: 5px; border-right: 1px solid #f0f0f0;" />';
					echo '<b>Arkos Noem Arenom :</b><br />';
					echo 'Que tal, soy el Jorge alias el arkos, el por que de mi psedonimo es algo muy simple, perteneci a una banda de metal-gotico llamada Sadness Tears en la cual fui bautizado como el Arkos Noem Arenom.<br /><br />Despues de el desplante que tuve con la banda regrese a mis estudios dentro de la UAM, aunque no estoy muy agusto con ello. Estudio la carrera de Ingenier&iacute;a Electr&oacute;nica, area de concentraci&oacute;n computaci&oacute;n, actualmente soy participe de diversos <a href="#bottom">proyectos</a> de desarrollo web. Pero sigo en la carretera de la musica. Soy miembro activo del CEUAMI, aunque ya me queda poco tiempo con ellos.';
				echo '</span>';*/
				//Fin acerca de mi
				
				//Galerias recientes
				$sql_rec_gallery = 'SELECT gallery_id,gallery_name,gallery_date_created FROM web_gallery ORDER BY gallery_date_created DESC LIMIT 10';
				$res_rec_gallery = exeQuery($sql_rec_gallery);
				if ( mysql_num_rows($res_rec_gallery) > 0 ){
					echo '<span class="table-title"><span>Galerias recientes</span></span>';
					echo '<span class="list-links">';
					while ( $rec_gallery = mysql_fetch_array($res_rec_gallery) ){
						$sql_num_archives_gallery = 'SELECT * FROM web_image_gallery WHERE image_gallery=' . $rec_gallery['gallery_id'] . ';';
						$res_num_archives_gallery = exeQuery($sql_num_archives_gallery);
						echo '<a href="' . INDEX . '?option=gallery&call=show_gallery&gallery_id=' . $rec_gallery['gallery_id'] . '"><span>' . mysql_num_rows($res_num_archives_gallery) . ' &#124; </span>' . $rec_gallery['gallery_name'] . '</a>';
					}
					echo '</span>';
				}
				//Fin galerias recientes
				
				// Links de interes
				$sql_interest_links = 'SELECT roll_title,roll_url,roll_description FROM web_blog_roll ORDER BY roll_title ASC;';
				$res_interest_links = exeQuery($sql_interest_links);
				if ( mysql_num_rows($res_interest_links) > 0 ){
					echo '<span class="table-title"><span>Links de interes</span></span>';
					echo '<span class="list-links">';
						while ( $interest_links = mysql_fetch_array($res_interest_links) ){
							echo '<a href="' . $interest_links['roll_url'] . '" title="' . $interest_links['roll_description'] . '" target="_blank"><span>:: </span>' . $interest_links['roll_title'] . '</a>';
						}
					echo '</span>';
				}
				//Fin links de interes
			echo '</span></td>';
		echo '</tr>';
		echo '<tr>';
			echo '<td style="width: 10px; background-image: url(../images/bgs/bg-content-left-archives.png); background-repeat: repeat-y; background-position: 0px 0px;" valign="bottom"><img src="../images/bdr/bdr-bottom-margin.png" /></td>';
			echo '<td style="width: 10px; background-image: url(../images/bgs/bg-content-right-archives.png); background-repeat: repeat-y; background-position: 0px 0px;" valign="bottom"><img src="../images/bdr/bdr-bottom-margin.png" /></td>';
		echo '</tr>';
	echo '</table>';
		echo '</tr>';
	echo '</table>';
?>