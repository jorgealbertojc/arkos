<?php

	$dir_uploads = '../uploads/';
	$images_dir = $dir_uploads . 'image/';

	if ( isset($_GET['dir']) && $_GET['dir'] != '' && $_GET['dir'] != '/' )
		$directory = $_GET['dir'] . '/';
	else
		$directory = '';
	
	echo 'Actions :';
	//echo ' <a href="#mb_upload_image" rel="lightbox[inline 800 600]" title="Upload Images" class="uploadFile"><span>Upload new image</span></a>';
	echo ' <a href="javascript:void(0)" title="Create folder" onClick="create_folder(\'' . $directory . '\',\'' . $images_dir . '\')" class="addFolder"><span>Create folder</span></a>';
	// Beg: upload_images
		echo '<span class="index-content">';
		
		
		$max_per_file = ini_get('upload_max_filesize');
		$max_upload_filesize = (float )substr(strtolower($max_per_file),0,-1) * 1024;
		if ( $_POST['upload'] == true ){
			$target_dir = $images_dir . $directory;
			for ( $i = 0 ; $i < 5 ; $i++ ){
				if ( $_FILES['use_settings']['name'][$i] != '' ){
					$file_name = $_FILES['use_settings']['name'][$i];
					$file_type = $_FILES['use_settings']['type'][$i];
					$file_tmp = $_FILES['use_settings']['tmp_name'][$i];
					$file_size = $_FILES['use_settings']['size'][$i];
					if ( ($file_size/1024) < $max_upload_filesize){
						if ( $file_type == 'image/gif' || $file_type == 'image/jpeg' || $file_type == 'image/png' || $file_type == 'image/tiff' ){
							if ( move_uploaded_file($file_tmp,$target_dir . strtolower($file_name) ) )
								echo '<span class="type-exito">The file was uploaded successful.</span>';
							else
								echo '<span class="type-error">The file was\'nt uploaded</span>';
						}
						else
							echo '<span class="type-error">The file ' . $file_name . ' is\'nt an image.</span>';
					}
					else
						echo 'The file <b>' . $_FILES['use_settings']['name'][$i] . '</b> is very longer than ' . $max_upload_filesize . 'Mb.';
				}
			}
		}
		
				echo '<span class="add-category" style="padding: 5px;text-align: right; margin-bottom: 10px;">';
						echo '<form id="main_form" method="POST" enctype="multipart/form-data" action="' . $_SERVER['REQUEST_URI'] . '">';
							echo '<input type="FILE" name="use_settings" />';
							echo '<span style="display: block; float: right;"><button type="submit" name="upload" class="submit-button" value="true"><span class="in-submit-left"><span class="in-submit-right">Upload queue</span></span></button></span>';
						echo '</form>';
				echo '</span>';
		echo '</span>';
			// End: upload_images
	echo '<span style="display: block;">';
	if ( file_exists($images_dir) ){	
		echo '<table cellpadding="0" cellspacing="0" border="0" class="blog-entry">';
			echo '<thead>';
				echo '<tr>';
					echo '<td align="center"><span style="display: block; width: 49px;">&nbsp;</span></td>';
					echo '<td><span style="display: block; width: 400px;">Name</span></td>';
					echo '<td><span style="display: block; width: 100px;">Type</span></td>';
					echo '<td><span style="display: block; width: 100px;">Size</span></td>';
					echo '<td><span style="display: block; width: 100px;">Mod Time</span></td>';
					echo '<td><span style="display: block; width: 100px;">Created Time</span></td>';
					echo '<td colspan="2"><span style="display: block; width: 100px;">Actions</span></td>';
				echo '</tr>';
			echo '</thead>';
			echo '<tbody>';
				
			// Beg: back_button
			
			if ( $directory != '' ){
				$href_back = substr(substr($directory,0,-1),0,strrpos(substr($directory,0,-1),'/'));
				echo '<tr class="on">';
					echo '<td align="center"><span style="display: block; width: 49px;"><img src="../images/icn/admin-16-back.png" /></span></td>';
					echo '<td><a href="' . INDEX_ADMIN . '?action=optImages&dir=' . $href_back . '">&laquo; Back to previuos folder</a></td>';
					echo '<td></td>';
					echo '<td></td>';
					echo '<td></td>';
					echo '<td></td>';
					echo '<td></td>';
					echo '<td></td>';
				echo '</tr>';
			}
			
			// End: back_button
			// Beg: open_dirs
				$hande_dirs = opendir($images_dir . $directory);
					$class_switch = 1;
					while ( $dirs = readdir($hande_dirs) ){
						if ( $dirs != '.' && $dirs != '..' ){
							$opened = $images_dir . $directory . $dirs;
							if ( is_dir($opened) ){
								echo '<tr class="';
									if ( $class_switch == 0 ){
										echo 'on';
										$class_switch = 1;
									}
									else{
										if ( $class_switch == 1 ){
											echo 'off';
											$class_switch = 0;
										}
									}
								echo '">';
								//type (image)
								echo '<td align="center"><span style="display: block; width: 49px;"><img src="../images/icn/admin-16-general-folder-close.png" /></span></td>';
								echo '<td><span style="display: block; width: 400px;"><a href="' . INDEX_ADMIN . '?action=optImages&dir=' . $directory . $dirs . '">' . $dirs . '</a></span></td>';
								echo '<td><span style="display: block; width: 50px;">Directory</span></td>';
								echo '<td><span style="display: block; width: 50px;">1024 Kb</span></td>';
								echo '<td><span style="display: block; width: 100px;">' . date('M d &#124; Y',fileatime($opened)) . '</span></td>';
								echo '<td><span style="display: block; width: 100px;">' . date('M d &#124; Y',filemtime($opened)) . '</span></td>';
								echo '<td align="center"><span style="display: block; width: 35px;"><a href="javascript:void(0);"><img src="../images/icn/admin-16-edit-icn.png" style="border: 0px;"></a></span></td>';
								echo '<td align="center"><span style="display: block; width: 35px;"><a href="javascript:void(0);" onClick="delete_folder(\'' . $dirs . '\',\'' . $opened . '\',\'' . $directory . '\');"><img src="../images/icn/admin-16-cross.png" style="border: 0px;"></a></span></td>';
								echo '</tr>';
							}
						}
					}
				closedir($hande_dirs);
			// End: open_dirs
			
			
			// Beg: open_files
				$hande_files = opendir($images_dir . $directory);
					//$class_switch = 0;
					while ( $files = readdir($hande_files) ){
						if ( $files != '.' && $files != '..' && strtolower($files) != 'thumbs.db' ){
							$opened = $images_dir . $directory . $files;
							if ( !is_dir($opened) ){
								echo '<tr class="';
									if ( $class_switch == 0 ){
										echo 'on';
										$class_switch = 1;
									}
									else{
										if ( $class_switch == 1 ){
											echo 'off';
											$class_switch = 0;
										}
									}
								echo '">';
								//type (image)
								echo '<td align="center"><span style="display: block; width: 49px;"><img src="../images/icn/admin-16-picture.png" /></span></td>';
								echo '<td><span style="display: block; width: 400px;"><a href="download-file.php?f=' . $opened . '">' . $files . '</a></span></td>';
								switch(strtolower(substr($files,strrpos($files,'.')))){
										case '.png':
											$file_type = 'PNG';
										break;
										case '.gif':
											$file_type = 'GIF';
										break;
										case '.jpg':
											$file_type = 'JPEG';
										break;
									}
								echo '<td><span style="display: block; width: 50px;">' . $file_type . '</span></td>';
									$file_size = ceil(@filesize($opened)/1024);
									if ( $file_size < 1024 )
										$file_size = ($file_size) . ' Kb';
									else
										$file_size = ($file_size / 1024) . ' Mb';
								echo '<td><span style="display: block; width: 50px;">' . $file_size . '</span></td>';
								echo '<td><span style="display: block; width: 100px;">' . date('M d &#124; Y',@fileatime($opened)) . '</span></td>';
								echo '<td><span style="display: block; width: 100px;">' . date('M d &#124; Y',filemtime($opened)) . '</span></td>';
								echo '<td align="center"><span style="display: block; width: 35px;"><a href="javascript:void(0);"><img src="../images/icn/admin-16-edit-icn.png" style="border: 0px;"></a></span></td>';
								echo '<td align="center"><span style="display: block; width: 35px;"><a href="javascript:void(0);" onClick="delete_image(\'' . $files . '\',\'' . $opened . '\',\'' . $directory . '\')"><img src="../images/icn/admin-16-cross.png" style="border: 0px;"></a></span></td>';
								echo '</tr>';
							}
						}
					}
				closedir($hande_files);
			// End: open_files
			echo '</tbody>';
		echo '</table>';
	}
	else{
		echo 'Error404 : No such file or directory "' . $images_dir . '"';
	}
	echo '</span>';
?>
