<?php
	
			echo '<span class="advertices">';
				echo '<span class="in-adv">';
					$user_logged = logged_user($_SESSION['session_user_id']);
					echo '<h2 class="admin">Wellcome ' . $user_logged['fname'] . ' ' . $user_logged['lname'] . '</h2>';
					echo 'You are currently viewing the site <span class="black-blue">&#34;' . SITE_NAME . '&#34;</span> site that is loaded with LEAF Professionals &#124; Content Management System.<br /><br />';
					echo 'To add new posts - <a href="' . INDEX_ADMIN . '?action=optBlog&page=addPost" class="styler add-post">click here</a>.<br /><br />';
					echo '<span class="adv">';
						echo 'You can access the account dashboard and site management area functions by <a href="' . INDEX_ADMIN . '?action=manAccounts&page=myAccount" class="styler man-account">clicking the Manage Account</a> button on the General Admin Controls.<br /><br />';
						echo 'If you have a problem whit anything of this sysmen, please contact the site administrator sending a email to: <a href="mailto:' . SITE_ADMIN_EMAIL . '" class="styler send-email">' . SITE_ADMIN_EMAIL . '</a>';
						
					echo '</span>';
				echo '</span>';
			echo '</span>';			
			// General admin
			echo '<span class="index-content">';
				echo '<h2 class="admin">General Settings</h2>';
				echo '<ul class="buttons">';
					echo '<li><a href="javascript:void(0);"><span class="gen-link settings">Settings</span></a></li>';
					echo '<li><a href="javascript:void(0);"><span class="gen-link layout">Layout</span></a></li>';
					echo '<li><a href="javascript:void(0);"><span class="gen-link registry">Registry</span></a></li>';
					echo '<li><a href="javascript:void(0);"><span class="gen-link users">Users</span></a></li>';
					echo '<li><a href="javascript:void(0);"><span class="gen-link component-management">Component Management</span></a></li>';
					echo '<li><a href="javascript:void(0);"><span class="gen-link spam">Spam</span></a></li>';
					echo '<li><a href="javascript:void(0);"><span class="gen-link url-manager">URL Manager</span></a></li>';
					echo '<li><a href="javascript:void(0);"><span class="gen-link view-site">View Site</span></a></li>';
				echo '</ul>';
			echo '</span>';
			
			//Components admin
			echo '<span class="index-content">';
				echo '<h2 class="admin">Components / Gadgets</h2>';
				echo '<ul class="buttons">';
					echo '<li><a href="' . INDEX_ADMIN . '?action=optBlog&page=index"><span class="gen-link blog">Blog</span></a></li>';
					echo '<li><a href="' . INDEX_ADMIN . '?action=optComments"><span class="gen-link comments">Comments</span></a></li>';
					echo '<li><a href="' . INDEX_ADMIN . '?action=optImages"><span class="gen-link images">Images</span></a></li>';
					echo '<li><a href="javascript:void(0);"><span class="gen-link docs">Documents</span></a></li>';
					echo '<li><a href="' . INDEX_ADMIN . '?action=optRoll"><span class="gen-link blogroll">Blog-Roll</span></a></li>';
					echo '<li><a href="javascript:void(0);"><span class="gen-link add-note">Stick Notes</span></a></li>';
					echo '<li><a href="javascript:void(0);"><span class="gen-link email">Email Service</span></a></li>';
					echo '<li><a href="' . INDEX_ADMIN . '?action=manAccounts&page=myAccount"><span class="gen-link account">My Account</span></a></li>';
				echo '</ul>';
			echo '</span>';
?>
