<?php
echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
echo '<html xmlns="http://www.w3.org/1999/xhtml">';
  	echo '<head>';
        	echo '<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />';
        	echo '<title>Admin :: ' . SITE_NAME . '</title>';
        	echo '<link rel="alternate" media="screen" type="application/rss+xml" href="http://' . $_SERVER['SERVER_NAME'] . INDEX . '../feed/" title="RSS 2.0 - All" />';
        	echo '<link rel="shortcut icon" href="../data/favicon/favicon.ico">';
        	echo '<link rel="stylesheet" type="text/css" href="../data/css/admin-stylesheet.css" />';
        	if ( ($_GET['action']=='optBlog' && $_GET['page']=='addPost')  || ( $_GET['action'] == 'optBlog' && $_GET['page'] == 'editPost' && $_GET['idPost'] > 0 && $_GET['idPost'] != '' ) ){
				echo '<script type="text/javascript" src="../fckeditor/fckeditor.js"></script>';
				echo '<script type="text/javascript">';
				echo 'window.onload = function(){';
				echo 'var oFCKeditor = new FCKeditor(\'post_content\',703,600) ;';
				echo 'oFCKeditor.BasePath = "../fckeditor/" ;';
				echo 'oFCKeditor.ReplaceTextarea() ;';
				echo '}';
				echo '</script>';
			}
  			
  			// Mootools Scripts
        	echo '<script type="text/javascript" src="../data/js/mootools.1.2.js"></script>';
        	echo '<script type="text/javascript" src="../data/js/moo.kwick.js"></script>';
        	echo '<script type="text/javascript">window.addEvent(\'domready\',Kwix.start);</script>';
        	echo '<script type="text/javascript" src="../data/js/demo.js"></script>';
        	echo '<script type="text/javascript" src="../data/js/sexyalertbox.packed.js"></script>';
        	echo '<script type="text/javascript" src="../data/js/sexyAlert.delete.post.js"></script>';
  			echo '<script type="text/javascript" src="../data/js/mediaboxAdv.js"></script>';
  			if ( $_GET['action'] == 'optImages' ){
  				echo '<script type="text/javascript" src="../data/js/Stickman.MultiUpload.js"></script>';
  				echo '<script type="text/javascript">window.addEvent(\'domready\', function(){new MultiUpload( $( \'main_form\' ).use_settings, 5, \'[{id}]\', true, true );});</script>';
  			}

	echo '</head>';
	echo '<body>';
	echo '<div align="center">';
		echo '<span id="nav-bar">';
			echo '<span class="top-banner"></span>';
			echo '<span class="dvsr"></span>';
			echo '<span class="site-info">';
			echo '<b>Site info</b>';
				echo '<table cellpadding="0" cellspacing="0" border="0">';
					echo '<tr>';
						echo '<td style="width: 50px;">Ver :</td>';
						echo '<td style="width: 10px;"></td>';
						echo '<td>' . SITE_VERSION . '</td>';
					echo '</tr>';
					echo '<tr><td colspan="3" style="height: 4px;"></td></tr>';
					echo '<tr>';
						echo '<td>Date :</td>';
						echo '<td style="width: 10px;"></td>';
						echo '<td>' . date_month(SITE_UPDATED) . ' ' . date_day(SITE_UPDATED) . ' / ' . date_year(SITE_UPDATED) . '</td>';
					echo '</tr>';
					echo '<tr><td colspan="3" style="height: 4px;"></td></tr>';
					echo '<tr>';
						echo '<td>Name :</td>';
						echo '<td style="width: 10px;"></td>';
						echo '<td><a href="' . INDEX . '">';
							echo substr(SITE_NAME,0,40);
							if ( strlen(SITE_NAME) > 40 )
								echo '&#133;';
						echo '</a></td>';
					echo '</tr>';
				echo '</table>';
			echo '</span>';
			echo '<span class="site-nav-bar">';
				echo '<span class="arrow-dvsr"></span>';
				echo '<div id="kwick">';
					echo '<ul class="kwicks">';
						echo '<li><a class="kwick faqs" href="javascript:void(0);" title="Faq\'s"><span>Faq\'s</span></a></li>';
						echo '<li><a class="kwick notes" href="javascript:void(0);" title="Notes"><span>Notes</span></a></li>';
						echo '<li><a class="kwick users" href="javascript:void(0);" title="Users"><span>Uses</span></a></li>';
						echo '<li><a class="kwick preferences" href="javascript:void(0);" title="Preferences"><span>Preferences</span></a></li>';
						echo '<li><a class="kwick archives" href="javascript:void(0);" title="Documents"><span>Documents</span></a></li>';
						echo '<li><a class="kwick picture" href="' . INDEX_ADMIN . '?action=optImages" title="Images"><span>Images</span></a></li>';
						echo '<li><a class="kwick dashboard" href="' . INDEX_ADMIN . '" title="Dashboard"><span>Dashboard</span></a></li>';
					echo '</ul>';
				echo '</div>';
			echo '</span>';
			echo '<span class="dvsr" style="margin-left: 0px;"></span>';
			echo '<a href="' . INDEX_ADMIN . 'logout.php" class="site-logout"><span class="in-logout">Logout</span></a>';
		echo '</span>';
?>