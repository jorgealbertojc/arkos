<?php

	// Saving the img name
	if ( $_POST['chimgname'] == true ){
		if ( strlen($_POST['img_name']) < 2 ){
			echo '<span class="type-error">The name of the image is very short.</span>';
		}
		else{
			if ( strlen($_POST['img_name']) >= 2 ){
				$sql_save_img_name = 'UPDATE web_image_gallery SET image_title="' . str_replace($car_esp,$car_hex,$_POST['img_name']) . '" WHERE image_id=' . $_POST['img_id'] . ';';
				exeQuery($sql_save_img_name);
				echo '<span class="type-info">Please wait...</span>';
				echo '<script type="text/javascript">window.location.href=\'' . INDEX_ADMIN . '?action=optImages&page=gallery&site=viewGallery&idGallery=' . $_GET['idGallery'] . '&adv=1&type=gallery&opt=imgChname&res=1\'</script>';
			}
		}
	}
	
	// Saving new gallery
	if ( $_POST['save_gallery'] == true ){
		if ( strlen($_POST['gallery_name']) < 2 || strlen($_POST['gallery_description']) < 5 ){
			echo '<span class="type-error"><ul>';
				if ( strlen($_POST['gallery_name']) < 2 )
					echo '<li>The name of the gallery is very short.</li>';
				if ( strlen($_POST['gallery_description']) < 5 )
					echo '<li>The description of the gallery is very short.</li>';
			echo '</ul></span>';
		}
		else{
			if ( strlen($_POST['gallery_name']) >= 2 && strlen($_POST['gallery_description']) >= 5 ){
				$target_folder = str_replace($car_esp,'_',strtolower($_POST['gallery_name']));
				$target_folder = str_replace(' ','_',$target_folder);
				$target_gallery = '../gallery/images/';
				if ( !file_exists($target_gallery . $target_folder) ){
					$sql_save_gallery = 'INSERT INTO web_gallery VALUES(
																	NULL,
																	"' . $target_gallery . $target_folder . '",
																	"' . str_replace($car_esp,$car_hex,$_POST['gallery_name']) . '",
																	"' . str_replace($car_esp,$car_hex,$_POST['gallery_description']) . '",
																	' . $_SESSION['session_user_id'] . ',
																	NULL,
																	1
																	);';
					exeQuery($sql_save_gallery);
					mkdir($target_gallery . $target_folder,0777);
					echo '<span class="type-info">Please wait...</span>';
					echo '<script type="text/javascript">window.location.href=\'' . INDEX_ADMIN . '?action=optImages&page=gallery&site=' . $_GET['site'] . '&adv=1&type=gallery&opt=created&res=1\'</script>';
				}
				else
					echo '<span class="type-info">The folder "' . $target_folder . '" for the gallery "' . str_replace($car_esp,$car_hex,$_POST['gallery_name']) . '" is in the server, please change the gallery name.</span>';
				
			}
		}
	}
	
	// Updating gallery
	if ( $_POST['update_gallery'] == true ){
		if ( strlen($_POST['gallery_update_name']) < 2 || strlen($_POST['gallery_update_description']) < 5 ){
			echo '<span class="type-error"><ul>';
				if ( strlen($_POST['gallery_update_name']) < 2 )
					echo '<li>The name of the gallery is very short.</li>';
				if ( strlen($_POST['gallery_update_description']) < 5 )
					echo '<li>The description of the gallery is very short.</li>';
			echo '</ul></span>';
		}
		else{
			if ( strlen($_POST['gallery_update_name']) >= 2 && strlen($_POST['gallery_update_description']) >= 5 && $_POST['gallery_update_id'] > 0 ){
				$sql_update_gallery_info = 'UPDATE web_gallery SET gallery_name="' . str_replace($car_esp,$car_hex,$_POST['gallery_update_name']) . '",gallery_description="' . str_replace($car_esp,$car_hex,$_POST['gallery_update_description']) . '" WHERE gallery_id=' . $_POST['gallery_update_id'] . ' LIMIT 1;';
				exeQuery($sql_update_gallery_info);
				echo '<span class="type-info">Please wait...</span>';
				echo '<script type="text/javascript">window.location.href=\'' . INDEX_ADMIN . '?action=optImages&page=gallery&site=' . $_GET['site'] . '&adv=1&type=gallery&opt=update&res=1\'</script>';
			}
		}
	}
	
	
	
	echo 'Actions : ';
	echo '<a href="#mb_create_gallery" rel="lightbox[inline 580 320]" class="addCategory"><span>Add Gallery</span></a>';
	echo '<a href="' . INDEX_ADMIN . '?action=optImages&page=gallery&site=upload" class="upload-gallery"><span>Upload images</span></a>';
	echo '<span class="index-content">';
		switch($_GET['site']){
			case 'viewGallery':
				include_once 'admin-image-this-gallery.php';
				break;
			case 'upload':
				include_once 'admin-images-upload.php';
				break;
			default:
				include_once 'admin-image-index-gallery.php';
		}
	echo '</span>';
	
	
	// Create new gallery form
	echo '<div id="mb_create_gallery" style="display: none;">';
		echo '<span class="add-category">';
		echo '<form method="post" action="' . INDEX_ADMIN . '?action=optImages&page=gallery&site=' . $_GET['site'] . '">';
			echo '<h2 class="admin">Create new gallery</h2>';
			echo '<table cellpadding="0" cellspacing="0" border="0">';
				echo '<tr>';
					echo '<td><span style="display: block; width: 100px;">Gallery Name:</span></td>';
					echo '<td>';
						echo '<input type="text" class="input-text" style="width: 400px;" name="gallery_name"';
							if ( $_POST['save_gallery'] == true )
								echo ' value="' . $_POST['gallery_name'] . '"';
						echo ' />';
					echo '</td>';
				echo '</tr>';
				echo '<tr><td colspan="2" style="height: 10px;"></td></tr>';
				echo '<tr>';
					echo '<td valign="top"><span style="display: block; width: 100px;">Gallery Description:</span></td>';
					echo '<td>';
						echo '<textarea class="input-text" style="width: 400px; height: 150px;" name="gallery_description">';
							if ( $_POST['save_gallery'] == true )
								echo $_POST['gallery_description'];
						echo '</textarea>';
					echo '</td>';
				echo '</tr>';
				echo '<tr><td colspan="2" style="height: 10px;"></td></tr>';
				echo '<tr><td colspan="2" align="right">';
					echo '<button type="submit" class="submit-button" name="save_gallery" value="true"><span class="in-submit-left"><span class="in-submit-right">Save Gallery</span></span></button>';
					echo '<button type="reset" class="submit-button" name="save_gallery" value="false"><span class="in-submit-left"><span class="in-submit-right">Reset</span></span></button>';
					echo '<a href="javascript:void(0);" onClick="Mediabox.close();" class="submit-button"><span class="in-submit-left"><span class="in-submit-right">Cancel</span></span></button>';
				echo '</td></tr>';
			echo '</table>';
		echo '</form>';
		echo '</span>';
	echo '</div>';
?>
