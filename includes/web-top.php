<?php

	echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
	echo '<html xmlns="http://www.w3.org/1999/xhtml">';
		echo '<head>';
			echo '<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />';
			echo '<title> ';
			// titulo del post en el titulo de la pagina
				switch($_GET['call']){
					case 'show_post':{
						if ( isset($_GET['post_id']) && $_GET['post_id'] > 0 && $_GET['post_id']!=''){
							$sql_show_title_post = 'SELECT post_title FROM web_posts WHERE post_id="' . $_GET['post_id'] . '" LIMIT 1';
							$res_show_title_post = exeQuery($sql_show_title_post);
							if ( mysql_num_rows($res_show_title_post) > 0 ){
								$show_title_post = mysql_fetch_array($res_show_title_post);
								echo $show_title_post['post_title'];
								echo ' :: ';
							}
						}
						break;
					}
					case 'show_gallery':{
						echo 'Galerias';
						if ( isset($_GET['gallery_id']) && $_GET['gallery_id'] > 0 && $_GET['gallery_id']!='' ){
							$sql_show_gallery_name = 'SELECT gallery_name FROM web_gallery WHERE gallery_id="' . $_GET['gallery_id'] . '" LIMIT 1';
							$res_show_gallery_name = exeQuery($sql_show_gallery_name);
							if ( mysql_num_rows($res_show_gallery_name) > 0 ){
								$show_gallery_name = mysql_fetch_array($res_show_gallery_name);
								echo ' / ';
								echo $show_gallery_name['gallery_name'];
							}
							else{
								echo 'Galeria sin nombre';
							}
							echo ' :: ';
						}
						else{
							echo ' / Galer&iacute;a inexistente :: ';
						}
						break;
					}
				}
			echo SITE_NAME;
			echo '</title>';
			echo '<link rel="alternate" media="screen" type="application/rss+xml" href="http://' . $_SERVER['SERVER_NAME'] . INDEX . '../feed/" title="RSS 2.0 - All" />';
			echo '<link rel="shortcut icon" href="../data/favicon/favicon.ico">';
			echo '<link rel="stylesheet" type="text/css" href="../data/css/stylesheet.css" />';
			echo '<script type="text/javascript" src="../data/js/jquery-1.3.2.min.js"></script>';
			echo '<script type="text/javascript" src="../data/js/jquery.ddaccordion.js"></script>';
			echo '<script type="text/javascript" src="../data/js/niceforms.js"></script>';
			echo '<script type="text/javascript" src="../data/js/jquery.quicksearch.js"></script>';
			echo '<script type="text/javascript" src="../data/js/jquery.lightbox-0.5.js"></script>';
			echo '<script type="text/javascript" src="../data/js/jquery.qtip-1.0.0-rc3.js"></script>';
			echo '<script type="text/javascript" src="../data/js/jquery.effects.js"></script>';
			echo '<script type="text/javascript" src="../data/highlighter/scripts/shCore.js"></script>';
			echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushBash.js"></script>';
			echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushCpp.js"></script>';
			echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushCSharp.js"></script>';
			echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushCss.js"></script>';
			echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushDelphi.js"></script>';
			echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushDiff.js"></script>';
			echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushGroovy.js"></script>';
			echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushJava.js"></script>';
			echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushJScript.js"></script>';
			echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushPhp.js"></script>';
			echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushPlain.js"></script>';
			echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushPython.js"></script>';
			echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushRuby.js"></script>';
			echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushScala.js"></script>';
			echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushSql.js"></script>';
			echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushVb.js"></script>';
			echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushXml.js"></script>';
			//echo '<link type="text/css" rel="stylesheet" href="../data/highlighter/styles/shCore.css"/>';
			//echo '<link type="text/css" rel="stylesheet" href="../data/highlighter/styles/shThemeDefault.css"/>';
			echo '<script type="text/javascript">';
				echo 'SyntaxHighlighter.config.clipboardSwf = \'../data/highlighter/scripts/clipboard.swf\';';
				echo 'SyntaxHighlighter.all();';
			echo '</script>';
		echo '</head>';
		echo '<body>';
		echo '<a name="top"></a>';
		echo '<div align="center">';
			echo '<div id="div-body">';
				// Inicio del banner
				echo '<div id="banner-top" style="margin-top: 0px;">';
					echo '<span class="right-pos">Bienvenido, ';
					
						if ( isset($_SESSION['session_user_id']) && isset($_SESSION['session_user_name']) ){
							$names = logged_user($_SESSION['session_user_id']);
							echo $names['fname'] . ' ' . $names['lname'] . ' &#124; <a href="' . INDEX_ADMIN . '">Site Admin</a> &#124; <a href="' . INDEX . 'logout.php">Logout</a>';
						}
						else{
							echo 'Visitante <span class="global-black-char">&#124;</span> <a href="' . INDEX_ADMIN . '">Login</a>';
						}
					
					echo ' <a class="rss" href="http://' . $_SERVER['SERVER_NAME'] . INDEX  . '../feed/">RSS</a></span>';
					echo '<span class="banner"></span>';
					echo '<div id="main-tabs">';
						echo '<ul class="tabs">';
							echo '<li><a href="' . INDEX . HOME . '" class="';
								if ( !isset($_GET['option']) || $_GET['option']=='index' || $_GET['option']=='' )
									echo 'this';
								else
									echo 'tab';
							echo '"><span>Home / Index</span></a></li>';
							echo '<li><a href="' . INDEX . GALLERY . '" class="';
								if ( $_GET['option']=='gallery' )
									echo 'this';
								else
									echo 'tab';
							echo '"><span>Galerias</span></a></li>';
							echo '<li><a href="' . INDEX . PORTFOLIO . '" class="';
								if ( $_GET['option']=='portfolio' )
									echo 'this';
								else
									echo 'tab';
							echo '"><span>Portafolio</span></a></li>';
							echo '<li><a href="' . INDEX . CONTACT . '" class="';
								if ( $_GET['option']=='contact' )
									echo 'this';
								else
									echo 'tab';
							echo '"><span>Contacto</span></a></li>';
						echo '</ul>';
					echo '</div>';
				echo '</div>';
				//Fin del banner
				
				//Inicio del bloque de contenido
				echo '<div id="div-content">';
					echo '<span class="top-margin"></span>';
					//Inicio del espacio que puede ser utilizado para escribir
					echo '<span class="write-space">';
						advertises();

?>
