<?php
	
	
	
	echo '<span class="index-content">';
		echo '<div id="traza">';
			echo '<a href="' . INDEX_ADMIN . '" class="item">Dashboard</a>';
			echo '<span class="middle"></span>';
			echo '<a href="' . INDEX_ADMIN . '?action=optComments" class="item">Comments</a>';
			if ( isset($_GET['page']) && $_GET['page'] != '' ){
				switch($_GET['page']){
					case 'reply':
						$b_link = 'reply';
						$b_title = 'Reply Comment';
						break;
				}
				echo '<span class="middle"></span>';
				echo '<a href="' . INDEX_ADMIN . '?action=optBlog&page=' . $b_link . '" class="item">' . $b_title . '</a>';
				echo '<span class="right"></span>';
			}
			else{
				echo '<span class="right"></span>';
			}
			echo '</div>';
			// Advertices function
			admin_advertices();
			
			if ( $_POST['reply_submit'] == 1 ){
				if ( strlen($_POST['reply_subject']) < 5  || strlen($_POST['reply_content']) < 10 ){
					echo '<span class="type-error"><ul>';
						if ( strlen($_POST['reply_subject']) < 5 )
							echo '<li>The subject of reply is very short.</li>';
						if ( strlen($_POST['reply_content']) < 10 )
							echo '<li>The answer of reply is very short.</li>';
					echo '</ul></span>';
				}
				else{
					if ( strlen($_POST['reply_subject']) >= 5  && strlen($_POST['reply_content']) >= 10 ){
						$sql_save_reply = 'INSERT INTO web_comments VALUES(
																		NULL,
																		' . $_POST['reply_post_id'] . ',
																		"' . user_id($_SESSION['session_user_id']) . '",
																		"' . str_replace($car_esp,$car_hex,$_POST['reply_content']) . '",
																		"' . admin_user_email($_SESSION['session_user_id']) . '",
																		"http://' . $_SERVER['SERVER_NAME'] . '",
																		"' . getRealIp() . '",
																		NULL
																		);';
						//echo $sql_save_reply; cuestion de ejecutar el query
						//Configuracion del correo electronico que se enviara al usuario
						
						$headers = 'MIME-Version: 1.0' . "\r\n";
						$headers .= 'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";
						$headers .= 'From: "Web-Blog ' . str_replace($car_hex,$car_esp,SITE_NAME) . '" <no-reply@' . $_SERVER['SERVER_NAME'] . '>' . "\r\n";
						$headers .= 'Reply-To: "Web-Blog ' . str_replace($car_hex,$car_esp,SITE_NAME) . '" <no-reply@' . $_SERVER['SERVER_NAME'] . '>' . "\r\n";
						
						$subject = str_replace('\n','',$_POST['reply_subject']);
						
						$reply_to = '"' . $_POST['reply_user_name'] . '" <' . $_POST['reply_email_autor'] . '>';
						$replier_name = logged_user($_SESSION['session_user_id']);
						$to_replier = '"' . $replier_name['fname'] . ' ' . $replier_name['lname'] . '" <' . admin_user_email($_SESSION['session_user_id']) . '>';
						
						$message = '
<html>
	<body>
		<div align="center">
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td bgcolor="#000000" background="http://' . $_SERVER['SERVER_NAME'] . '/images/bgs/admin-bg-nav-bar.png"><span style="display: block; width: 700px; height: 40px; padding-top: 10px; padding-bottom: 10px;"><img src="http://' . $_SERVER['SERVER_NAME'] . '/images/bnr/admin-mail-title.png" /></span></td>
				</tr>
				<tr>
					<td>
						<span style="display: block; width: 700px; background-color: #fff;"><span style="display: block; padding: 20px; border-left: 1px solid #f4f4f4; border-right: 1px solid #f4f4f4; font-family: Lucida Sans Unicode, Trebuchet MS, Arial, Sans-serif; font-size: 11px; color: #345;">
							Dear <b>' . $_POST['reply_user_name'] . '</b>.<br /><br />
							in response to your comment made on : ' . $_POST['reply_com_date'] . ':
							<span style="display: block; margin-top: 10px; margin-bottom: 10px; padding: 20px; background-color: #fafafa; border: 1px solid #eaeaea;">
								' . nl2br($_POST['reply_content']) . '
								<span style="display: block; margin-top: 20px; font-size: 10px; color: #678; padding-top: 10px; border-top: 1px solid #eaeaea;">
									Responded  : ' . user_id($_SESSION['session_user_id']) . '<br />
									Date : ' . date('r') . '
								</span>
							</span>
							<span style="display: block; margin-top: 20px; padding-top: 10px; border-top: 1px solid #f0f0f0; font-family: Trebuchet MS, Arial, Sans-serif; font-size: 10px; text-align: center;">
								<a href="http://' . $_SERVER['SERVER_NAME'] . '/index/" style="text-decoration: none"><span style="color: #9ab;">Index / Home</span></a>
								&nbsp; &#124; &nbsp; <a href="http://' . $_SERVER['SERVER_NAME'] . '/index/?option=gallery" style="text-decoration: none"><span style="color: #9ab;">Galleria</span></a>
								&nbsp; &#124; &nbsp; <a href="http://' . $_SERVER['SERVER_NAME'] . '/index/" style="text-decoration: none"><span style="color: #9ab;">Portafolio</span></a>
								&nbsp; &#124; &nbsp; <a href="http://' . $_SERVER['SERVER_NAME'] . '/index/?option=contact" style="text-decoration: none"><span style="color: #9ab;">Contacto</span></a>
								&nbsp; &#124; &nbsp; <a href="http://' . $_SERVER['SERVER_NAME'] . '" style="text-decoration: none"><span style="color: #9ab;">Web Development CMS</span></a>
								&nbsp; &#124; &nbsp; <a href="http://' . $_SERVER['SERVER_NAME'] . '" style="text-decoration: none"><span style="color: #9ab;">LEAF Professionals</span></a>
							</span>
						</span></span>
					</td>
				</tr>
				<tr>
					<td bgcolor="#304050"><span style="display: block; width: 700px; height: 20px;"></span></td>
				</tr>
			</table>
			<span style=" display: block; width: 700px;font-family: Trebuchet MS, Arial, Sans-serif; font-size: 10px; color: #89a; text-align: center; margin-top: 30px;">
				You are receiving this message from ' . SITE_NAME . ', because you are one of our valued members, or you left a comment. ' . SITE_NAME . ' respects your privacy.
			</span>
		</div>
	</body>
</html>';
						$message_to_replier = '
<html>
	<body>
		<div align="center">
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td bgcolor="#000000" background="http://' . $_SERVER['SERVER_NAME'] . '/images/bgs/admin-bg-nav-bar.png"><span style="display: block; width: 700px; height: 40px; padding-top: 10px; padding-bottom: 10px;"><img src="http://' . $_SERVER['SERVER_NAME'] . '/images/bnr/admin-mail-title.png" /></span></td>
				</tr>
				<tr>
					<td>
						<span style="display: block; width: 700px; background-color: #fff;"><span style="display: block; padding: 20px; border-left: 1px solid #f4f4f4; border-right: 1px solid #f4f4f4; font-family: Lucida Sans Unicode, Trebuchet MS, Arial, Sans-serif; font-size: 11px; color: #345;">
							You have replied to a message left by <b>' . $_POST['reply_user_name'] . '</b>.<br /><br /> Your answer was the following:
							<span style="display: block; margin-top: 10px; margin-bottom: 10px; padding: 20px; background-color: #fafafa; border: 1px solid #eaeaea;">
								' . nl2br($_POST['reply_content']) . '
							</span>
							<span style="display: block; margin-top: 20px; padding-top: 10px; border-top: 1px solid #f0f0f0; font-family: Trebuchet MS, Arial, Sans-serif; font-size: 10px; text-align: center;">
								<a href="http://' . $_SERVER['SERVER_NAME'] . '/index/" style="text-decoration: none"><span style="color: #9ab;">Index / Home</span></a>
								&nbsp; &#124; &nbsp; <a href="http://' . $_SERVER['SERVER_NAME'] . '/index/?option=gallery" style="text-decoration: none"><span style="color: #9ab;">Galleria</span></a>
								&nbsp; &#124; &nbsp; <a href="http://' . $_SERVER['SERVER_NAME'] . '/index/" style="text-decoration: none"><span style="color: #9ab;">Portafolio</span></a>
								&nbsp; &#124; &nbsp; <a href="http://' . $_SERVER['SERVER_NAME'] . '/index/?option=contact" style="text-decoration: none"><span style="color: #9ab;">Contacto</span></a>
								&nbsp; &#124; &nbsp; <a href="http://' . $_SERVER['SERVER_NAME'] . '" style="text-decoration: none"><span style="color: #9ab;">Web Development CMS</span></a>
								&nbsp; &#124; &nbsp; <a href="http://' . $_SERVER['SERVER_NAME'] . '" style="text-decoration: none"><span style="color: #9ab;">LEAF Professionals</span></a>
							</span>
						</span></span>
					</td>
				</tr>
				<tr>
					<td bgcolor="#304050"><span style="display: block; width: 700px; height: 20px;"></span></td>
				</tr>
			</table>
			<span style=" display: block; width: 700px;font-family: Trebuchet MS, Arial, Sans-serif; font-size: 10px; color: #89a; text-align: center; margin-top: 30px;">
				You are receiving this message from ' . SITE_NAME . ', because you are one of our valued members, or you left a comment. ' . SITE_NAME . ' respects your privacy.
			</span>
		</div>
	</body>
</html>';
						exeQuery($sql_save_reply);
						if ( @mail($reply_to,$subject,$message,$headers) ){
							//echo '<span class="type-exito">The reply has been sent to "' . $_POST['reply_user_name'] . '" &lt;' . $_POST['reply_email_autor'] . '&gt;</span>';
							echo '<script type="text/javascript">window.location.href=\'' . INDEX_ADMIN . '?action=optComments&adv=1&type=reply&save=1&send=1\'</script>';
						}
						else{
							//echo '<span class="type-error">The reply can\'t be sent, please check your SMTP server.</span>';
							echo '<script type="text/javascript">window.location.href=\'' . INDEX_ADMIN . '?action=optComments&adv=1&type=reply&save=1&send=0\'</script>';
						}
						@mail($to_replier,$subject,$message_to_replier,$headers);
					}
				}
			}
	echo '</span>';
	echo '<span class="index-content">';
		echo '<table cellpadding="0" cellspacing="0" border="0" class="blog-entry">';
			echo '<thead>';
				echo '<tr>';
					echo '<td align="center"><span style="display: block; width: 49px;">#</span></td>';
					echo '<td><span style="display: block; width: 250px;">Autor</span></td>';
					echo '<td><span style="display: block; width: 350px;">Post</span></td>';
					echo '<td><span style="display: block; width: 100px;">Date</span></td>';
					echo '<td><span style="display: block; width: 120px;">IP</span></td>';
					echo '<td colspan="3"><span style="display: block; width: 60px;">Actions</span></td>';
				echo '</tr>';
			echo '</thead>';
			echo '<tbody>';
				
				$sql_show_comments = 'SELECT * FROM web_comments ORDER BY com_date_created DESC';
				
				$res_show_comments = exeQuery($sql_show_comments);
				if ( mysql_num_rows($res_show_comments) > 0 ){
					$class_theme = 0;
					$i = 0;
					while ( $show_comments = mysql_fetch_array($res_show_comments) ){
						echo '<tr class="';
							if ( $class_theme == 0 ){
								$class_theme = 1;
								echo 'on';
							}
							else{
								if ( $class_theme == 1 ){
									$class_theme = 0;
									echo 'off';
								}
							}
						echo '">';
							//ID
							echo '<td align="center"><span style="display: block; width: 49px;">' . $show_comments['com_id'] . '</span></td>';
							//Autor
							echo '<td><span style="display: block; width: 250px;">' . $show_comments['com_autor'] . '</span></td>';
							//Post
							echo '<td><span style="display: block; width: 350px;">RE: ';
								$sql_post_title = 'SELECT post_title FROM web_posts WHERE post_id=' . $show_comments['com_post'] . ' LIMIT 1;';
								$res_post_title = exeQuery($sql_post_title);
								if ( mysql_num_rows($res_post_title) == 1 ){
									$post_title = mysql_fetch_array($res_post_title);
									$title_of_post = $post_title['post_title'];
									echo substr($post_title['post_title'],0,50);
									if ( strlen($post_title['post_title']) > 50 )
										echo '&#133;';
								}
								else{
									$title_of_post = 'Undefined';
									echo 'Undefined. Please check this and report bug.';
								}
							echo '</span></td>';
							//Date
							echo '<td><span style="display: block; width: 100px;">' . substr(date_month($show_comments['com_date_created']),0,3) . ' ' . date_day($show_comments['com_date_created']) . ' &#124; ' . date_year($show_comments['com_date_created']) . '</span></td>';
							//Ip
							echo '<td><span style="display: block; width: 120px;">' . $show_comments['com_autor_ip'] . '</span></td>';
							//Actions
								// See comment
							echo '<td align="center"><span style="display: block;width: 25px;"><a href="#mb_see_comment_' . $show_comments['com_id'] . '" rel="lightbox[inline 800 600]"><img src="../images/icn/admin-16-see-document.png" title="See The Comment" style="border: 0px;" /></a></span>';
								 echo '<div id="mb_see_comment_' . $show_comments['com_id'] . '" style="display: none;">';
								 	echo '<span class="add-category">';
								 		echo '<span style="float: right;"><img src="http://www.gravatar.com/avatar.php?gravatar_id=' . md5($show_comments['com_email']) . '&size=50" style="height: 50px; width: 50px; padding: 3px; border: 1px solid #102a3f; margin: 5px; display: block;" /></span>';
								 		echo 'From : ' . $show_comments['com_autor'] . '<br /> Email: <a href="mailto:' . $show_comments['com_email'] . '">' . $show_comments['com_email'] . '</a><br /><br />';
								 		echo nl2br($show_comments['com_comment']);
								 	echo '</span>';
								 echo '<div>';
							echo '</td>';
								// Reply comment
					echo '<td align="center"><span style="display: block;width: 25px;"><a href="#mb_reply_comment_' . $show_comments['com_id'] . '" rel="lightbox[inline 620 500]"><img src="../images/icn/admin-16-send-reply.png" title="Reply Comment" style="border: 0px;" /></a></span>';
						echo '<div id="mb_reply_comment_' . $show_comments['com_id'] . '" style="display: none;">';
							echo '<span class="add-category">';
								echo 'Reply to comment in : ' . $title_of_post . '<br />';
								echo 'From : ' . $show_comments['com_autor'] . '<br /><br />';
								echo '<form method="post" action="' . INDEX_ADMIN . '?action=optComments">';
									echo '<input type="hidden" name="reply_post_id" value="' . $show_comments['com_post'] . '" />';
									echo '<input type="hidden" name="reply_email_autor" value="' . $show_comments['com_email'] . '" />';
									echo '<input type="hidden" name="reply_user_name" value="' . $show_comments['com_autor'] . '" />';
									echo '<input type="hidden" name="reply_com_date" value="' . date_month($show_comments['com_date_created']) . ' ' . date_day($show_comments['com_date_created']) . ' de ' . date_year($show_comments['com_date_created']) . '" />';
									echo '<table cellpadding="0" cellspacing="0" border="0">';
										echo '<tr>';
											echo '<td>Subject:</td>';
											echo '<td style="width: 10px;"></td>';
											echo '<td><input type="text" name="reply_subject" class="input-text" style="width: 500px;" value="RE: ' . $title_of_post . '" /></td>';
										echo '</tr>';
										echo '<tr><td colspan="3" style="height: 10px;"></td></tr>';
										echo '<tr>';
											echo '<td valign="top">Answer:</td>';
											echo '<td style="width: 10px;"></td>';
											echo '<td><textarea name="reply_content" class="input-text" style="width: 500px; height: 300px;"></textarea></td>';
										echo '</tr>';
										echo '<tr><td colspan="3" style="height: 10px;"></td></tr>';
										echo '<tr><td colspan="3" align="right">';
											echo '<button type="submit" name="reply_submit" value="1" class="submit-button"><span class="in-submit-left"><span class="in-submit-right">Send</span></span></button>';
											echo '<button type="reset" name="reply_submit" value="0" class="submit-button"><span class="in-submit-left"><span class="in-submit-right">Reset</span></span></button>';
											echo '<a href="javascript:void(0);" onClick="Mediabox.close();" class="submit-button"><span class="in-submit-left"><span class="in-submit-right">Cancel</span></span></button>';
										echo '</td></tr>';
									echo '</table>';
								echo '</form>';
							echo '</span>';
						echo '</div>';
					echo '</td>';
					echo '<td align="center"><span style="display: block;width: 25px;"><a href="javascript:void(0);" onClick="delete_comment(' . $show_comments['com_id'] . ',\'' . $show_comments['com_autor'] . '\')"><img src="../images/icn/admin-16-cross.png" title="Delete Comment" style="border: 0px;" /></a></span></td>';
						echo '</tr>';
					}
				}
				else{
					echo '<tr><td colspan="0" align="center">No comments found.</td></tr>';
				}
			echo '</tbody>';
		echo '</table>';
	echo '</span>';
?>
