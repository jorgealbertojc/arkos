<?php
	// Beg: Traza
	
	echo '<span class="index-content">';
		echo '<div id="traza">';
			echo '<a href="' . INDEX_ADMIN . '" class="item">Dashboard</a>';
			echo '<span class="middle"></span>';
			echo '<a href="' . INDEX_ADMIN . '?action=manAccounts&page=index" class="item">Accounts</a>';
			if ( isset($_GET['page']) && $_GET['page'] != '' ){
				switch($_GET['page']){
					case 'index':
						$b_link = 'accounts';
						$b_title = 'Manage Accounts';
						break;
					case 'myAccount':
						$b_link = 'myAccount';
						$b_title = 'My Account';
						break;
					default:
						$b_link = 'editAccount';
						$b_title = 'Edit My Account';
				}
				echo '<span class="middle"></span>';
				echo '<a href="' . INDEX_ADMIN . '?action=manAccounts&page=' . $b_link . '" class="item">' . $b_title . '</a>';
				echo '<span class="right"></span>';
			}
			else{
				echo '<span class="right"></span>';
			}
			echo '</div>';
			// Advertices function
			admin_advertices();
	echo '</span>';
	
	// End: Traza
	echo '<span class="index-content">';
	// edit acount-info
	if ( $_POST['update_account_info'] == true ){
		if ( strlen($_POST['user_first_name']) < 2 || strlen($_POST['user_last_name']) < 2 || !is_email($_POST['user_email']) ){
			echo '<span class="type-error"><ul>';
			if ( strlen($_POST['user_first_name']) < 2 )
				echo '<li>The first name is very short.</li>';
			if ( strlen($_POST['user_last_name']) < 2 )
				echo '<li>The last name is very short.</li>';
			if ( strlen($_POST['user_email']) < 2 )
				echo '<li>The email is invalid.</li>';
			echo '</ul></span>';
		}
		else{
			if ( strlen($_POST['user_first_name']) >= 2 && strlen($_POST['user_last_name']) >= 2 && is_email($_POST['user_email']) ){
				$sql_update_user_info = 'UPDATE web_users SET 
															user_user_first_name="' . str_replace($car_esp,$car_hex,$_POST['user_first_name']) . '",
															user_user_last_name="' . str_replace($car_esp,$car_hex,$_POST['user_last_name']) . '",
															user_user_email="' . $_POST['user_email'] . '",
															user_date_updated="' . date('Y-m-d H:i:s') . '"
															WHERE user_id=' . $_SESSION['session_user_id'] . ' LIMIT 1';
				exeQuery($sql_update_user_info);
				echo '<span class="type-info">Updating info, please wait...</span>';
				echo '<script type="text/javascript">window.location.href=\'' . INDEX_ADMIN . '?action=manAccounts&page=myAccount&adv=1&type=accounts&opt=editMyAccount&res=1\'</script>';
			}
		}
	}
	// Edit passwd
	if ( $_POST['update_passwd'] == true ){
		if ( strlen($_POST['actual_passwd']) < 6 || strlen($_POST['new_passwd']) < 6 || strlen($_POST['repeat_new_passwd']) < 6 || strlen($_POST['new_passwd']) != strlen($_POST['repeat_new_passwd']) ){
			echo '<span class="type-error"><ul>';
			if ( strlen($_POST['actual_passwd']) < 6 )
				echo '<li>The actual passwd is incorrect.</li>';
			if ( strlen($_POST['new_passwd']) < 6 || strlen($_POST['repeat_new_passwd']) )
				echo '<li>The new passwd is very short.</li>';
			if ( strlen($_POST['new_passwd']) != strlen($_POST['repeat_new_passwd']) )
				echo '<li>The new passwd not been in the correct format.</li>';
			echo '</ul></span>';
		}
		else{
			if ( strlen($_POST['actual_passwd']) >= 6 && strlen($_POST['new_passwd']) >= 6 && strlen($_POST['repeat_new_passwd']) >= 6 && strlen($_POST['new_passwd']) == strlen($_POST['repeat_new_passwd']) ){
				$sql_check_passwd = 'SELECT user_name,user_passwd FROM web_users WHERE user_name="' . user_id($_SESSION['session_user_id']) . '" AND user_passwd="' . md5($_POST['actual_passwd']) . '" LIMIT 1';
				$res_check_passwd = exeQuery($sql_check_passwd);
				if ( mysql_num_rows( $res_check_passwd ) > 0 ){
					if ( $_POST['new_passwd'] == $_POST['repeat_new_passwd'] ){
						$sql_update_passwd = 'UPDATE web_users SET user_passwd="' . md5($_POST['new_passwd']) . '",user_date_updated="' . date('Y-m-d H:i:s') . '" WHERE user_id=' . $_SESSION['session_user_id'] . ' LIMIT 1;';
						exeQuery($sql_update_passwd);
						echo '<span class="type-info">Updating passwd, please wait...</span>';
						echo '<script type="text/javascript">window.location.href=\'' . INDEX_ADMIN . '?action=manAccounts&page=myAccount&adv=1&type=accounts&opt=editPasswd&res=1\'</script>';
					}
					else{
						echo '<span class="type-error">The new passwd and the repeat new passwd is\'nt equal.</span>';
					}
				}
				else
					echo '<span class="type-error">The user ca\'nt be found.</span>';
			}
		}
	}
	echo '</span>';
	
	
	
	
	// Beg: switch for select the correctly page
	
	switch($_GET['page']){
		case 'myAccount':
			include_once 'admin-users-my-account.php';
			break;
		default:
			echo 'ja';
	}
	
	// End: switch for select the correctly page
?>
