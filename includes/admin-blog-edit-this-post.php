<?php
	if ( $_POST['post_submit'] == 1 ){
		if ( strlen($_POST['post_title']) < 2 || strlen($_POST['post_content']) < 10 ){
			echo '<span class="type-alerta">';
				echo '<ul>';
					if (strlen($_POST['post_title']) < 2 )
						echo '<li>The title is very short.</li>';
					if (strlen($_POST['post_content']) < 10 )
						echo '<li>The content is very short.</li>';
				echo '</ul>';
			echo '</span>';
		}
		else{
			if ( strlen($_POST['post_title']) >= 2 && strlen($_POST['post_content']) >= 10 ){
				$sql_update_post = 'UPDATE web_posts SET 
														post_tags="' . str_replace(' ',',',str_replace($car_esp,$car_hex,$_POST['post_tags'])) . '",
														post_category=' . $_POST['post_category'] . ',
														post_published=' . $_POST['post_pub'] . ',
														post_title="' . str_replace('"', '\\"', str_replace($car_esp,$car_hex,$_POST['post_title'])) . '",
														post_content="' . str_replace('"', '\\"', str_replace($acent,$no_acent,$_POST['post_content'])) . '"
														WHERE post_id=' . $_POST['post_id'] . ' LIMIT 1;
														';
				$res_update_post = exeQuery($sql_update_post);
				echo '<span class="type-exito">Updating post, please wait....</span>';
				echo '<script type="text/javascript" languaje="javascript">window.location.href= "' . INDEX_ADMIN . '?action=optBlog&page=index&adv=1&type=post&update=1";</script>';
				/*$sql_save_post = 'INSERT INTO web_posts VALUES(
															NULL,
															' . $_SESSION['session_user_id'] . ',
															NULL,
															"' . str_replace(' ',',',str_replace($car_esp,$car_hex,$_POST['post_tags'])) . '",
															' . $_POST['post_category'] . ',
															"' . date(Ymd) . '",
															"' . date('Y-m') . '",
															' . $_POST['post_pub'] . ',
															"' . str_replace($car_esp,$car_hex,$_POST['post_title']) . '",
															"' . str_replace($acent,$no_acent,$_POST['post_content']) . '"
															)';
				$res_save_post = exeQuery($sql_save_post);
				echo '<span class="type-exito">Publishing post, please wait....</span>';
				echo '<script type="text/javascript" languaje="javascript">window.location.href= "' . INDEX_ADMIN . '?action=optBlog&page=index&adv=1&type=addPost&published=1";</script>';*/
			}
		}
	}
	if ( $_GET['page'] == 'editPost' && $_GET['idPost'] > 0 && $_GET['idPost'] != '' ){
		$sql_this_post = 'SELECT * FROM web_posts WHERE post_id=' . $_GET['idPost'] . ' LIMIT 1';
		$res_this_post = exeQuery($sql_this_post);
		if ( mysql_num_rows($res_this_post) > 0 ){
			$this_post = mysql_fetch_array($res_this_post);
			echo '<form method="post" name="create_post" action="' . INDEX_ADMIN . '?action=optBlog&page=editPost&idPost=' . $_GET['idPost'] . '">';
			echo '<input type="hidden" name="post_id" value="' . $_GET['idPost'] . '" />';
			echo '<table cellpadding="0" cellspacing="0" border="0">';
				echo '<tr>';
					echo '<td valign="top">';
						echo '<table cellpadding="0" cellspacing="0" border="0">';
							echo '<tr>';
								echo '<td style="width: 50px;">Titulo</td>';
								echo '<td style="width: 10px"></td>';
								echo '<td align="left"><input type="text" name="post_title" class="input-text" style="width: 631px;"';
									if ( $_POST['post_submit'] == 1 )
										echo ' value="' . $_POST['post_title'] . '"';
									else
										echo ' value="' . $this_post['post_title'] . '"';
								echo ' /></td>';
							echo '</tr>';
							echo '<tr><td colspan="3" style="height: 10px;"></td></tr>';
							echo '<tr><td colspan="3"><textarea id="post_content" name="post_content" style="width: 703px; height: 350px;">';
								if ( $_POST['post_submit'] == 1 )
									echo str_replace('\\"','"',$_POST['post_content']);
								else
									echo $this_post['post_content'];
							echo '</textarea></td></tr>';
						echo '</table>';
					echo '</td>';
					echo '<td valign="top">';
						echo '<span class="post-advanced-options">';
							echo '<span class="content">';
								echo '<table cellpadding="0" cellspacing="0" border="0" class="advanced-opt">';
									echo '<tr class="on">';
										echo '<td align="left">Tags</td>';
										echo '<td style="width: 10px"></td>';
										echo '<td align="left"><input type="text" name="post_tags" class="input-text" style="width: 200px;"';
											if ( $_POST['post_submit'] == 1 )
												echo ' value="' . $_POST['post_tags'] . '"';
											else
												echo ' value="' . $this_post['post_tags'] . '"';
										echo ' /></td>';
									echo '</tr>';
									echo '<tr>';
										echo '<td align="left">Category</td>';
										echo '<td style="width: 10px"></td>';
										echo '<td>';
										echo '<select name="post_category" class="input-text" style="width: 212px;">';
											//echo '<option value="NULL"> ---</option>';
											$sql_select_category = 'SELECT category_id,category_name FROM web_categories ORDER BY category_id DESC';
											$res_select_category = exeQuery($sql_select_category);
											if ( mysql_num_rows($res_select_category) > 0 ){
												while ( $select_category = mysql_fetch_array($res_select_category) ){
													echo '<option value="' . $select_category['category_id'] . '"';
														if ( $_POST['post_submit'] == 1 ){
															if ( $_POST['post_category'] == $select_category['category_id'] )
																echo ' selected';
														}
														else{
															if ( $select_category['category_id'] == $this_post['post_category'] )
																echo ' selected';
														}
													echo '>';
														echo $select_category['category_name'];
													echo '</option>';
												}
											}
											else
												echo '<option disabled="disabled" value="NULL">No categories found</option>';
										echo '</select>';
										echo '</td>';
									echo '</tr>';
									echo '<tr class="on">';
										echo '<td align="left">Published</td>';
										echo '<td style="width: 10px"></td>';
										echo '<td align="left">';
										echo '<script type="text/javascript" src="../data/js/moo.check.js"></script>';
											echo '<table cellpadding="0" cellspacing="0" border="0">';
												echo '<tr>';
													echo '<td style="width: 15px"><input type="radio" name="post_pub" value="1"';
														if ( $_POST['post_submit'] == 1 ){
															if ( $_POST['post_pub'] == 1 )
																echo ' checked="checked"';
														}
														else{
															if ($this_post['post_published'] == 1 )
																echo ' checked="checked"';
														}
													echo ' /></td>';
													echo '<td>Yes</td>';
													echo '<td style="width: 20px"></td>';
													echo '<td style="width: 15px"><input type="radio" name="post_pub" value="0"';
														if ( $_POST['post_submit'] == 1 ){
															if ( $_POST['post_pub'] == 0 )
																echo ' checked="checked"';
														}
														else{
															if ($this_post['post_published'] == 0 )
																echo ' checked="checked"';
														}
													echo ' /></td>';
													echo '<td>No</td>';
												echo '<tr>';
											echo '</table>';
										echo '</td>';
									echo '</tr>';
									//echo '<tr colspan="3" style="height: 10px;"></tr>';
									echo '<tr>';
										echo '<td align="left" colspan="3">';
											echo 'Actions <br />';
											echo '<button type="submit" class="submit-button" name="post_submit" value="1"><span class="in-submit-left"><span class="in-submit-right">Save Changes</span></span></button>';
											echo '<button type="reset" class="submit-button" name="post_submit" value="0"><span class="in-submit-left"><span class="in-submit-right">Reset</span></span></button>';
											echo '<a href="javascript:void(0);" onClick="trash_post(\'' . INDEX_ADMIN . '?action=optBlog&page=index\'); return false;" class="submit-button"><span class="in-submit-left"><span class="in-submit-right">Cancel</span></span></button>';
										echo '</td>';
									echo '</tr>';
								echo '</table>';
							echo '</span>';
						echo '<span>';
					echo '</td>';
				echo '</tr>';
			echo '</table>';
			echo '</form>';
		}
	}
	else
		echo '<span class="type-error">No post id send.</span>';
?>
