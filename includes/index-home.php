<?php
	echo '<table cellpadding="0" cellspacing="0" border="0">';
		echo '<tr>';
			echo '<td valign="top" rowspan="3">';
				echo '<div id="main-content">';
				
					switch($_GET['call']){
						case 'category':
							include_once 'index-category-post.php';
							break;
						case 'show_post':{
							include_once 'index-this-posts.php';
							break;
						}
						default:{
							include_once 'index-all-posts.php';
							break;
						}
					}
				echo '</div>';
			echo '</td>';
			echo '<td style="width: 10px; background-image: url(../images/bgs/bg-content-left-archives.png); background-repeat: repeat-y; background-position: 0px 0px;" valign="top"><img src="../images/bdr/bdr-top-margin.png" /></td>';
			// Celda central
			echo '<td valign="top" rowspan="3"><span class="center-content"><span class="write-space">';
				
					// Inicio comentarios
					$sql_comments_autors = 'SELECT com_autor,com_id,com_post,com_date_created FROM web_comments ORDEr BY com_id DESC LIMIT 15';
					$res_comments_autors = exeQuery($sql_comments_autors);
					if ( mysql_num_rows($res_comments_autors) > 0 ){
						echo '<span class="table-title"><span>Comentarios recientes</span></span>';
						echo '<span class="list-links">';
						while ( $comments_autors = mysql_fetch_array($res_comments_autors) ){
							echo '<a href="' . INDEX . '?option=index&call=show_post&post_id=' . $comments_autors['com_post'] . '#comment-id=' . $comments_autors['com_id'] . '">';
								echo '<span>' . substr(date_month($comments_autors['com_date_created']),0,3) . ' ' . date_day($comments_autors['com_date_created']) . ' &#124; </span>';
								echo substr($comments_autors['com_autor'],0,20);
								if ( strlen($comments_autors['com_autor']) > 20 ){
									echo '&#133;';
								}
							echo '</a>';
						}
						//echo '<a href="javascript: void(0);"><span>09 Feb 13 &#124; </span>Saul</a>';
						echo '</span>';
					}
					// Fin comentarios
					
					//inicio titulos de posts
					$sql_title_posts = 'SELECT post_id,post_title,post_date_created FROM web_posts ORDER BY post_id DESC LIMIT 15';
					$res_title_posts = exeQuery($sql_title_posts);
					if ( mysql_num_rows($res_title_posts) > 0 ){
						echo '<span class="table-title"><span>Posts recientes</span></span>';
						echo '<span class="list-links">';
							while ( $title_posts = mysql_fetch_array($res_title_posts) ){
								echo '<a href="' . INDEX . '?option=index&call=show_post&post_id=' . $title_posts['post_id'] . '">';
									echo '<span>' . substr(date_month($title_posts['post_date_created']),0,3) . ' ' . date_day($title_posts['post_date_created']) . ' &#124; </span>';
									echo substr($title_posts['post_title'],0,20);
									if ( strlen($title_posts['post_title']) > 20 ){
									echo '&#133;';
								}
								echo '</a>';
							}
						echo '</span>';
					}
					//fin titulos de post
					
					// Inicio archivos de Blog
						$sql_blog_archive = 'SELECT DISTINCT post_year_month FROM web_posts ORDER BY post_year_month DESC';
						$res_blog_archive = exeQuery($sql_blog_archive);
						if ( mysql_num_rows($res_blog_archive) > 0 ){
							// Inicio archivos de Blog
							echo '<span class="table-title"><span>Archivos de Blog</span></span>';
							echo '<span class="list-links">';
							while ( $blog_archive = mysql_fetch_array($res_blog_archive) ){
								echo '<div class="menuheaders"><a href="javascript:void(0);" title="' . date_year($blog_archive['post_year_month']) . ' &#124; ' . date_month($blog_archive['post_year_month']) . ' "><span>' . date_year($blog_archive['post_year_month']) . ' </span>' . date_month($blog_archive['post_year_month']) . '</a></div>';
								echo '<ul class="menucontents">';
									$sql_date_archives = 'SELECT post_id,post_title,post_date_created FROM web_posts WHERE post_year_month="' . $blog_archive['post_year_month'] . '" ORDER BY post_date_created DESC';
									$res_date_archives = exeQuery($sql_date_archives);
									while ( $date_archives = mysql_fetch_array($res_date_archives) ){
										echo '<li><a href="' . INDEX . '?option=index&call=show_post&post_id=' . $date_archives['post_id'] . '"><span>' . date_day($date_archives['post_date_created']) . ' &#124; </span>' . substr($date_archives['post_title'],0,20);
											if ( strlen($date_archives['post_title']) > 20 )
												echo '&#133;';
										echo '</a></li>';
									}
								echo '</ul>'; 
							}
							echo '<span>';
						}
					//Fin archivos de blog
					
			echo '</span></span></td>';
			echo '<td style="width: 10px; background-image: url(../images/bgs/bg-content-right-archives.png); background-repeat: repeat-y; background-position: 0px 0px;" valign="top"><img src="../images/bdr/bdr-top-margin.png" /></td>';
			echo '<td valign="top"><span class="right-content">';
				
				// Inicio categorias
				$sql_show_categories = 'SELECT category_id,category_name,category_description FROM web_categories ORDER BY category_name ASC';
				$res_show_categories = exeQuery($sql_show_categories);
				if ( mysql_num_rows($res_show_categories) > 0 ){
					echo '<span class="table-title"><span>Categorias</span></span>';
					echo '<span class="list-links mylinks">';
						while ( $show_categories = mysql_fetch_array($res_show_categories) ){
							echo '<a href="' . INDEX . '?option=index&call=category&category=' . $show_categories['category_id'] . '" title="' . $show_categories['category_description'] . '">';
								echo '<span>';
									$sql_num_this = 'SELECT post_id FROM web_posts WHERE post_category=' . $show_categories['category_id'] . '';
									$res_num_this = exeQuery($sql_num_this);
									if ( mysql_num_rows($res_num_this) < 10 )
										echo '0'.mysql_num_rows($res_num_this);
									else
										echo mysql_num_rows($res_num_this);
								echo ' &#124; </span>';
								echo $show_categories['category_name'];
							echo '</a>';
						}
					echo '</span>';
				}
				// Fin categorias
				
				//Acerca de mi
				/*echo '<span class="table-title"><span>Acerca de mi</span></span>';
				echo '<span class="about-me">';
					echo '<img src="../images/img/arkosnoemarenom.jpg" style="float: left; padding-right: 2px; margin-right: 5px; margin-bottom: 5px; border-right: 1px solid #f0f0f0;" />';
					echo '<b>Arkos Noem Arenom :</b><br />';
					echo 'Que tal, soy el Jorge alias el arkos, el por que de mi psedonimo es algo muy simple, perteneci a una banda de metal-gotico llamada Sadness Tears en la cual fui bautizado como el Arkos Noem Arenom.<br /><br />Despues de el desplante que tuve con la banda regrese a mis estudios dentro de la UAM, aunque no estoy muy agusto con ello. Estudio la carrera de Ingenier&iacute;a Electr&oacute;nica, area de concentraci&oacute;n computaci&oacute;n, actualmente soy participe de diversos <a href="#bottom">proyectos</a> de desarrollo web. Pero sigo en la carretera de la musica. Soy miembro activo del CEUAMI, aunque ya me queda poco tiempo con ellos.';
				echo '</span>';*/
				//Fin acerca de mi
				
				//Galerias recientes
				$sql_rec_gallery = 'SELECT gallery_id,gallery_name,gallery_date_created FROM web_gallery ORDER BY gallery_date_created DESC LIMIT 10';
				$res_rec_gallery = exeQuery($sql_rec_gallery);
				if ( mysql_num_rows($res_rec_gallery) > 0 ){
					echo '<span class="table-title"><span>Galerias recientes</span></span>';
					echo '<span class="list-links">';
					while ( $rec_gallery = mysql_fetch_array($res_rec_gallery) ){
						$sql_num_archives_gallery = 'SELECT * FROM web_image_gallery WHERE image_gallery=' . $rec_gallery['gallery_id'] . ';';
						$res_num_archives_gallery = exeQuery($sql_num_archives_gallery);
						echo '<a href="' . INDEX . '?option=gallery&call=show_gallery&gallery_id=' . $rec_gallery['gallery_id'] . '"><span>';
						if ( mysql_num_rows($res_num_archives_gallery) < 10 )
							echo '0' . mysql_num_rows($res_num_archives_gallery);
						else
							echo  mysql_num_rows($res_num_archives_gallery);
						echo ' &#124; </span>' . $rec_gallery['gallery_name'] . '</a>';
					}
					echo '</span>';
				}
				//Fin galerias recientes
				
				// Links de interes
				$sql_interest_links = 'SELECT roll_title,roll_url,roll_description FROM web_blog_roll ORDER BY roll_title ASC;';
				$res_interest_links = exeQuery($sql_interest_links);
				if ( mysql_num_rows($res_interest_links) > 0 ){
					echo '<span class="table-title"><span>Links de interes</span></span>';
					echo '<span class="list-links mylinks">';
						while ( $interest_links = mysql_fetch_array($res_interest_links) ){
							echo '<a href="' . $interest_links['roll_url'] . '" title=":: &lt;b&gt;' . $interest_links['roll_title'] . '&lt;/b&gt;&lt;br /&gt;:: ' . $interest_links['roll_description'] . '&lt;br /&gt;:: ' . $interest_links['roll_url']  . '" target="_blank"><span>:: </span>' . substr($interest_links['roll_title'],0,30) . ((strlen($interest_links['roll_title']) > 30)? '&#133;':'') . '</a>';
						}
					echo '</span>';
				}
				//Fin links de interes
			echo '</span></td>';
		echo '</tr>';
		echo '<tr>';
			echo '<td style="width: 10px; background-image: url(../images/bgs/bg-content-left-archives.png); background-repeat: repeat-y; background-position: 0px 0px;" valign="bottom"><img src="../images/bdr/bdr-bottom-margin.png" /></td>';
			echo '<td style="width: 10px; background-image: url(../images/bgs/bg-content-right-archives.png); background-repeat: repeat-y; background-position: 0px 0px;" valign="bottom"><img src="../images/bdr/bdr-bottom-margin.png" /></td>';
		echo '</tr>';
	echo '</table>';
?>