<?php

	echo '<span class="index-content">';
		echo '<div id="traza">';
			echo '<a href="' . INDEX_ADMIN . '" class="item">Dashboard</a>';
			echo '<span class="middle"></span>';
			echo '<a href="' . INDEX_ADMIN . '?action=optImages" class="item">Images</a>';
			if ( isset($_GET['page']) && $_GET['page'] != '' ){
				switch($_GET['page']){
					case 'postUploaded':
						$b_link = 'postUploaded';
						$b_title = 'Post Uploaded';
						break;
					case 'gallery':
						$b_link = 'gallery';
						$b_title = 'Galleries';
						break;
					case 'upload':
						$b_link = 'upload';
						$b_title = 'Upload images';
						break;
				}
				echo '<span class="middle"></span>';
				echo '<a href="' . INDEX_ADMIN . '?action=optImages&page=' . $b_link . '" class="item">' . $b_title . '</a>';
				echo '<span class="right"></span>';
			}
			else{
				echo '<span class="right"></span>';
			}
		echo '</div>';
		// Advertices function
		admin_advertices();
	echo '</span>';

	echo '<span class="page-actions">';
		echo '<a href="' . INDEX_ADMIN . '?action=optImages&page=postUploaded"';
			if ( $_GET['page'] == '' || !isset($_GET['page']) || $_GET['page'] == 'postUploaded' )
				echo ' class="this"';
		echo '><span class="menu edit-posts">Post Uploaded</span></a>';
		echo '<a href="' . INDEX_ADMIN . '?action=optImages&page=gallery"';
			if ( $_GET['page'] == 'gallery' )
				echo ' class="this"';
		echo '><span class="menu add-posts">Galleries</span></a>';
	echo '</span>';

	echo '<span class="index-content">';
	
		switch($_GET['page']){
			case 'gallery':
				include_once 'admin-image-gallery.php';
				break;
			case 'upload':
				include_once 'admin-image-post-upload.php';
				break;
			default:{
				include_once 'admin-image-post-upload.php';
			}
		}
	
	echo '</span>';

?>
