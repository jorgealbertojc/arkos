<?php

echo '<table cellpadding="0" cellspacing="0" border="0" class="blog-entry">';
			echo '<thead>';
				echo '<tr>';
					echo '<td><span style="display: block; width: 30px;">&nbsp;</span></td>';
					echo '<td><span style="display: block; width: 450px;">Name/Description</span></td>';
					echo '<td><span style="display: block; width: 130px;">Autor</span></td>';
					echo '<td><span style="display: block; width: 130px;">Date</span></td>';
					echo '<td><span style="display: block; width: 30px;"><img src="../images/icn/admin-16-splash-category-icn.png" alt="Counter" title="Counter" /></span></td>';
					echo '<td><span style="display: block; width: 30px;"><img src="../images/icn/admin-16-general-public.png" alt="public/private" title="public/private" /></span></td>';
					echo '<td colspan="3"><span style="display: block; width: 150px;">Actions</span></td>';
				echo '</tr>';
			echo '</thead>';
			echo '<tbody>';
			
				// Consulta para saber cuales son las galerias
				$sql_galleries = 'SELECT * FROM web_gallery ORDER BY gallery_date_created DESC';
				$res_galleries = exeQuery($sql_galleries);
				
				if ( mysql_num_rows($res_galleries) > 0 ){
					$class_switch = 0;
					while ( $galleries = mysql_fetch_array($res_galleries) ){
						echo '<tr class="';
							if ( $class_switch == 0 ){
								echo 'on';
								$class_switch = 1;
							}
							else{
								if ( $class_switch == 1 ){
									echo 'off';
									$class_switch =0;
								}
							}
						echo '">';
							// Id gallery
							echo '<td align="center"><span style="display: block; width: 30px;">' . $galleries['gallery_id'] . '</span></td>';
							
							//Name description gallery
							echo '<td><span style="display: block; width: 450px;">';
								echo '<span class="gallery-title"><a href="' . INDEX_ADMIN . '?action=optImages&page=gallery&site=viewGallery&idGallery=' . $galleries['gallery_id'] . '">' . $galleries['gallery_name'] . '</a></span>';
								echo '<span class="gallery-descrition">';
									echo substr(nl2br($galleries['gallery_description']),0,300);
									if ( strlen($galleries['gallery_description'])  > 300 )
										echo '&#133;';
								echo '</span>';
							echo '</span></td>';
							
							// Autor gallery
							echo '<td><span style="display: block; width: 130px;">' . user_id($_SESSION['session_user_id']) . '</span></td>';
							
							//Date creation gallery
							echo '<td><span style="display: block; width: 130px;">';
								echo substr(date_month($galleries['gallery_date_created']),0,3) . ' ' . date_day($galleries['gallery_date_created']) . ' &#124; ' . date_year($galleries['gallery_date_created']);
							echo '</span></td>';
							
							// Counter gallery files
							echo '<td>';
								//Consulta a la tabla de imagenes
								$sql_gallery_images = 'SELECT image_id FROM web_image_gallery WHERE image_gallery=' . $galleries['gallery_id'] . ';';
								$res_gallery_images = exeQuery($sql_gallery_images);
								if ( mysql_num_rows($res_gallery_images) < 100 ){
									echo '<span style="display: block; width: 30px;" title="' . mysql_num_rows($res_gallery_images) . ' photos in this gallery">';
									if ( mysql_num_rows($res_gallery_images) < 10 )
										echo '&nbsp;&nbsp;' . mysql_num_rows($res_gallery_images);
									else	
										echo '&nbsp;' . mysql_num_rows($res_gallery_images);
									}
							echo '</span></td>';
							
							// Public/Private gallery
							echo '<td><span style="display: block; width: 30px;"><a href="publish-gallery.php?action=changePubMode&idGallery=' . $galleries['gallery_id'] . '"><img src="../images/icn/';
								if ( $galleries['gallery_published'] )
									echo 'admin-16-green-ball.png';
								else
									echo 'admin-16-red-ball.png';
							echo '" alt="public/private" title="public/private" style="border: 0px;" /></a></span></td>';
							
							//Actions gallery
							echo '<td align="center"><span style="display: block; width: 40px;"><a href="#mb_edit_gallery_' . $galleries['gallery_id'] . '"  rel="lightbox[inline 580 320]"><img src="../images/icn/admin-16-edit-icn.png" style="border: 0px;" title="Edit Gallery" /></a></span>';
								echo '<div id="mb_edit_gallery_' . $galleries['gallery_id'] . '" style="display: none;">';
									echo '<span class="add-category">';
										echo '<form method="post" action="' . INDEX_ADMIN . '?action=optImages&page=gallery&site=' . $_GET['site'] . '">';
											echo '<input type="hidden" name="gallery_update_id" value="' . $galleries['gallery_id'] . '" />';
											echo '<h2 class="admin">Update gallery : ' . $galleries['gallery_name'] . '</h2>';
											echo '<table cellpadding="0" cellspacing="0" border="0">';
												echo '<tr>';
													echo '<td><span style="display: block; width: 100px;">Gallery Name:</span></td>';
													echo '<td>';
														echo '<input type="text" class="input-text" style="width: 400px;" name="gallery_update_name" value="' . $galleries['gallery_name'] . '" />';
													echo '</td>';
												echo '</tr>';
												echo '<tr><td colspan="2" style="height: 10px;"></td></tr>';
												echo '<tr>';
													echo '<td valign="top"><span style="display: block; width: 100px;">Gallery Description:</span></td>';
													echo '<td>';
														echo '<textarea class="input-text" style="width: 400px; height: 150px;" name="gallery_update_description">';
															echo $galleries['gallery_description'];
														echo '</textarea>';
													echo '</td>';
												echo '</tr>';
												echo '<tr><td colspan="2" style="height: 10px;"></td></tr>';
												echo '<tr><td colspan="2" align="right">';
													echo '<button type="submit" class="submit-button" name="update_gallery" value="true"><span class="in-submit-left"><span class="in-submit-right">Update Gallery</span></span></button>';
													echo '<button type="reset" class="submit-button" name="update_gallery" value="false"><span class="in-submit-left"><span class="in-submit-right">Reset</span></span></button>';
													echo '<a href="javascript:void(0);" onClick="Mediabox.close();" class="submit-button"><span class="in-submit-left"><span class="in-submit-right">Cancel</span></span></button>';
												echo '</td></tr>';
											echo '</table>';
										echo '</form>';
									echo '</span>';
								echo '</div>';
							echo '</td>';
							echo '<td align="center"><span style="display: block; width: 40px;"><a href="' . INDEX_ADMIN . '?action=optImages&page=gallery&site=upload&idGallery=' . $galleries['gallery_id'] . '"><img src="../images/icn/admin-transfer-file.png" title="Upload images" style="border: 0px;" /></a></span></td>';
							echo '<td align="center"><span style="display: block; width: 40px;"><a href="javascript:void(0);" onClick="delete_gallery(\'' . $galleries['gallery_dir'] . '\',\'' . $galleries['gallery_id'] . '\',\'' . $galleries['gallery_name'] . '\');"><img src="../images/icn/admin-16-cross.png" style="border: 0px;" title="Delete Gallery" /></a></span></td>';
						echo '</tr>';
					}
				}
				else{
					echo '<tr class="off">';
						echo '<td colspan="9" align="center">No gallery found</td>';
					echo '</tr>';
				}
			echo '</tbody>';
		echo '</table>';
?>
