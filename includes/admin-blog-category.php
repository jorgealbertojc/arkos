<?php

	if ( $_POST['category_edit_submit'] == 1 ){
		if ( strlen($_POST['category_edit_name']) < 3 || strlen($_POST['category_edit_description']) < 10 ){
			echo '<span class="type-error"><ul>';
				if ( strlen($_POST['category_edit_name']) < 5 )
					echo '<li>Category name is very short.</li>';
				if ( strlen($_POST['category_edit_description']) < 10 )
					echo '<li>Category description is very short.</li>';
			echo '</ul></span>';
		}
		else{
			if ( strlen($_POST['category_edit_name']) >= 3 && strlen($_POST['category_edit_description']) >= 10 ){
				$sql_update_category = 'UPDATE web_categories SET 
																category_name="' . str_replace($car_esp,$car_hex,$_POST['category_edit_name']) . '",
																category_description="' . str_replace($car_esp,$car_hex,$_POST['category_edit_description']) . '" 
																WHERE category_id=' . $_POST['category_edit_id'] . ';';
				$res_update_category = exeQuery($sql_update_category);
				echo '<span class="type-info">Saving category, please wait...</span>';
				echo '<script type="text/javascript">window.location.href=\'' . INDEX_ADMIN . '?action=optBlog&page=category&adv=1&type=category&opt=update&res=1\';</script>';
			}
		}
	}

	//A�adir categoria
	if ( $_POST['category_submit'] == 1 ){
		if ( strlen($_POST['category_name']) < 3 || strlen($_POST['category_description']) < 10 ){
			echo '<span class="type-error"><ul>';
				if ( strlen($_POST['category_name']) < 5 )
					echo '<li>Category name is very short.</li>';
				if ( strlen($_POST['category_description']) < 10 )
					echo '<li>Category description is very short.</li>';
			echo '</ul></span>';
		}
		else{
			if ( strlen($_POST['category_name']) >= 3 && strlen($_POST['category_description']) >= 10 ){
				$sql_save_category = 'INSERT INTO web_categories VALUES(
																		NULL,
																		' . $_SESSION['session_user_id'] . ',
																		NULL,
																		"' . str_replace($car_esp,$car_hex,$_POST['category_name']) . '",
																		"' . str_replace($car_esp,$car_hex,$_POST['category_description']) . '"
																		)';
				exeQuery($sql_save_category);
				echo '<span class="type-info">Saving category, please wait...</span>';
				echo '<script type="text/javascript">window.location.href=\'' . INDEX_ADMIN . '?action=optBlog&page=category&adv=1&type=category&opt=published&res=1\';</script>';
			}
		}
	}
	// End a�adir categoria
	echo 'Actions : <a href="#mb_inline" rel="lightbox[inline 440 310]" title="Add category" class="addCategory"><span>Add Category</span></a>';
	
	echo '<div id="mb_inline">';
	echo '<span class="add-category">';
	echo '<b>Add category</b>';
	echo '<form method="post" action="' . INDEX_ADMIN . '?action=optBlog&page=category" style="display: block; margin-top: 10px;">';
		echo '<table cellpdding="0" cellspacing="0" border="0">';
			echo '<tr>';
				echo '<td style="width: 80px;">Name:</td>';
				echo '<td style="width: 10px;"></td>';
				echo '<td><input type="text" class="input-text" style="width: 300px;" name="category_name"';
					if ( $_POST['category_submit'] == 1 )
						echo ' value="' . $_POST['category_name'] . '"';
				echo ' /></td>';
			echo '</tr>';
			echo '<tr><td colspan="3" style="height: 10px;"></td></tr>';
			echo '<tr>';
				echo '<td valign="top">Description:</td>';
				echo '<td style="width: 10px;"></td>';
				echo '<td><textarea class="input-text" style="width: 300px; height: 150px;" name="category_description">';
					if ( $_POST['category_submit'] == 1 )
						echo $_POST['category_description'];
				echo '</textarea></td>';
			echo '</tr>';
			echo '<tr><td colspan="3" style="height: 10px;"></td></tr>';
			echo '<tr><td colspan="3" align="right">';
				echo '<button class="submit-button" type="submit" name="category_submit" value="1"><span class="in-submit-left"><span class="in-submit-right">Save category</span></span></button>';
				echo '<button class="submit-button" type="reset" name="category_submit" value="0"><span class="in-submit-left"><span class="in-submit-right">Reset</span></span></button>';
				echo '<a href="javascript:void(0);" onClick="Mediabox.close();" class="submit-button"><span class="in-submit-left"><span class="in-submit-right">Cancel</span></span></button>';
			echo '</td></tr>';
		echo '</table>';
	echo '</form>';
	echo '</div>';
echo '<table cellpadding="0" cellspacing="0" border="0" class="blog-entry">';
	echo '<thead>';
		echo '<tr>';
			echo '<td align="center"><span style="display: block; width: 50px;">#</span></td>';
			echo '<td><span style="display: block; width: 638px;">Category</span></td>';
			echo '<td><span style="display: block; width: 60px;">Autor</span></td>';
			echo '<td><span style="display: block; width: 100px;">Date</span></td>';
			echo '<td colspan="2"><span style="display: block; width: 124px;">Actions</span></td>';
		echo '</tr>';
	echo '</thead>';
	echo '<tbody>';
	
		$sql_list_category = 'SELECT * FROM web_categories ORDER BY category_date_created ASC';
		$res_list_category = exeQuery($sql_list_category);
		if ( mysql_num_rows($res_list_category) > 0 ){
			$class_theme = 0;
			while ( $list_category = mysql_fetch_array($res_list_category) ){
				echo '<tr class="';
							if ( $class_theme == 0 ){
								$class_theme = 1;
								echo 'on';
							}
							else{
								if ( $class_theme == 1 ){
									$class_theme = 0;
									echo 'off';
								}
							}
				echo '">';
					
					//ID
					echo '<td align="center">' . $list_category['category_id'] . '</td>';
					
					// Category and Description
					echo '<td valign="top">';
						echo '<b>' . $list_category['category_name'] . ':</b><br />';
						echo nl2br($list_category['category_description']) . '<br /><br />';
					echo '</td>';
					
					//Autor
					echo '<td>' . user_id($list_category['category_user']) . '</td>';
					
					//Date
					echo '<td>';
						echo date_month($list_category['category_date_created']) . ' ' . date_day($list_category['category_date_created']) . ' &#124; ' . date_year($list_category['category_date_created']);
					echo '</td>';
					
					//Edit
					echo '<td align="center"><a href="#mb_category_id_' . $list_category['category_id']. '" rel="lightbox[inline 440 310]" title="hola"><img src="../images/icn/admin-16-page_edit.png" style="border: 0px;" title="Edit Category" /></a>';
					echo '<div id="mb_category_id_' . $list_category['category_id'] . '" style="display: none;">';
						echo '<span class="add-category">';
							echo 'Edit category : <b>' . $list_category['category_name'] . '</b>';
							echo '<form method="post" action="' . INDEX_ADMIN . '?action=optBlog&page=category" style="display: block; margin-top: 10px;">';
							echo '<input type="hidden" name="category_edit_id" value="' . $list_category['category_id']. '" />';
							echo '<table cellpdding="0" cellspacing="0" border="0">';
								echo '<tr>';
									echo '<td style="width: 80px;">Name:</td>';
									echo '<td style="width: 10px;"></td>';
									echo '<td><input type="text" class="input-text" style="width: 300px;" name="category_edit_name" value="' . $list_category['category_name'] . '" /></td>';
								echo '</tr>';
								echo '<tr><td colspan="3" style="height: 10px;"></td></tr>';
								echo '<tr>';
									echo '<td valign="top">Description:</td>';
									echo '<td style="width: 10px;"></td>';
									echo '<td><textarea class="input-text" style="width: 300px; height: 150px;" name="category_edit_description">' . $list_category['category_description'] . '</textarea></td>';
								echo '</tr>';
								echo '<tr><td colspan="3" style="height: 10px;"></td></tr>';
								echo '<tr><td colspan="3" align="right">';
									echo '<button class="submit-button" type="submit" name="category_edit_submit" value="1"><span class="in-submit-left"><span class="in-submit-right">Save category</span></span></button>';
									echo '<button class="submit-button" type="reset" name="category_edit_submit" value="0"><span class="in-submit-left"><span class="in-submit-right">Reset</span></span></button>';
									echo '<a href="javascript:void(0);" onClick="Mediabox.close();" class="submit-button"><span class="in-submit-left"><span class="in-submit-right">Cancel</span></span></button>';
								echo '</td></tr>';
							echo '</table>';
						echo '</form>';
							
						echo '</span>';
					echo '<div>';
					echo '</td>';
					// Delete
					echo '<td align="center"><a href="javascript:void(0);" onclick="delete_category(\'' . $list_category['category_id'] . '\',\'' . $list_category['category_name'] . '\');return false;"><img src="../images/icn/admin-16-cross.png" title="Delete Category" style="border: 0px;" /></a></td>';
					
				echo '</tr>';
			}
		}
		else{
			echo '<tr class="off"><td colspan="6" align="center">No categories found.</td></tr>';
		}
	
	echo '<tbody>';
echo '</table>';

?>