<?php

					echo '</span>';
					//Fin del espacio que puede ser utilizado para escribir
					echo '<span class="bottom-margin"></span>';
				echo '</div>';
				//Fin del bloque de contenido
				
				//Inicia bloque de footer
				echo '<div id="footer">';
					echo '<table cellpadding="0" cellspacing="0" border="0">';
						echo '<tr>';
							echo '<td valign="top">';
								// Primer bloque
								echo '<span class="bloque">';
									echo '<h3>Navegaci&oacute;n</h3>';
									echo '<ul class="explorer">';
										echo '<li><a href="' . INDEX . HOME . '">Home / Index</a></li>';
										echo '<li><a href="' . INDEX . GALLERY . '">Galerias</a></li>';
										echo '<li><a href="' . INDEX . PORTFOLIO . '">Portfolio</a></li>';
										echo '<li><a href="' . INDEX . CONTACT . '">Contacto</a></li>';
									echo '</ul>';
								echo '</span>';
								// Fin primer bloque
							echo '</td>';
							echo '<td valign="top">';
								// Segundo bloque
								echo '<span class="bloque">';
									echo '<h3>Proyectos</h3>';
									echo '<ul class="explorer">';
										//echo '<li><a href="javascript:void(0);">[CEUAMI] <i>(Capitulo Estudiantil de Ciencias de la C&#133;)</i></a></li>';
										echo '<li><a href="javascript:void(0);">SIA - UAM <i>(Learning Management System)</i></a></li>';
										echo '<li><a href="javascript:void(0);">Geiser <i>(Gestion de Servicios)</i></a></li>';
										echo '<li><a href="javascript:void(0);">Leaf Project CMS <i>(Content Management System)</i></a></li>';
										echo '<li><a href="javascript:void(0);">[EMIA] <i>(FarenTec Corp.)</i></a></li>';
									echo '</ul>';
								echo '</span>';
								// Fin segundo bloque
							echo '</td>';
							echo '<td valign="top">';
								// Tercer bloque
								echo '<span class="bloque">';
									echo '<img src="../images/icn/twitter.png" style="float: right;" />';
									echo '<h3>Mi Twitter</h3>';
										echo 'Sigueme en twiter<br /><a href="http://www.twitter.com/arkosnoemarenom" target="_blank">Arkos Noem Arenom\'s Twitter</a><br /><br />';
									echo '<img src="../images/icn/opera.png" style="float: right;" />';
									echo '<h3>Mejor Opera</h3>';
									echo 'Para visualizar correctamente este sitio usa Opera Web Browser';
								echo '</span>';
								// Fin Tercer bloque
							echo '</td>';
						echo '</tr>';
					echo '</table>';
				echo '</div>';
				//Bloque de termino de footer
				echo '<div id="footer-bottom">';
					echo '<span class="little-info">&copy;2009 Arkos Noem Arenom <span class="global-black-char">&#124;</span> ';
					if ( isset($_SESSION['session_user_id']) && isset($_SESSION['session_user_name']) ){
						echo '<a href="' . INDEX_ADMIN . '">Site Admin</a> <span class="global-black-char">&#124;</span> <a href="' . INDEX . 'logout.php">Logout</a>';
					}
					else{
						echo '<a href="' . INDEX_ADMIN . '">Login</a></span>';
					}
				echo '</div>';
				// Fin de bloque de termino de footer
				//Termina bloque de footer
			echo '</div>';
		echo '</div>';
		echo '<a name="bottom"></a>';
?>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-11417082-1");
pageTracker._trackPageview();
} catch(err) {}</script>
<?
		echo '</body>';
	echo '</html>';
?>
