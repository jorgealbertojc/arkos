<?php
	session_start();
	echo '<html>';
		echo '<head>';
			echo '<title>Login Admin :: ' . SITE_NAME . '</title>';
			echo '<link rel="alternate" media="screen" type="application/rss+xml" href="http://' . $_SERVER['SERVER_NAME'] . INDEX . '../feed/" title="RSS 2.0 - All" />';
			echo '<link rel="shortcut icon" href="../data/favicon/favicon.ico">';
			echo '<link rel="stylesheet" type="text/css" href="../data/css/admin-login-site.css" />';
			echo '<script type="text/javascript" src="../data/js/jquery-1.3.2.js"></script>';
			echo '<script type="text/javascript">$(document).ready(function(){setTimeout(function(){ $(".messages").fadeOut(2000);}, 8000);});</script>';
		echo '</head>';
		echo '<body>';
		echo '<table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%">';
			echo '<tr><td align="center" valign="middle">';
				if ( $_GET['adv'] == 1 ){
					echo '<span style="display: block; width: 600px;">';
						switch($_GET['type']){
							case 'passwd':
								if ( $_GET['send'] == 1 ){
									$get_type = 'exito';
									$get_message = 'A new password has been sent to your email. Please check your inbox.';
								}
								else{
									if ( $_GET['send'] == 0 ){
										$get_type = 'alerta';
										$get_message = 'The new password can\'t be send to your email, please contact to the Web administrator.';
									}
								}
								break;
							case 'session':
								$get_type = 'exito';
								$get_message = 'Now you are logged out.';
								break;
						}
						echo '<span class="type-' . $get_type . ' messages">' . $get_message . '</span>';
					echo '</span>';
				}
				echo '<span class="body-login"><span class="login">';
					echo '<span class="login-banner"></span>';
					switch($_GET['option']){
						case 'lostPasswd':
							include_once 'admin-session-login-passwd.php';
							break;
						default:
							include_once 'index-session-login.php';
					}
				echo '</span></span>';
			echo '</td></tr>';
		echo '</table>';
		echo '</body>';
	echo '</html>';
?>