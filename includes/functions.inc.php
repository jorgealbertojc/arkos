<?php
	
	// Funcion para conectarse al servidor MySQL
	function connect(){
		global $mysql_config;
		$idcnx=mysql_connect($mysql_config['mysql-server'],$mysql_config['mysql-user'],$mysql_config['mysql-passwd']) or DIE(mysql_error());
		mysql_select_db($mysql_config['mysql-db'],$idcnx) or DIE(mysql_error());
		return $idcnx;
	}
	
	// funcion para ejecutar los comandos SQL
	function exeQuery($query){
		$res=mysql_query($query) or DIE(mysql_error());
		return $res;
	}
	
	// Funcion que regresa los datos del sitio en un arreglo
	function site_info(){
		$idcnx_info = connect();
		$sql_info = 'SELECT * FROM web_site_info LIMIT 1';
		$res_info = exeQuery($sql_info);
		if ( mysql_num_rows($res_info) > 0 )
			return mysql_fetch_array($res_info);
		else
			return array();
		mysql_close($idcnx_info);
	}
	
	////////////////////////////////////////////////////////////////////////////
	// Definicion del nombre del sitio
	////////////////////////////////////////////////////////////////////////////
	$info_site_admin = site_info();
	define(SITE_NAME,' ' . $info_site_admin['site_name']);
	define(SITE_VERSION, $info_site_admin['site_version']);
	define(SITE_CREATED, $info_site_admin['site_date_created']);
	define(SITE_UPDATED, $info_site_admin['site_date_updated']);
	define(SITE_ADMIN_NAME, $info_site_admin['site_admin_name']);
	define(SITE_ADMIN_EMAIL, $info_site_admin['site_admin_email']);
	define(SITE_UPLOAD_DIR,substr(INDEX,0,strrpos(substr(INDEX,0,-1),'/')) . '/uploads/');
	define(SITE_UPLOAD_FILE,SITE_UPLOAD_DIR . 'file');
	define(SITE_UPLOAD_IMG,SITE_UPLOAD_DIR . 'image');
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	
	// Funcion para encontrar el dia en una fecha dada recibe un TIMESTAMP o de la forma yyyy-mm-dd
	function date_day($date){
		$day=substr($date,8,2);
		return $day;
	}
	// Funcion para encontrar el a�o en una fecha dada recibe un TIMESTAMP o de la forma yyyy-mm-dd
	function date_year($date){
		$year=substr($date,0,4);
		return $year;
	}
	
	// Funcion para encontrar la hora en una fecha dada recibe un TIMESTAMP o de la forma yyyy-mm-dd hh:mm:ss
	function date_hour($date){
		$hour=substr($date,11,8);
		return $hour;
	}
	
	// Funcion para encontrar el mes en una fecha dada recibe un TIMESTAMP o de la forma yyyy-mm-dd
	function date_month($date){
		switch((int)substr($date,5,2)){
			case 0:{
				$month="Undefined";
				break;
			}
			case 1:{
				$month="January";
				break;
			}
			case 2:{
				$month="February";
				break;
			}
			case 3:{
				$month="March";
				break;
			}
			case 4:{
				$month="April";
				break;
			}
			case 5:{
				$month="May";
				break;
			}
			case 6:{
				$month="June";
				break;
			}
			case 7:{
				$month="July";
				break;
			}
			case 8:{
				$month="August";
				break;
			}
			case 9:{
				$month="September";
				break;
			}
			case 10:{
				$month="October";
				break;
			}
			case 11:{
				$month="November";
				break;
			}
			case 12:{
				$month="Dicember";
				break;
			}
		}
		return $month;
	}
	
	
		// funcion que comprueba si el correo dado es correcto o no
	function is_email($email){
		$mail_correcto = 0;
		//compruebo unas cosas primeras
		if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@")){
			if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," "))) {
			//miro si tiene caracter .
				if (substr_count($email,".")>= 1){
				//obtengo la terminacion del dominio
				$term_dom = substr(strrchr ($email, '.'),1);
					//compruebo que la terminaci�n del dominio sea correcta
					if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ){
					//compruebo que lo de antes del dominio sea correcto
					$antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1);
					$caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1);
					if ($caracter_ult != "@" && $caracter_ult != "."){
						$mail_correcto = 1;
					}
					}
				}
			}
		}
		if ($mail_correcto)
			return 1;
		else
			return 0;
	}
	
	// Funcion para saber cuanto peso se puede cargar al servidor
    function maxfile_upload(){
		$phpallowuploads = (bool) ini_get('file_uploads');
		if ( $phpallowuploads ){		
			$phpmaxsize = ini_get('upload_max_filesize');
			$phpmaxsize = trim($phpmaxsize);
			$last = strtolower($phpmaxsize{strlen($phpmaxsize)-1});
			switch($last) {
				case 'g':
					$phpmaxsize *= 1024;
				case 'm':
					$phpmaxsize *= 1024;
			}
		}
		else{
			$phpmaxsize = 0;
		}
		return $phpmaxsize;
	}
	
	
	// funcion que imprime los avisos de exito, alertas, etc..
	function advertises(){
		if ( $_GET['adv']==1 ){
			switch($_GET['type']){
				case "comment":{
					if ( $_GET['published'] ){
						$type = "type-exito";
						$message = "Listo... Ya publique tu comentario...  lo puedes revisar abajo.";
					}
					else{
						$type = "type-error";
						$message = "Lo siento no pude publicar tu comentario por mas que quise... Tal vez si lo intentas otra vez funcione.";
					}
					break;
				}
				case "session":{
					if ( !isset($_SESSION['session_user_id']) && !isset($_SESSION['session_user_name']) ){
						if ( $_GET['logout'] ){
							$type="type-exito";
							$message='Ok.. Ya terminaste la sesi&oacute;n y saliste bien librado. Hasta pronto...';
						}
						else{
							$type="type-error";
							$message='Que rayos?!!!... No puedo terminar la sesi&oacute;n, en este caso borra las cookies del navegador para que no haya problema. Ni modo&#133;';
						}
					}
					break;
				}
				case 'contact':{
					if ( $_GET['send']==1 ){
						$type = 'type-exito';
						$message = 'Tu mensaje se envio correctamente. No tardare en responder, as&iacute; que debes estar atento a tu correo.';
					}
					else{
						$type = 'type-error';
						$message = 'Por mas que lo intente, no pude enviar tu mensaje, si puedes intentalo mas tarde por favor.';
					}
					break;
				}
				case 'gallery':{
					if ( !$_GET['zip_created'] ){
						$type = 'type-alerta';
						$message = 'No pude crear el zip por que no hay directorio de donde sacar los archivos o no hay id de galeria.';
					}
					break;
				}
				/*case '':
					breaK;*/
			}
			echo '<div class="' . $type . ' mensajes">' . $message . '</div>';
		}
	}
	
	// Funcion que regresa el nombre del usuario que sea indicado por un numero de ID
	function user_id($id){
		$sql_user='SELECT user_name FROM web_users WHERE user_id=' . $id . ' LIMIT 1';
		$res_user=exeQuery($sql_user);
		if ( mysql_num_rows($res_user) > 0 ){
			$user=mysql_fetch_array($res_user);
			$name=$user['user_name'];
		}
		else{
			$name="Intruso";
		}
		return $name;
	}
	
	//Funcion que regresa el nombre y el apellido en un array del usuario que ha iniciado sesi�n recibe el numero de ID
	function logged_user($user_id){
		if ( (int )$user_id > 0 ){
			$sql_logged_user = 'SELECT user_user_first_name,user_user_last_name FROM web_users WHERE user_id=' . $user_id . ' LIMIT 1';
			$res_logged_user = exeQuery($sql_logged_user);
			if ( mysql_num_rows($res_logged_user) > 0 ){
				$logged_user = mysql_fetch_array($res_logged_user);
				$user_names = array('fname' => $logged_user['user_user_first_name'],'lname' => $logged_user['user_user_last_name'] );
			}
			else{
				$user_names = array('fname' => 'No names. ','lname' => 'No last names.' );
			}
				
		}
		else{
			$user_names = array('fname' => 'No names. ','lname' => 'No last names.' );
		}
		return $user_names;
	}
	
	// Funcion para saberla IP del usuario que visita la pagina
	function getRealIp() {
       if (!empty($_SERVER['HTTP_CLIENT_IP'])) {  //check ip from share internet
         $ip=$_SERVER['HTTP_CLIENT_IP'];
       } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {  //to check ip is pass from proxy
         $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
       } else {
         $ip=$_SERVER['REMOTE_ADDR'];
       }
       return $ip;
    }
    
    
    
    // Funciones de admin
    //funcion que regresa el numero total de eregistros de una tabla especificada
	function admin_total_regs($table){
		$sql_total = 'SELECT * FROM ' . $table . ';';
		$res_total = exeQuery($sql_total);
		return mysql_num_rows( $res_total );
	}
	function admin_total_files($dir_of_files){
		if ( file_exists($dir_of_files) ){
			$num_files = 0;
			$open = opendir($dir_of_files);
			while ( $read = readdir($open) )
					$num_files++;
			return $num_files;
		}
		else
			return 0;
	}
	
	
	// Funcion que regresa la categoria a la que pertenece un posts dado su ID
	function admin_post_category_id($cat_id){
		if ( $cat_id > 0 ){
			$sql_category_id = 'SELECT category_name FROM web_categories WHERE category_id=' . $cat_id . ' LIMIT 1';
			$res_category_id = exeQuery($sql_category_id);
			if ( mysql_num_rows($res_category_id) > 0 ){
				$category_id = mysql_fetch_array($res_category_id);
				return $category_id['category_name'];
			}
			else
				return 'Undef';
		}
		else
			return 'Undef';
	}
	
	// Funcion que regresa el numero de comentarios de una entrada dado el ID de entrada
	function admin_count_comments($post_id){
		$sql_count_comments = 'SELECT * FROM web_comments WHERE com_post=' . $post_id . ';';
		$res_count_comments = exeQuery($sql_count_comments);
		return mysql_num_rows($res_count_comments);
	}
	
	// Advertices
	function admin_advertices(){
		if ( isset($_GET['adv']) && $_GET['adv'] == 1 ){
			switch($_GET['type']){
				// blog-roll
				case 'roll':
					switch($_GET['opt']){
						case 'published':
							if ( $_GET['res'] == '1' ){
								$adv_type = 'exito';
								$adv_content = 'The Blog-Roll was published successful.';
							}
							else{
								$adv_type = 'erro';
								$adv_content = 'The Blog-Roll was\'nt published.';
							}
							break;
						case 'public':
							if ( $_GET['res'] == '1' ){
								$adv_type = 'exito';
								if ( $_GET['toggle'] == '1' ){
									$adv_content = 'The Blog-Roll was change pubMode to <b><u>public</u></b>.';
								}
								else{
									$adv_content = 'The Blog-Roll was change pubMode to <b><u>private</u></b>.';
								}
							}
							else{
								$adv_type = 'error';
								$adv_content = 'Can\'nt be change the puMode.';
							}
							break;
					}
					break;
				
				// Accounts
				case 'accounts':
					switch($_GET['opt']){
						case 'editMyAccount':
							if ( $_GET['res'] == '1' ){
								$adv_type = 'exito';
								$adv_content = 'The account info was updated successful.';
							}
							else{
								$adv_type = 'error';
								$adv_content = 'The account info was\'nt updated.';
							}
							break;
						case 'editPasswd':
							if ( $_GET['res'] == '1' ){
								$adv_type = 'exito';
								$adv_content = 'The password was updated successful.';
							}
							else{
								$adv_type = 'error';
								$adv_content = 'The password was\'nt updated.';
							}
							break;
					}
					break;
				//Gallery
				case 'gallery':
					switch($_GET['opt']){
						case 'imgChname':
							if ( $_GET['res'] == '1' ){
								$adv_type = 'exito';
								$adv_content = 'The image was renamed successful.';
							}
							else{
								$adv_type = 'error';
								$adv_content = 'The image was\'nt renamed.';
							}
							break;
						case 'imgDelete':
							if ( $_GET['res'] == '1' ){
								$adv_type = 'exito';
								$adv_content = 'The image was deleted successful.';
							}
							else{
								$adv_type = 'error';
								$adv_content = 'The image was\'nt deleted.';
							}
							break;
						case 'upImage':
							if ( $_GET['res'] == '1' ){
								if ( $_GET['files'] > 0 ){
									$adv_type = 'exito';
									$adv_content = '<b><u>' . $_GET['files'] . '</u></b> images was uploaded successful.';
								}
								else{
									$adv_type = 'error';
									$adv_content = 'The files ca\'nt be uploaded.';
								}
							}
							else{
								$adv_type = 'error';
								$adv_content = 'The files ca\'nt be uploaded.';
							}
							break;
						case 'delete':
							if ( $_GET['res'] == '1' ){
								$adv_type = 'exito';
								$adv_content = 'The gallery was deleted successful.';
							}
							else{
								$adv_type = 'error';
								$adv_content = 'The gallery was\'nt deleted.';
							}
							break;
						case 'created':
							if ( $_GET['res'] == '1' ){
								$adv_type = 'exito';
								$adv_content = 'The gallery was created successful.';
							}
							else{
								if ( $_GET['res'] == '0' ){
									$adv_type = 'error';
									$adv_content = 'The gallery was\'nt created.';
								}
							}
							break;
						case 'update':
							if ( $_GET['res'] == '1' ){
								$adv_type = 'exito';
								$adv_content = 'The gallery was updated successful.';
							}
							else{
								if ( $_GET['res'] == '0' ){
									$adv_type = 'error';
									$adv_content = 'The gallery was\'nt updated.';
								}
							}
							break;
					}
					if ( $_GET['pub'] == '1' ){
						if ( $_GET['toggle'] == '1' ){
							$adv_type = 'exito';
							$adv_content = 'The gallery was toggle to <b></u>public</u></b>.';
						}
						else{
							if ( $_GET['toggle'] == '0' ){
								$adv_type = 'exito';
								$adv_content = 'The gallery was toggle to <b></u>private</u></b>.';
							}
						}
					}
					else{
						if ( $_GET['pub'] == '0' ){
							$adv_type = 'error';
							$adv_content = 'The gallery properties can\'t be modified.';
						}
					}
					break;
				//Folder
				case 'folder':
					switch($_GET['opt']){
						case 'create':
							if ( $_GET['res'] ){
								$adv_type = 'exito';
								$adv_content = 'The folder was created successful.';
							}
							else{
								$adv_type = 'error';
								$adv_content = 'The folder was\'nt created.';
							}
							break;
					}
					break;
				//Image file
				case 'image':
					switch($_GET['opt']){
						case 'delete':
							if ( $_GET['res'] ){
								$adv_type = 'exito';
								$adv_content = 'The file was deleted successful.';
							}
							else{
								$adv_type = 'error';
								$adv_content = 'The file was\'nt deleted.';
							}
							break;
					}
					
					break;
				//Comment reply
				case 'reply':
					if ($_GET['save']){
						$adv_type = 'exito';
						$adv_content = 'The comments was published successful.';
					}
					else{
						$adv_type = 'error';
						$adv_content = 'The comments was\'nt published successful.';
					}
					if ( $_GET['send'] )
						$error_smtp = 0;
					else
						$error_smtp = 1;
					break;
				//Post
				case 'post':
					if ( $_GET['published'] ){
						$adv_type = 'exito';
						$adv_content = 'The post was published successful.';
					}
					else{
						$adv_type = 'error';
						$adv_content = 'The post was not published. Sorry!.';
					}
					if ( $_GET['update'] ){
						$adv_type = 'exito';
						$adv_content = 'The post was updated successful.';
					}
					break;
				//Post
				case 'publish':
					if ( $_GET['pub'] ){
						$adv_type = 'exito';
						if ($_GET['toggle'] ){
							$adv_content = 'The post was toggle to <u><b>public</b></u>.';
						}
						else{
							$adv_content = 'The post was toggle to <u><b>hidden</b></u>.';
						}
					}
					break;
				//Post
				case 'deletePost':
					if ( $_GET['delete'] ){
						$adv_type = 'exito';
						$adv_content = 'The post was deleted successful.';
					}
					else{
						$adv_type = 'error';
						$adv_content = 'The post was not deleted. Sorry!';
					}
					break;
				// Category
				case 'category':
					switch($_GET['opt']){
						case 'delete':
							if ( $_GET['res'] ){
								$adv_type = 'exito';
								$adv_content = 'The category was delete successful.';
							}
							else{
								$adv_type = 'error';
								$adv_content = 'An error was ocurred, please contact whit the administrator.';
							}
							break;
						case 'update':
							if ( $_GET['res'] ){
								$adv_type = 'exito';
								$adv_content = 'The category was updated successful.';
							}
							else{
								$adv_type = 'error';
								$adv_content = 'An error was ocurred, please contact whit the administrator.';
							}
							break;
						case 'published':
							if ( $_GET['res'] ){
								$adv_type = 'exito';
								$adv_content = 'The category was published successful.';
							}
							else{
								$adv_type = 'error';
								$adv_content = 'An error was ocurred, please contact whit the administrator.';
							}
							break;
					}
					break;
				//comentarios
				case 'comment':{
					switch($_GET['opt']){
						case 'delete':
							if ( $_GET['res'] ){
								$adv_type = 'exito';
								$adv_content = 'The comment was delete successful.';
							}
							else{
								$adv_type = 'error';
								$adv_content = 'The comment was\'nt delete.';
							}
							break;
					}
					break;
				}
			}
			echo '<span class="type-' . $adv_type . ' messages">' . $adv_content . '</span>';
			if ( $error_smtp ){
				echo '<span class="type-error messages">The SMTP server is not working succesfull, please check your SMTP port or change the SMTP config un your <i>php.ini</i></span>';
			}
		}
	}
	
	// funcion que regresa el correo electronico de un usuario, resive el id de usuario
	function admin_user_email($user_id){
		$sql_user_email = 'SELECT user_user_email FROM web_users WHERE user_id=' . $user_id . ' LIMIT 1';
		$res_user_email = exeQuery($sql_user_email);
		if ( mysql_num_rows($res_user_email) > 0 ){
			$user_email = mysql_fetch_array($res_user_email);
			$email = $user_email['user_user_email'];
		}
		else{
			$email = 'fake@' . $_SERVER['SERVER_NAME'];
		}
		return $email;
	}
	function makeHref($str, $target="_blank"){		
		// case insensitive regexp. 
		// -- protocol tag or www 
		$str2 = preg_replace("!(((http(s?)://)|(www\.))".    
		             // -- rest of the host, topdomain is 2-4 letters 
		             "([-a-z0-9.]{2,}\.[a-z]{2,4}".                                      
		             // -- port (optional) 
		             "(:[0-9]+)?)".                
		             // -- path (optional) 
		             "((/([^\s]*[^\s.,\"'])?)?)".  
		             // -- parameters (optional, but obligated to begin with a question mark) 
		             "((\?([^\s]*[^\s.,\"'])?)?))!i",     
		             // replace it with <a tag 
		             "<a href=\"http\\4://\\5\\6\\8\\9\" target=\"$target\">\\1</a>", 
		             $str); 
		return $str2; 
	}
	
	
	// Funcion para truncar una cadena con un limite...
	function truncate($string, $limit, $break=".", $pad="[...]") {   
    // return with no change if string is shorter than $limit   
    if(strlen($string) <= $limit)   
        return $string;    
  
    // is $break present between $limit and the end of the string?   
    if(false !== ($breakpoint = strpos($string, $break, $limit))) {   
        if($breakpoint < strlen($string) - 1) {   
            $string = substr($string, 0, $breakpoint) . $pad;   
        }   
    }   
    return $string;   
}
?>