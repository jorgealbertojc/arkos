<?php
	echo '<span class="index-content">';
	$sql_user_info = 'SELECT * FROM web_users WHERE user_id=' . $_SESSION['session_user_id'] . ' LIMIT 1';
	$res_user_info = exeQuery($sql_user_info);
	if ( mysql_num_rows($res_user_info) > 0 ){
		$user_info = mysql_fetch_array($res_user_info);
		
		// actions
		echo 'Actions : ';
		echo '<a href="#mb_edit_account_info" rel="lightbox[500 250]" class="admin-account"><span class="actions edit-info">Edit my account info</span></a>';
		echo '<a href="javascript:void(0);" class="admin-account"><span class="actions add-sticknote">Add me a sticknote</span></a>';
		echo '<a href="#mb_edit_my_passwd" rel="lightbox[500 250]" class="admin-account"><span class="actions edit-passwd">Edit my account passwd</span></a>';
		
		
		//Edit account info
		echo '<div id="mb_edit_account_info" style="display: none;">';
			echo '<span class="add-category">';
				echo '<h2 class="admin">Edit my account info</h2>';
				echo '<form method="post" action="' . INDEX_ADMIN . '?action=manAccounts&page=myAccount" name="edit_account-info">';
					echo '<table cellpadding="0" cellspacing="0" border="0">';
						// user name
						echo '<tr>';
							echo '<td><span style="display: block; width: 100px;">User name</span></td>';
							echo '<td><input type="text" class="input-text" style="width: 345px;" value="' . user_id($user_info['user_id']) . '" disabled="disabled" /></td>';
						echo '</tr>';
						echo '<tr><td colspan="2" style="height: 10px;"></td></tr>';
						// first name
						echo '<tr>';
							echo '<td><span style="display: block; width: 100px;">First name</span></td>';
							echo '<td><input type="text" class="input-text" style="width: 345px;" value="' . $user_info['user_user_first_name'] . '" name="user_first_name" /></td>';
						echo '</tr>';
						echo '<tr><td colspan="2" style="height: 10px;"></td></tr>';
						// last name
						echo '<tr>';
							echo '<td><span style="display: block; width: 100px;">Last name</span></td>';
							echo '<td><input type="text" class="input-text" style="width: 345px;" value="' . $user_info['user_user_last_name'] . '" name="user_last_name" /></td>';
						echo '</tr>';
						echo '<tr><td colspan="2" style="height: 10px;"></td></tr>';
						// user email
						echo '<tr>';
							echo '<td><span style="display: block; width: 100px;">User email</span></td>';
							echo '<td><input type="text" class="input-text" style="width: 345px;" value="' . $user_info['user_user_email'] . '" name="user_email" /></td>';
						echo '</tr>';
						echo '<tr><td colspan="2" style="height: 10px;"></td></tr>';
						// Actions of the form
						//echo '<tr><td colspan="2" style="height: 10px;"></td></tr>';
						echo '<tr><td colspan="2" align="right">';
							echo '<button type="submit" class="submit-button" name="update_account_info" value="true"><span class="in-submit-left"><span class="in-submit-right">Update info</span></span></button>';
							echo '<button type="reset" class="submit-button" name="update_account_info" value="false"><span class="in-submit-left"><span class="in-submit-right">Reset</span></span></button>';
							echo '<a href="javascript:void(0);" onClick="Mediabox.close();" class="submit-button"><span class="in-submit-left"><span class="in-submit-right">Cancel</span></span></button>';
						echo '</td></tr>';
					echo '</table>';
				echo '</form>';
			echo '</span>';
		echo '</div>';
		
		//Edit my passwd
		echo '<div id="mb_edit_my_passwd" style="display: none">';
			echo '<span class="add-category">';
				echo '<h2 class="admin">Edit my passwd</h2>';
				echo '<form method="post" action="' . INDEX_ADMIN . '?action=manAccounts&page=myAccount" name="edit_my_passwd">';
					echo '<table cellpadding="0" cellspacing="0" border="0">';
						// actual passwd
						echo '<tr>';
							echo '<td><span style="display: block; width: 150px;">Actual passwd</span></td>';
							echo '<td><input type="password" class="input-text" style="width: 295px;" name="actual_passwd" /></td>';
						echo '</tr>';
						echo '<tr><td colspan="2" style="height: 10px;"></td></tr>';
						// new passwd
						echo '<tr>';
							echo '<td><span style="display: block; width: 150px;">New passwd</span></td>';
							echo '<td><input type="password" class="input-text" style="width: 295px;" name="new_passwd" /></td>';
						echo '</tr>';
						echo '<tr><td colspan="2" style="height: 10px;"></td></tr>';
						// repeat new passwd
						echo '<tr>';
							echo '<td><span style="display: block; width: 150px;">Repeat new passwd</span></td>';
							echo '<td><input type="password" class="input-text" style="width: 295px;" name="repeat_new_passwd" /></td>';
						echo '</tr>';
						echo '<tr><td colspan="2" style="height: 10px;"></td></tr>';
						// Actions of the form
						echo '<tr><td colspan="2" align="right">';
							echo '<button type="submit" class="submit-button" name="update_passwd" value="true"><span class="in-submit-left"><span class="in-submit-right">Update passwd</span></span></button>';
							echo '<button type="reset" class="submit-button" name="update_passwd" value="false"><span class="in-submit-left"><span class="in-submit-right">Reset</span></span></button>';
							echo '<a href="javascript:void(0);" onClick="Mediabox.close();" class="submit-button"><span class="in-submit-left"><span class="in-submit-right">Cancel</span></span></button>';
						echo '</td></tr>';
					echo '</table>';
				echo '</form>';
			echo '</span>';
		echo '</div>';
		
		// End: actions
		
		$user_logged_in = logged_user($_SESSION['session_user_id']);
		echo '<h2 class="admin">Manage my account | ' . $user_logged_in['fname'] . ' ' . $user_logged_in['lname'] . '</h2>';
		
		//Table of contents
		echo '<table cellpadding="0" cellspacing="0" border="0" class="blog-entry">';
			echo '<thead>';
				echo '<tr>';
					echo '<td><span style="display: block; width: 200px;">Identifier</span></td>';
					echo '<td><span style="display: block; width: 644px;">Value</span></td>';
					echo '<td><span style="display: block; width: 150px;">Photo</span></td>';
				echo '</tr>';
			echo '</thead>';
			echo '<tbody>';
				// user name
				echo '<tr class="on">';
					echo '<td><span style="display: block; width: 200px;">User name</span></td>';
					echo '<td><span style="display: block; width: 644px;">' . user_id($user_info['user_id']) . '</span></td>';
					echo '<td rowspan="9" valign="top"><span style="display: block; width: 150px;"><img src="http://www.gravatar.com/avatar.php?gravatar_id=' . md5($user_info['user_user_email']) . '&size=120" style="border: 1px solid #234; padding: 3px; margin: 10px; height: 120px; width: 120px; display: block;" /></span></td>';
				echo '</tr>';
				
				//Firsta Name
				echo '<tr class="off">';
					echo '<td><span style="display: block; width: 200px;">First name(s)</span></td>';
					echo '<td><span style="display: block; width: 644px;">' . $user_info['user_user_first_name'] . '</span></td>';
				echo '</tr>';
				
				// Last name
				echo '<tr class="on">';
					echo '<td><span style="display: block; width: 200px;">Last name(s)</span></td>';
					echo '<td><span style="display: block; width: 644px;">' . $user_info['user_user_last_name'] . '</span></td>';
				echo '</tr>';
				
				// User email
				echo '<tr class="off">';
					echo '<td><span style="display: block; width: 200px;">User email</span></td>';
					echo '<td><span style="display: block; width: 644px;">' . $user_info['user_user_email'] . '</span></td>';
				echo '</tr>';
				
				//Member from
				echo '<tr class="on">';
					echo '<td><span style="display: block; width: 200px;">Member from</span></td>';
					echo '<td><span style="display: block; width: 644px;">' . substr(date_month($user_info['user_date_created']),0,3) . ' ' . date_day($user_info['user_date_created']) . ' &#124; ' . date_year($user_info['user_date_created']) . ' at ' . date_hour($user_info['user_date_created']) . ' Hrs</span></td>';
				echo '</tr>';
				
				// Last update
				echo '<tr class="off">';
					echo '<td><span style="display: block; width: 200px;">Last update</span></td>';
					echo '<td><span style="display: block; width: 644px;">' . substr(date_month($user_info['user_date_updated']),0,3) . ' ' . date_day($user_info['user_date_updated']) . ' &#124; ' . date_year($user_info['user_date_updated']) . ' at ' . date_hour($user_info['user_date_updated']) . ' Hrs</span></td>';
				echo '</tr>';
				
				// Is user hidden?
				echo '<tr class="on">';
					echo '<td><span style="display: block; width: 200px;">Hidden user</span></td>';
					echo '<td><span style="display: block; width: 644px;">';
					if ( $user_info['user_hidden'] )
						echo '<img src="../images/icn/admin-16-red-ball.png" style="float: left; margin-right: 5px;" />User hidden';
					else
						echo '<img src="../images/icn/admin-16-green-ball.png" style="float: left; margin-right: 5px;" />No hidden';
					echo '</span></td>';
				echo '</tr>';
				
				// is user admin?
				echo '<tr class="off">';
					echo '<td><span style="display: block; width: 200px;">Admin user</span></td>';
					echo '<td><span style="display: block; width: 644px;">';
					if ( !$user_info['user_admin'] )
						echo '<img src="../images/icn/admin-16-red-ball.png" style="float: left; margin-right: 5px;" />No dmin';
					else
						echo '<img src="../images/icn/admin-16-green-ball.png" style="float: left; margin-right: 5px;" />User admin';
					echo '</span></td>';
				echo '</tr>';
				
				// Rate in admin mode
				echo '<tr class="on">';
					echo '<td><span style="display: block; width: 200px;">Admin type</span></td>';
					echo '<td><span style="display: block; width: 644px;">';
					if ( !$user_info['user_admin'] )
						echo '<img src="../images/icn/admin-16-red-ball.png" style="float: left; margin-right: 5px;" />None';
					else{
						echo '<img src="../images/icn/admin-16-green-ball.png" style="float: left; margin-right: 5px;" />';
						echo 'Admin rate ' . $user_info['user_admin_type'] . '';
					}
					echo '</span></td>';
				echo '</tr>';
			echo '</tbody>';
		echo '</table>';
	}
	echo '</span>';
?>
