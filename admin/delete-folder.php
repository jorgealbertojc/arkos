<?php
	session_start();
	include_once '../includes/config.inc.php';
	include_once '../includes/functions.inc.php';
	if ( isset($_SESSION['session_user_id']) && isset($_SESSION['session_user_name']) && ( $_GET['folderName'] != '' ) && ( $_GET['folderDir'] != '' ) && ( $_GET['action'] == 'deleteFolder' ) ){
		if ( file_exists($_GET['folderDir']) ){
			function delete_dir_recursive($dir){
				$open_dir = opendir($dir);
				while ( $files = readdir($open_dir) ){
					if ( $files != '..' && $files != '.'){
						if ( substr($dir,-1) != '/' ){
							$fDirs = $dir . '/';
						}
						else
							$fDirs = $dir;
						$directs = $fDirs . $files;
						if ( !is_dir($directs) ){
							unlink($directs);
						}
						else{
							delete_dir_recursive($directs);
						}
					}
				}
				closedir($open_dir);
				rmdir($dir);
			}
			delete_dir_recursive($_GET['folderDir']);
			header('Location: ' . INDEX_ADMIN .'?action=optImages&dir=' . substr(str_replace('//','/',$_GET['folderReturn']),0,-1) . '&adv=1&type=folder&opt=delete&res=1');
		}
		else
			header('Location: ' . INDEX_ADMIN .'?action=optImages&dir=' . substr(str_replace('//','/',$_GET['folderReturn']),0,-1) . '&adv=1&type=folder&opt=delete&res=0');
	}
	else
		header('Location: ' . INDEX_ADMIN .'?action=optImages&dir=' . substr(str_replace('//','/',$_GET['folderReturn']),0,-1) . '&adv=1&type=folder&opt=delete&res=0');

?>
