<?php
	session_start();
	include_once '../includes/config.inc.php';
	header("Cache-control: private");
	session_unset();
	session_destroy();
	header('Location: ' . INDEX_ADMIN . '?adv=1&type=session&close=1');
	exit(0);
?>
