<?php
	session_start();
	include_once '../includes/config.inc.php';
	include_once '../includes/functions.inc.php';
	if ( isset($_SESSION['session_user_id']) && isset($_SESSION['session_user_name']) && ( $_GET['folderName'] != '' ) && ( $_GET['action'] == 'createFolder' ) && ( $_GET['folderBase'] != '' ) ){
		if ( !file_exists($_GET['folderBase'] . $_GET['folderTarget'] . $_GET['folderName']) ){
			mkdir($_GET['folderBase'] . $_GET['folderTarget'] . $_GET['folderName'], 7777);
			header('Location: ' . INDEX_ADMIN .'?action=optImages&dir=' . substr($_GET['folderTarget'],0,-1) . '&adv=1&type=folder&opt=create&res=1');
		}
		else
			header('Location: ' . INDEX_ADMIN .'?action=optImages&dir=' . substr($_GET['folderTarget'],0,-1) . '&adv=1&type=folder&opt=create&res=0');
	}
	else
		header('Location: ' . INDEX_ADMIN .'?action=optImages&dir=' . substr($_GET['folderTarget'],0,-1) . '&adv=1&type=folder&opt=create&res=0');
?>