<?php
	session_start();
	include_once '../includes/config.inc.php';
	include_once '../includes/functions.inc.php';
	if ( isset($_SESSION['session_user_id']) && isset($_SESSION['session_user_name']) && ( $_GET['idGallery'] > 0 ) && ( $_GET['action'] == 'changePubMode' ) ){
		$idcnx_pub_gallery = connect();
		
		$sql_pub_state = 'SELECT gallery_published FROM web_gallery WHERE gallery_id=' . $_GET['idGallery'] . ' LIMIT 1';
		$res_pub_state = exeQuery($sql_pub_state);
		if ( mysql_num_rows($res_pub_state) > 0 ){
			$pub_state = mysql_fetch_array($res_pub_state);
			if ( $pub_state['gallery_published'] == 0 ){
				$sql_change_pub_state = 'UPDATE web_gallery SET gallery_published=1 WHERE gallery_id=' . $_GET['idGallery'] . '';
				$toggle_flag = 1;
			}
			else{
				$sql_change_pub_state = 'UPDATE web_gallery SET gallery_published=0 WHERE gallery_id=' . $_GET['idGallery'] . '';
				$toggle_flag = 0;
			}
			exeQuery($sql_change_pub_state);
			header('Location: ' . INDEX_ADMIN .'?action=optImages&page=gallery&adv=1&type=gallery&pub=1&toggle=' . $toggle_flag . '');
		}
		else
			header('Location: ' . INDEX_ADMIN .'?action=optImages&page=gallery&adv=1&type=gallery&pub=0&toggle=0');
		
		mysql_lose($idcnx_pub_gallery);
	}
	else{
		header('Location: ' . INDEX_ADMIN .'?action=optImages&page=gallery&adv=1&type=gallery&pub=0&toggle=0');
	}

?>
