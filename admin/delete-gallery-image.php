<?php
	session_start();
	include_once '../includes/config.inc.php';
	include_once '../includes/functions.inc.php';
	if ( isset($_SESSION['session_user_id']) && isset($_SESSION['session_user_name']) && ( $_GET['imageId'] > 0 ) && ( $_GET['action'] == 'deleteImage' ) && file_exists($_GET['imageDir']) && $_GET['idGallery'] > 0 ){
		if ( file_exists($_GET['imageDir']) ){
			$idcnx_delete_img = connect();
			$sql_delete_image_reg = 'DELETE FROM web_image_gallery WHERE image_dir="' . $_GET['imageDir'] . '"';
			exeQuery($sql_delete_image_reg);
			$sql_opt_images = 'OPTIMIZE TABLE web_image_gallery';
			exeQuery($sql_opt_images);
			mysql_close($idcnx_delete_img);
			@unlink($_GET['imageDir']);
			header('Location: ' . INDEX_ADMIN .'?action=optImages&page=gallery&site=viewGallery&idGallery=' . $_GET['idGallery'] . '&adv=1&type=gallery&opt=imgDelete&res=1');
		}
		else{
			header('Location: ' . INDEX_ADMIN .'?action=optImages&page=gallery&site=viewGallery&idGallery=' . $_GET['idGallery'] . '&adv=1&type=gallery&opt=imgDelete&res=0');
		}
	}
	else{
		header('Location: ' . INDEX_ADMIN .'?action=optImages&page=gallery&site=viewGallery&idGallery=' . $_GET['idGallery'] . '&adv=1&type=gallery&opt=imgDelete&res=0');
	}
?>
