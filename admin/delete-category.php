<?php
	session_start();
	include_once '../includes/config.inc.php';
	include_once '../includes/functions.inc.php';
	if ( isset($_SESSION['session_user_id']) && isset($_SESSION['session_user_name']) && ( $_GET['idCategory'] > 0 ) && ( $_GET['action'] == 'deleteCategory' ) ){
		$idcnx_delete_category = connect();
		$sql_delete_category = 'DELETE FROM web_categories WHERE category_id=' . $_GET['idCategory'] . ' LIMIT 1';
		exeQuery($sql_delete_category);
		$sql_opt_category = 'OPTIMIZE TABLE web_categories;';
		exeQuery($sql_opt_category);
		mysql_close($idcnx_delete_category);
		header('Location: ' . INDEX_ADMIN .'?action=optBlog&page=category&adv=1&type=category&opt=delete&res=1');
	}
	else{
		header('Location: ' . INDEX_ADMIN .'?action=optBlog&page=category&adv=1&type=category&opt=delete&res=0');
	}
?>
