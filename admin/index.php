<?php
	session_start();
	if ( isset($_SESSION['session_user_id']) && isset($_SESSION['session_user_name']) ){
		include_once '../includes/config.inc.php';
		include_once '../includes/functions.inc.php';
		$idcnx_admin = connect();
		include_once '../includes/admin-top.php';
		// Top page
			
			switch($_GET['action']){
				case 'optRoll':
					include_once '../includes/admin-blog-roll-index.php';
					break;
				case 'manAccounts':
					include_once '../includes/admin-users-index.php';
					break;
				case 'optImages':
					include_once '../includes/admin-image-index.php';
					break;
				case 'optComments':
					include_once '../includes/admin-comments-index.php';
					break;
				case 'optBlog':
					include_once '../includes/admin-blog-index.php';
					break;
				default:
					include_once '../includes/admin-index.php';
			}
			
		// Bottom page
		include_once '../includes/admin-bottom.php';
		mysql_close($idcnx_admin);
	}
	else{
		include_once '../includes/config.inc.php';
		include_once '../includes/functions.inc.php';
		include_once '../includes/admin-session-login-form.php';
	}
?>
