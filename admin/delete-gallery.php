<?php
	session_start();
	include_once '../includes/config.inc.php';
	include_once '../includes/functions.inc.php';
	if ( isset($_SESSION['session_user_id']) && isset($_SESSION['session_user_name']) && ( $_GET['galleryId'] > 0 ) && ( $_GET['action'] == 'deleteGallery' ) && file_exists($_GET['galleryDir']) ){
		
		// Delete all photos in gallery
		$sql_delete_files = 'SELECT * FROM web_image_gallery WHERE image_gallery=' . $_GET['galleryId'] . '';
		$res_delete_files = exeQuery($sql_delete_files);
		if ( mysql_num_rows($res_delete_files) > 0 ){
			while ( $delete_files = mysql_fetch_array($res_delete_files) ){
				if ( file_exists($delete_files['image_dir']) )
					@unlink($delete_files['image_dir']);
			}
		}
		// Delete regs on the Data Base
		$sql_delete_reg = 'DELETE FROM web_image_gallery WHERE image_gallery=' . $_GET['galleryId'] . '';
		exeQuery($sql_delete_reg);
		// Delete the files in the dir
		$delete_all_files = opendir($_GET['galleryDir']);
			while ( $read_files = readdir($delete_all_files) ){
				if ( $read_files != '.' && $read_files != '..' ){
					@unlink($_GET['galleryDir'] . '/' . $read_files);
				}
			}
		closedir($delete_all_files);
		
		// Delete dir of gallery
		@rmdir($_GET['galleryDir']);
		//Delete reg of the Data base
		$sql_delete_gallery_reg = 'DELETE FROM web_gallery WHERE gallery_id=' . $_GET['galleryId'] . ' LIMIT 1';
		exeQuery($sql_delete_gallery_reg);
		
		// Optimize tables
		$sql_opt_images = 'OPTIMIZE TABLE web_image_gallery;';
		exeQuery($sql_opt_images);
		$sql_opt_gallery = 'OPTIMIZE TABLE web_gallery;';
		exeQuery($sql_opt_gallery);
		header('Location: ' . INDEX_ADMIN .'?action=optImages&page=gallery&adv=1&type=gallery&opt=delete&res=1');
	}
	else
		header('Location: ' . INDEX_ADMIN .'?action=optImages&page=gallery&adv=1&type=gallery&opt=delete&res=0');
?>
