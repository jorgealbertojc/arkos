<?php
	session_start();
	include_once '../includes/config.inc.php';
	include_once '../includes/functions.inc.php';
	if ( isset($_SESSION['session_user_id']) && isset($_SESSION['session_user_name']) && ( $_GET['fileUrl'] != '' ) && ( $_GET['action'] == 'deleteFile' ) ){
		if ( file_exists($_GET['fileUrl']) ){
			if ( unlink($_GET['fileUrl']) )
				header('Location: ' . INDEX_ADMIN .'?action=optImages&dir=' . substr(str_replace('//','/',$_GET['fileReturn']),0,-1) . '&adv=1&type=image&opt=delete&res=1');
			else
				header('Location: ' . INDEX_ADMIN .'?action=optImages&page=postUploaded&adv=1&type=image&opt=delete&res=0');
		}
		else
			header('Location: ' . INDEX_ADMIN .'?action=optImages&page=postUploaded&adv=1&type=image&opt=delete&res=0');
	}
	else{
		header('Location: ' . INDEX_ADMIN .'?action=optImages&page=postUploaded&adv=1&type=image&opt=delete&res=0');
	}
?>
