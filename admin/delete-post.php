<?php
	session_start();
	include_once '../includes/config.inc.php';
	include_once '../includes/functions.inc.php';
	if ( isset($_SESSION['session_user_id']) && isset($_SESSION['session_user_name']) && ( $_GET['idPost'] > 0 ) && ( $_GET['action'] == 'deletePost' ) ){
		$idcnx_publish = connect();
		$sql_delete_comments_post = 'DELETE FROM web_comments WHERE com_post=' . $_GET['idPost'] . '';
		$res_delete_comments_post = exeQuery($sql_delete_comments_post);
		$sql_delete_post = 'DELETE FROM web_posts WHERE post_id=' . $_GET['idPost'] . ' LIMIT 1;';
		$res_delete_post = exeQuery($sql_delete_post);
		$sql_opt_comms = 'OPTIMIZE TABLE web_comments;';
		$sql_opt_posts = 'OPTIMIZE TABLE web_posts';
		exeQuery($sql_opt_comms);
		exeQuery($sql_opt_posts);
		header('Location: ' . INDEX_ADMIN .'?action=optBlog&page=index&adv=1&type=deletePost&delete=1');
		mysql_close($idcnx_publish);
	}
	else{
		header('Location: ' . INDEX_ADMIN .'?action=optBlog&page=index&adv=1&type=deletePost&change=0');
	}

?>
