<?php
	// Encambezados que utiliza la funcion mail para formar archivos con estilos en HTML
	$headers = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";
	$headers .= 'From: "Web-Blog ' . str_replace($car_hex,$car_esp,SITE_NAME) . '" <no-reply@arkos.net63.net>' . "\r\n";
	$headers .= 'Reply-To: "Web-Blog ' . str_replace($car_hex,$car_esp,SITE_NAME) . '" <no-reply@arkos.net63.net>' . "\r\n";
	
	
	$arraydateen = array(
		'January', 'February','March','April','May','June','July','August','September','October','November','December',
		'Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday',
		'Jan','Apr','Aug','Dec'
	);
	$arraydatees = array(
		'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre',
		'Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo',
		'Ene','Abr','Ago','Dic'
	);
	$pron_array = array(
		' al ',' la ',' el ',' los ',' las ',' lo ',' les ',' una ',' uno ',' unas ',
		' unos ',' es ',' son ',' de ',' no ',' si ',' nos '
	);
	
	/**
	 *
	 *	Funcion que devuelve el nombre de la categoria en la que se encuentra
	 *	valuada la entrada del blog. Resibe el Id de categoria del post.
	 *
	 */
	 function post_category($postcatid = 0){
	 	$post_category = 'Uncategorized';
	 	if ( $postcatid > 0 ){
	 		$sql_postcategory = 'SELECT category_name FROM web_categories WHERE category_id=' . $postcatid . ' LIMIT 1';
	 		$res_postcategory = exeQuery($sql_postcategory);
	 		$postcategory = mysql_fetch_array($res_postcategory);
	 		$post_category = $postcategory['category_name'];
	 	}
	 	else
	 		$post_category = 'Uncategorized';
	 	return $post_category;
	 }
	
	/**
	 *
	 *	Funcion que deuelve el numero de comentarios que hay en una entrada,
	 *	pasandole como parametro el numero de id de la entrada.
	 *
	 */
	 function posts_numcomments($postid = 1){
	 	$num_comments = 0;
	 	if ( $postid > 0 ){
	 		$sql_numcomments = 'SELECT * FROM web_comments WHERE com_post=' . $postid . ';';
	 		$res_numcomments = exeQuery($sql_numcomments);
	 		$num_comments = mysql_num_rows($res_numcomments);
	 	}
	 	else
	 		$num_comments = 0;
	 	return $num_comments;
	 }
	
	/**
	 *
	 *	Funcion que devuelve el nombre del usuario mediante el identificador
	 *	esta funcion es muy util para las entradas de blog o para cualquier
	 *	tabla de SQL que contenga una llave foranea referente a el id de usuario
	 */
	function userid_completename($iduser = 1){
		
		$username = 'Invalid user id';
		if ( $iduser > 0 ){
			$sql_completename = 'SELECT user_user_first_name,user_user_last_name FROM web_users WHERE user_id=' . $iduser . ' LIMIT 1;';
			$res_completename = exeQuery($sql_completename);
			if ( mysql_num_rows($res_completename) > 0 ){
				$completename = mysql_fetch_array($res_completename);
				$username = $completename['user_user_first_name'] . ' ' . $completename['user_user_last_name'];
			}
			else
				$username = 'Invalid user id';
		}
		else
			$username = 'Invalid user id';
		return $username;
	}
	
	/**
	*
	*	Funcion que separa los tags que se encuentran en un post separados por
	*	comas(,), e imprime los tags separados por comas vueltos un link
	*/
	function posts_tags($tag_string = ''){
		if ( strlen($tag_string) <= 0 )
			echo '<a href="javascript:void(0);">Sin tags</a>';
		else{
			$tags = explode(',',$tag_string);
			for ( $i = 0 ; $i < sizeof($tags) ; $i++ ){
				echo '<a href="' . INDEX . '?option=search&call=search&swords=' . $tags[$i] . '&searching=true">' . $tags[$i] . '</a>';
				if ( $i < (sizeof($tags) - 1) )
					echo ' / ';
			}
		}
	}

	
	/**
	*
	*	Funcion que devuelve como valor el numero de entradas que contiene
	*	una categoria dado el identificador de la categoria, regresa un valor
	*	entero.
	*/
	function category_numposts($category_id = 0){
		$sql_catposts = 'SELECT post_id FROM web_posts WHERE post_category = ' . $category_id . '';
		$res_catposts = exeQuery($sql_catposts);
		return (int)mysql_num_rows($res_catposts);
	}
	
	/**
	*
	*	Funcion que se encarga de enviar un correo electronico a el usuario
	*	que dejo un comentario en el Blog, esto lo hace usando el estilo de
	*	correo con HTMl.
	*	
	*	Regresa verdadero si envio el correo, falso en caso contrario, se basa
	*	en la funcion mail de php.
	*/
	function advertice_userleavecomment($user_nickname = 'No nickname', $user_email = 'me@server.dom', $user_comment = 'No comment found...'){
		global $headers;
		$return_value = 0;
		$message = '<html>
			<body>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td background="http://' . $_SERVER['SERVER_NAME'] . INDEX . 'images/mail/bgs.top.png" bgcolor="#112233"><span style="display:block; height: 80px; width: 800px;">
							<img src="http://' . $_SERVER['SERVER_NAME'] . INDEX . 'images/mail/email.arkosnoemarenom.png" />
						</span></td>
					</tr>
					<tr>
						<td><span style="display: block; background-color: #123; width: 800px; font-size: 11px; font-family: Lucida Sans Unicode, Sans-serif; color: #d0eaff;"><span style="display: block; padding: 20px;">
							<div align="right"><img src="http://' . $_SERVER['SERVER_NAME'] . INDEX . 'images/mail/emailing.png" /></div>
							
							Estas recibiendo este correo por que has dejado un comentario en ' . SITE_NAME . ', tu comentario es el siguiente:<br /><br />
							<span style="display:block; padding: 10px; border: 1px dotted #456; margin-top: 20px; margin-bottom: 20px; color: #a0bacf; background-color: #234;">
								' . nl2br($user_comment) . '<br />
							</span>
							
							En cuanto me sea posible, respondere a tu comentario, ' . $user_nickname . ' gracias por tu visita y tu(s) comentario(s).
							
							<div align="center" style="display:block; font-size: 10px; margin-top: 30px;">
								
								<a href="http://' . $_SERVER['SERVER_NAME'] . INDEX . '" style="text-decoration: none;"><span style="color: #5086ff; display: inline-block; padding-left: 20px; padding-right:20px;">Blog</span></a> &#124; <a href="http://' . $_SERVER['SERVER_NAME'] . INDEX . '?option=gallery" style="text-decoration: none;"><span style="color: #5086ff; display: inline-block; padding-left: 20px; padding-right:20px;">Galerias</span></a> &#124; <a href="http://' . $_SERVER['SERVER_NAME'] . INDEX . '?option=portfolio" style="text-decoration: none; display: inline-block; padding-left: 20px; padding-right:20px;"><span style="color: #5086ff;">Portafolio</span></a> &#124; <a href="http://' . $_SERVER['SERVER_NAME'] . INDEX . '?option=contact" style="text-decoration: none; display: inline-block; padding-left: 20px; padding-right:20px;"><span style="color: #5086ff;">Contacto</span></a>
								
							</div>
							
						</span></span></td>
					</tr>
					<tr>
						<td>
							<span style=" display: block; width: 800px;font-family: Trebuchet MS, Arial, Sans-serif; font-size: 10px; color: #89a; text-align: left; margin-top: 10px;">
								Estas recibiendo este mensaje desde ' . SITE_NAME . ' por que eres uno de nuestros miembros mas valiosos, adem&aacute;s has dejado un comentario o enviado un mensaje desde nuestro sitio Web, ' . SITE_NAME . ' respeta tu privacidad y tus datos seran resguardados y jamas seran p&uacute;blicos.<br /><br />
								Arkos Noem Arenom, el logo Arkos Noem Arenom, LEAF Professionals y el logo LEAF Professionals, son marcas registradas de Jorge Alberto Jaime, en M&eacute;xico y otros paises. Todas las otras marcas son propiedad de sus respectivos due&ntilde;os.<br /><br />
							</span>
						</td>
					</tr>
				</table>
			</body>
			</html>';
		$message_owner = '<html>
			<body>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td background="http://' . $_SERVER['SERVER_NAME'] . INDEX . 'images/mail/bgs.top.png" bgcolor="#112233"><span style="display:block; height: 80px; width: 800px;">
							<img src="http://' . $_SERVER['SERVER_NAME'] . INDEX . 'images/mail/email.arkosnoemarenom.png" />
						</span></td>
					</tr>
					<tr>
						<td><span style="display: block; background-color: #123; width: 800px; font-size: 11px; font-family: Lucida Sans Unicode, Sans-serif; color: #d0eaff;"><span style="display: block; padding: 20px;">
							<div align="right"><img src="http://' . $_SERVER['SERVER_NAME'] . INDEX . 'images/mail/emailing.png" /></div>
							
							Estas recibiendo este correo por que han dejado un nuevo comentario en ' . SITE_NAME . ', el comentario es el siguiente:<br /><br />
							<span style="display:block; padding: 10px; border: 1px dotted #456; margin-top: 20px; margin-bottom: 20px; color: #a0bacf; background-color: #234;">
								' . nl2br($user_comment) . '
							</span>
							
							En cuanto te sea posible, responde a este comentario.
							
							<div align="center" style="display:block; font-size: 10px; margin-top: 30px;">
								
								<a href="http://' . $_SERVER['SERVER_NAME'] . INDEX . '" style="text-decoration: none;"><span style="color: #5086ff; display: inline-block; padding-left: 20px; padding-right:20px;">Blog</span></a> &#124; <a href="http://' . $_SERVER['SERVER_NAME'] . INDEX . '?option=gallery" style="text-decoration: none;"><span style="color: #5086ff; display: inline-block; padding-left: 20px; padding-right:20px;">Galerias</span></a> &#124; <a href="http://' . $_SERVER['SERVER_NAME'] . INDEX . '?option=portfolio" style="text-decoration: none; display: inline-block; padding-left: 20px; padding-right:20px;"><span style="color: #5086ff;">Portafolio</span></a> &#124; <a href="http://' . $_SERVER['SERVER_NAME'] . INDEX . '?option=contact" style="text-decoration: none; display: inline-block; padding-left: 20px; padding-right:20px;"><span style="color: #5086ff;">Contacto</span></a>
								
							</div>
							
						</span></span></td>
					</tr>
					<tr>
						<td>
							<span style=" display: block; width: 800px;font-family: Trebuchet MS, Arial, Sans-serif; font-size: 10px; color: #89a; text-align: left; margin-top: 10px;">
								Estas recibiendo este mensaje desde ' . SITE_NAME . ' por que eres uno de nuestros miembros mas valiosos, adem&aacute;s has dejado un comentario o enviado un mensaje desde nuestro sitio Web, ' . SITE_NAME . ' respeta tu privacidad y tus datos seran resguardados y jamas seran p&uacute;blicos.<br /><br />
								Arkos Noem Arenom, el logo Arkos Noem Arenom, LEAF Professionals y el logo LEAF Professionals, son marcas registradas de Jorge Alberto Jaime, en M&eacute;xico y otros paises. Todas las otras marcas son propiedad de sus respectivos due&ntilde;os.<br /><br />
							</span>
						</td>
					</tr>
				</table>
			</body>
			</html>';
		if ( @mail($user_email, SITE_NAME . ' | Comentario', $message, $headers) ){
			//$message_owner
			@mail('arkos.noem.arenom@gmail.com',$user_nickname . ' | Nuevo comentario', $message_owner, $headers);
			$return_value = 1;
		}
		else
			$return_value = 0;
		return $return_value;
	}
	
	/**
	*
	*	Funcion que envia un correo electronico a un usuario cuando envia un comentario 
	*	desde el formulario de contacto, finalmente envia un aviso con todo el contenido
	*	del comentario al due�o del sitio Web	
	*/
	
	function sndemail_user($mail_username = 'none', $mail_usermail = 'no@mail.com', $mail_userwurl = 'http://', $mail_usersubj = 'invalid contact', $mail_usermess = 'No message' ){
		global $headers;
		$return_value = 0;
		$message = '<html>
			<body>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td background="http://' . $_SERVER['SERVER_NAME'] . INDEX . 'images/mail/bgs.top.png" bgcolor="#112233"><span style="display:block; height: 80px; width: 800px;">
							<img src="http://' . $_SERVER['SERVER_NAME'] . INDEX . 'images/mail/email.arkosnoemarenom.png" />
						</span></td>
					</tr>
					<tr>
						<td><span style="display: block; background-color: #123; width: 800px; font-size: 11px; font-family: Lucida Sans Unicode, Sans-serif; color: #d0eaff;"><span style="display: block; padding: 20px;">
							<div align="right"><img src="http://' . $_SERVER['SERVER_NAME'] . INDEX . 'images/mail/emailing.png" /></div>
							
							Estas recibiendo este correo por que has enviado un mensaje directamente a ' . SITE_NAME . ', tu mensaje es el siguiente:<br /><br />
							<span style="display:block; padding: 10px; border: 1px dotted #456; margin-top: 20px; margin-bottom: 20px; color: #a0bacf; background-color: #234;">
								' . nl2br($mail_usermess) . '
							</span>
							
							En cuanto me sea posible, respondere a tu comentario.
							
							<div align="center" style="display:block; font-size: 10px; margin-top: 30px;">
								
								<a href="http://' . $_SERVER['SERVER_NAME'] . INDEX . '" style="text-decoration: none;"><span style="color: #5086ff; display: inline-block; padding-left: 20px; padding-right:20px;">Blog</span></a> &#124; <a href="http://' . $_SERVER['SERVER_NAME'] . INDEX . '?option=gallery" style="text-decoration: none;"><span style="color: #5086ff; display: inline-block; padding-left: 20px; padding-right:20px;">Galerias</span></a> &#124; <a href="http://' . $_SERVER['SERVER_NAME'] . INDEX . '?option=portfolio" style="text-decoration: none; display: inline-block; padding-left: 20px; padding-right:20px;"><span style="color: #5086ff;">Portafolio</span></a> &#124; <a href="http://' . $_SERVER['SERVER_NAME'] . INDEX . '?option=contact" style="text-decoration: none; display: inline-block; padding-left: 20px; padding-right:20px;"><span style="color: #5086ff;">Contacto</span></a>
								
							</div>
							
						</span></span></td>
					</tr>
					<tr>
						<td>
							<span style=" display: block; width: 800px;font-family: Trebuchet MS, Arial, Sans-serif; font-size: 10px; color: #89a; text-align: left; margin-top: 10px;">
								Estas recibiendo este mensaje desde ' . SITE_NAME . ' por que eres uno de nuestros miembros mas valiosos, adem&aacute;s has dejado un comentario o enviado un mensaje desde nuestro sitio Web, ' . SITE_NAME . ' respeta tu privacidad y tus datos seran resguardados y jamas seran p&uacute;blicos.<br /><br />
								Arkos Noem Arenom, el logo Arkos Noem Arenom, LEAF Professionals y el logo LEAF Professionals, son marcas registradas de Jorge Alberto Jaime, en M&eacute;xico y otros paises. Todas las otras marcas son propiedad de sus respectivos due&ntilde;os.<br /><br />
							</span>
						</td>
					</tr>
				</table>
			</body>
			</html>';
		$message_owner = '<html>
			<body>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td background="http://' . $_SERVER['SERVER_NAME'] . INDEX . 'images/mail/bgs.top.png" bgcolor="#112233"><span style="display:block; height: 80px; width: 800px;">
							<img src="http://' . $_SERVER['SERVER_NAME'] . INDEX . 'images/mail/email.arkosnoemarenom.png" />
						</span></td>
					</tr>
					<tr>
						<td><span style="display: block; background-color: #123; width: 800px; font-size: 11px; font-family: Lucida Sans Unicode, Sans-serif; color: #d0eaff;"><span style="display: block; padding: 20px;">
							<div align="right"><img src="http://' . $_SERVER['SERVER_NAME'] . INDEX . 'images/mail/emailing.png" /></div>
							
							Estas recibiendo este correo por te han enviado un mensaje directamente desde ' . SITE_NAME . ', el mensaje es el siguiente:<br /><br />
							<span style="display:block; padding: 10px; border: 1px dotted #456; margin-top: 20px; margin-bottom: 20px; color: #a0bacf; background-color: #234;">
								' . nl2br($mail_usermess) . '
								<br /><br />
								<b>Datos del autor:</b><br />
								Nombre: ' . $mail_username . '<br />
								Correo: ' . $mail_usermail . '<br />
								Web: ' . $mail_userwurl . '<br />
								IP: ' . getRealIp() . '<br />
								Fecha: ' . date('r') . '
							</span>
							
							En cuanto me sea posible, responde a este comentario.
							
							<div align="center" style="display:block; font-size: 10px; margin-top: 30px;">
								
								<a href="http://' . $_SERVER['SERVER_NAME'] . INDEX . '" style="text-decoration: none;"><span style="color: #5086ff; display: inline-block; padding-left: 20px; padding-right:20px;">Blog</span></a> &#124; <a href="http://' . $_SERVER['SERVER_NAME'] . INDEX . '?option=gallery" style="text-decoration: none;"><span style="color: #5086ff; display: inline-block; padding-left: 20px; padding-right:20px;">Galerias</span></a> &#124; <a href="http://' . $_SERVER['SERVER_NAME'] . INDEX . '?option=portfolio" style="text-decoration: none; display: inline-block; padding-left: 20px; padding-right:20px;"><span style="color: #5086ff;">Portafolio</span></a> &#124; <a href="http://' . $_SERVER['SERVER_NAME'] . INDEX . '?option=contact" style="text-decoration: none; display: inline-block; padding-left: 20px; padding-right:20px;"><span style="color: #5086ff;">Contacto</span></a>
								
							</div>
							
						</span></span></td>
					</tr>
					<tr>
						<td>
							<span style=" display: block; width: 800px;font-family: Trebuchet MS, Arial, Sans-serif; font-size: 10px; color: #89a; text-align: left; margin-top: 10px;">
								Estas recibiendo este mensaje desde ' . SITE_NAME . ' por que eres uno de nuestros miembros mas valiosos, adem&aacute;s has dejado un comentario o enviado un mensaje desde nuestro sitio Web, ' . SITE_NAME . ' respeta tu privacidad y tus datos seran resguardados y jamas seran p&uacute;blicos.<br /><br />
								Arkos Noem Arenom, el logo Arkos Noem Arenom, LEAF Professionals y el logo LEAF Professionals, son marcas registradas de Jorge Alberto Jaime, en M&eacute;xico y otros paises. Todas las otras marcas son propiedad de sus respectivos due&ntilde;os.<br /><br />
							</span>
						</td>
					</tr>
				</table>
			</body>
			</html>';
		if ( @mail($mail_usermail,$mail_username . ' | ' . $mail_usersubj, $message, $headers) ){
			//$message_owner
			@mail('arkos.noem.arenom@gmail.com',$mail_username . ' | ' . $mail_usersubj, $message_owner, $headers);
			$return_value = 1;
		}
		else
			$return_value = 0;
		return $return_value;
		
	}
	
	
	/**
	*
	*	Funcion que se encarga de mostrar las advertencias o mensajes de aviso,
	*	despues de ejecutar alguna accion de la cual el resultado sea regresado
	*	por metodo GET
	*/
	
	function admin_adverticement(){
		if ( isset($_GET['advflag']) && strlen(trim($_GET['advflag'])) >= 4 && $_GET['advflag'] == 'true' ){
			echo '<div class="advertices" align="center"><span class="inner-adv">';
				
				switch($_GET['advtype']){
					
					case 'session':
						switch($_GET['advoption']){
							case 'close':
								if ($_GET['advres'] == 'true'){
									$adv_option    = 'success';
									$adv_message   = 'Ok, sesi&oacute;n terminada correctamente, esperamos verte por aca muy pronto.';
								}
								else{
									$adv_option    = 'alert';
									$adv_message   = 'La sesi&oacute;n ya habia caducado, ten cuidado con esto!.';
								}
								break;
							default:
								$adv_option    = 'alert';
								$adv_message   = 'Aggghhh! algo fallo en el aviso, no sabemos que fue, esperamos que no sea una molestia =P.';
						}
						break;
					
					case 'comment':
						switch($_GET['advoption']){
							case 'publish':
								if ($_GET['advres'] == 'true'){
									$adv_option    = 'success';
									$adv_message   = 'Perfecto, tu comentario ha sido publicado, puedes revisarlo abajo.';
								}
								break;
							default:
								$adv_option    = 'alert';
								$adv_message   = 'Aggghhh! algo fallo en el aviso, no sabemos que fue, esperamos que no sea una molestia =P.';
						}
						break;
					
					case 'email':
						switch($_GET['advoption']){
							case 'send':
								if ($_GET['advres'] == 'true'){
									$adv_option    = 'success';
									$adv_message   = 'Perfecto, tu mensaje ha sido enviado, ahora solo espera la respuesta.';
								}
								break;
							default:
								$adv_option    = 'alert';
								$adv_message   = 'Aggghhh! algo fallo en el aviso, no sabemos que fue, esperamos que no sea una molestia =P.';
						}
						break;
					default: 
						$adv_option    = 'alert';
						$adv_message   = 'Aggghhh! algo fallo en el aviso, no sabemos que fue, esperamos que no sea una molestia =P.';
				}
				echo '<div class="messages box-' . $adv_option . '">' . $adv_message . '</div>';
				
			echo '</span></div>';
			
			
		}
	}
	
	
	// funcion para convertir URL's en links
	function strtolink($str = ""){
		return ereg_replace("[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]","<i><a href=\"\\0\" target=\"_blank\">\\0</a></i>", $str); 
	}
	
	
	/**
	 *
	 *	Parse Tweets
	 *	
	 */
	
	// funcion que hace los reemplazos para el tweet de twitter
	function clean_tweet($tweet)
	{
		$regexps = array
		(
			"link" => '/[a-z]+:\/\/[a-z0-9-_]+\.[a-z0-9-_:~%&\?\+#\/.=]+[^:\.,\)\s*$]/i',
			"at" => '/(^|[^\w]+)\@([a-zA-Z0-9_]{1,15}(\/[a-zA-Z0-9-_]+)*)/',
			"hash" => "/(^|[^&\w'\"]+)\#([a-zA-Z0-9_]+)/"
		);
		
		foreach ($regexps as $name => $re)
		{
			$tweet = preg_replace_callback($re, 'parse_tweet_'.$name, $tweet);
		}
		
		return $tweet;
	}
	
	/*
	* Wrap a link element around URLs matched via preg_replace()
	*/
	function parse_tweet_link($m)
	{
		return '<i><a href="'.$m[0].'" target="_blank">'.((strlen($m[0]) > 25) ? substr($m[0], 0, 24).'...' : $m[0]).'</a></i>';
	}
	
	/*
	* Wrap a link element around usernames matched via preg_replace()
	*/
	function parse_tweet_at($m)
	{
		return $m[1].'<a href="http://twitter.com/'.$m[2].'" target="_blank">@'.$m[2].'</a>';
	}
	
	/*
	* Wrap a link element around hashtags matched via preg_replace()
	*/
	function parse_tweet_hash($m)
	{
		return $m[1].'<i><a href="http://search.twitter.com/search?q=%23'.$m[2].'" target="_blank">#'.$m[2].'</a></i>';
	}
	
	/**
	 *
	 *	End: parse tweets
	 *	
	 */
	
?>