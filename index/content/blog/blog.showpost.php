<?php
    $showcomments = 0;
    echo '<span class="posts">';
        if ( isset($_GET['post_id']) && strlen(trim($_GET['post_id'])) > 0 ){
        
            if ( !preg_match('/[^0-9]+/',$_GET['post_id']) ){
                
                $sql_thispost = 'SELECT * FROM web_posts WHERE post_id = ' . $_GET['post_id'] . ' AND post_published != 0 LIMIT 1';
                $res_thispost = exeQuery($sql_thispost);
                if ( mysql_num_rows($res_thispost) == 1 ){
                    $showcomments = 1;
                    $thisposts = mysql_fetch_array($res_thispost);
                    echo '<span class="post-title">' . $thisposts['post_title'] . '</span>';
                    echo '<span class="post-info">';
                        echo 'Publicado por: <a href="javascript:void(0);">' . userid_completename($thisposts['post_user']) . '</a>';
                        echo ' el <b>' . str_replace($arraydateen,$arraydatees,date('l d  \d\e F \d\e Y &#64; h:i:s a',(strtotime($thisposts['post_date_created']) - 10800))) . '</b><br />';
                        echo 'En la categoria: <a href="' . INDEX . '?option=index&call=category&category=' . $thisposts['post_category'] . '">' . post_category($thisposts['post_category']) . '</a> &#124; Con los tags: ';
                        posts_tags($thisposts['post_tags']);
                        echo '<br />';
                    echo '</span>';
                    echo '<span class="post-content">' . $thisposts['post_content'] . '</span>';
                }
                else
                    echo 'No se encontro el post con el id: <b>' . ((strlen(trim($_GET['post_id']))>0)?$_GET['post_id']:'&lt;empty&gt;') . '</b>';
            }
            else
                echo 'No se encontro el post con el id: <b>' . ((strlen(trim($_GET['post_id']))>0)?$_GET['post_id']:'&lt;empty&gt;') . '</b>';
        }
        else{
            echo 'No se encontro el post con el id: <b>' . ((strlen(trim($_GET['post_id']))>0)?$_GET['post_id']:'&lt;empty&gt;') . '</b>';
        }
    echo '</span>';
    
    if ( $showcomments ){
        echo '<a name="comments-index"></a>';
        $sql_postcomments = 'SELECT * FROM web_comments WHERE com_post = ' . $_GET['post_id'] . ' ORDER BY com_date_created DESC;';
        $res_postcomments = exeQuery($sql_postcomments);
        if ( mysql_num_rows($res_postcomments) > 0 ){
            echo '<a name="comments-index"></a>';
            echo '<span class="bartitle recentcomments" style="width: 100%;"></span>';
            
            $comment_classtoggle = 1;
            while ( $postcomments = mysql_fetch_array($res_postcomments) ){
                echo '<a name="comment-' . $postcomments['com_id'] . '"></a>';
                echo '<span class="postcomment ';
                    if ( $comment_classtoggle == 1 ){
                        echo 'on';
                        $comment_classtoggle = 0;
                    }
                    else{
                        if ( $comment_classtoggle == 0 ){
                            echo 'off';
                            $comment_classtoggle = 1;
                        }
                    }
                echo '">';
                    echo '<table cellpadding="0" cellspacing="0" border="0" width="100%"><tr><td>';
                    echo '<img src="http://www.gravatar.com/avatar.php?gravatar_id=' . md5($postcomments['com_email']) . '&size=40" class="comimage" />';
                    echo '<a href="' . ((strlen(trim($postcomments['com_url'])) > 0)?$postcomments['com_url']:'http://' . $_SERVER['SERVER_NAME']) . '" class="comment-url" title="' . ((strlen(trim($postcomments['com_url'])) > 0)?$postcomments['com_url']:'http://' . $_SERVER['SERVER_NAME']) . '" target="_blank"><b>' . $postcomments['com_autor'] . '</b></a> dijo:<br />';
                    //echo eregi_replace('([[:alnum:]]+://[^[:space:]]*[[:alnum:]#?/&= +%_:]]*)','<a href="\\1" target="_blank">\\1</a>',nl2br($postcomments['com_comment']));
                    echo nl2br($postcomments['com_comment']);
                    echo '<span class="com-info">';
                        echo 'Publicado el ' . str_replace($arraydateen,$arraydatees,date('l d  \d\e F \d\e Y &#64; h:i:s a',strtotime($postcomments['com_date_created']))) . ' &#124; ';
                        echo 'IP: ' . $postcomments['com_autor_ip'];
                    echo '</span>';
                    echo '</td></tr></table>';
                echo '</span>';
            }
            echo '<span class="comment-break"></span>';
        }
        
        echo '<a name="comments-leavecomment"></a>';
        echo '<span class="bartitle leave-comment" style="width: 100%;"></span>';
        
        //echo '<div class="box-info">Disculpe las molestias, pero por el momento no puede dejar comentarios.</div>';
        
        if ( $_POST['lc_usersubmit'] == 'true' ){
            
            if ( strlen(trim($_POST['lc_username'])) <= 0 || !is_email($_POST['lc_useremail']) || strlen(trim($_POST['lc_usercomment'])) <= 0 || $_POST['lc_usercptch'] != $_SESSION['tmptxt'] || getRealIp() == '91.214.45.7' || substr(getRealIp(),0,6) == '91.214' || !empty($_POST['lc_antispam']) ){
                if ( strlen(trim($_POST['lc_username'])) <= 0 )
                    echo '<div class="messages box-alert">Por favor coloca tu nombre.</div>';
                if ( !is_email($_POST['lc_useremail']) )
                    echo '<div class="messages box-alert">Da un correo electronico valido.</div>';
                if ( strlen(trim($_POST['lc_usercomment'])) <= 0 )
                    echo '<div class="messages box-alert">No puedes enviar mensajes vacios.</div>';
                if ( $_POST['lc_usercptch'] != $_SESSION['tmptxt'] )
                    echo '<div class="messages box-alert">El c&oacute;digo captcha es incorrecto.</div>';
                if ( getRealIp() == '91.214.45.7' || substr(getRealIp(),0,6) == '91.214' )
                    echo '<div class="messages box-alert">Esta IP ha sido bloqueada.</div>';
                if ( !empty($_POST['lc_antispam']) )
                    echo '<div class="messages box-alert">Lo sentimos, pero estas considerado como SPAM y tu comentario no se publicara.</div>';
            }
            else{
                if ( strlen(trim($_POST['lc_username'])) > 0 && is_email($_POST['lc_useremail']) && strlen(trim($_POST['lc_usercomment'])) > 0 && $_POST['lc_usercptch'] == $_SESSION['tmptxt'] && getRealIp() != '91.214.45.7' && substr(getRealIp(),0,6) != '91.214' && empty($_POST['lc_antispam']) ){
                    if ( strlen(trim($_POST['lc_userhttp'])) > 7 && $_POST['lc_userhttp'] != 'http://' )
                        if ( substr($_POST['lc_userhttp'],0,7) == 'http://' )
                            $comm_url = $_POST['lc_userhttp'];
                        else
                            $comm_url = 'http://' . $_POST['lc_userhttp'];
                    else
                        $comm_url = '';
                    $sql_savecomment = 'INSERT INTO web_comments VALUES(
                        NULL,
                        "' . $_GET['post_id'] . '",
                        "' . str_replace($car_esp,$car_hex,$_POST['lc_username']) . '",
                        "' . str_replace($car_esp,$car_hex,$_POST['lc_usercomment']) . '",
                        "' . $_POST['lc_useremail'] . '",
                        "' . $comm_url . '",
                        "' . getRealIp() . '",
                        NULL
                    );';
                    
                    if ( $_POST['lc_antispam'] == '' && strlen($_POST['lc_antispam']) == 0 ){
                        if ( substr_count($_POST['lc_usercomment'], 'quizilla') == 0 && substr_count($_POST['lc_usercomment'], 'viddler') == 0 && substr_count($_POST['lc_usercomment'], 'ted.com') == 0 && substr_count($_POST['lc_usercomment'], 'upcoming') == 0 && substr_count(strtolower($_POST['lc_usercomment']), 'nude') == 0 && substr_count(strtolower($_POST['lc_usercomment']), 'teen') == 0 && substr_count(strtolower($_POST['lc_usercomment']), 'models') == 0 && substr_count(strtolower($_POST['lc_usercomment']), 'fotolog' ) == 0 && substr_count(strtolower($_POST['lc_usercomment']), 'livejournal' ) == 0 && substr_count(strtolower($_POST['lc_usercomment']), 'http' ) == 0 && substr_count(strtolower($_POST['lc_usercomment']), 'collegehumor' ) == 0 ){
                            if ( exeQuery($sql_savecomment) ){
                                echo '<div class="messages box-success">Ok, tu comentario esta siendo guardado, por favor espera&#133;</div>';
                                echo '<script type="text/javascript">window.top.location.href="' . INDEX . '?option=' . $_GET['option'] . '&call=' . $_GET['call'] . '&post_id=' . $_GET['post_id'] . '&advflag=true&advtype=comment&advoption=publish&advres=true";</script>';
                                @advertice_userleavecomment($_POST['lc_username'], $_POST['lc_useremail'], $_POST['lc_usercomment']);
                            }
                            else{
                                echo '<div class="messages box-alert">Fuck you SPAM.</div>';
                            }
                        }
                        else{
                            echo '<div class="messages box-alert">Fuck you SPAM.</div>';
                        }

                    }
                    else{
                        echo '<div class="messages box-alert">Fuck you SPAM.</div>';
                    }
                }
            }
            
        }
        echo '<form method="post" action="' . INDEX . '?option=' . $_GET['option'] . '&call=' . $_GET['call'] . '&post_id=' . $_GET['post_id'] . '#comments-leavecomment">';
            echo '<input type="text" name="lc_antispam" style="display: none;" value="" />';
            echo '<table cellpadding="0" cellspacing="0" border="0">';
                echo '<tr>';
                    echo '<td style="vertical-align:middle;">';
                        echo '<span class="advertice-red">*</span>Nombre:';
                    echo '</td>';
                    echo '<td>';
                        echo '<span class="left-input"><span class="right-input"><input type="text" name="lc_username" class="middle-input" title="Este campo es obligatorio."';
                            if ( $_POST['lc_usersubmit'] == 'true' )
                                echo ' value="' . $_POST['lc_username'] . '"';
                        echo ' /></span></span>';
                    echo '</td>';
                echo '</tr>';
                echo '<tr>';
                    echo '<td style="vertical-align:middle;">';
                        echo '<span class="advertice-red">*</span>Correo:';
                    echo '</td>';
                    echo '<td>';
                        echo '<span class="left-input"><span class="right-input"><input type="text" name="lc_useremail" class="middle-input" title="Este campo es obligatorio, pero tu correo no ser&aacute; p&iacute;blico"';
                            if ( $_POST['lc_usersubmit'] == 'true' )
                                echo ' value="' . $_POST['lc_useremail'] . '"';
                        echo ' /></span></span>';
                    echo '</td>';
                echo '</tr>';
                echo '<tr>';
                    echo '<td style="vertical-align:middle;">';
                        echo '&nbsp;&nbsp;Web:';
                    echo '</td>';
                    echo '<td>';
                        echo '<span class="left-input"><span class="right-input"><input type="text" name="lc_userhttp" class="middle-input"';
                            if ( $_POST['lc_usersubmit'] == 'true' )
                                echo ' value="' . $_POST['lc_userhttp'] . '"';
                        echo ' /></span></span>';
                    echo '</td>';
                echo '</tr>';
                
                echo '<tr>';
                    echo '<td style="vertical-align:middle;">';
                        echo '<img src="captcha.blog.comment.php" />';
                    echo '</td>';
                    echo '<td>';
                        echo '<span class="left-input"><span class="right-input"><input type="text" name="lc_usercptch" class="middle-input"  title="Coloca el c&oacute;digo de la imagen (sensible a mayusculas y minusculas)" /></span></span>';
                    echo '</td>';
                echo '</tr>';
                
                /*echo '<tr>';
                    echo '<td colspan="2">';
                        //echo '<span class="advertice-red">*</span>';
                        echo '<table cellpadding="0" cellspacing="0" border="0">';
                            echo '<tr>';
                                echo '<td>';
                                    echo '<img src="captcha.blog.comment.php" />';
                                echo '</td>';
                                echo '<td valign="middle">';
                                    echo '<span class="left-input"><span class="right-input"><input type="text" name="lc_usercaptcha" class="middle-input" style="width: 200px; background-color: #000;" /></span></span>';
                                echo '</td>';
                            echo '</tr>';
                        echo '</table>';
                    echo '</td>';
                echo '</tr>';*/
                echo '<tr>';
                    echo '<td style="padding-top: 10px;">';
                        echo '<span class="advertice-red">*</span>Comentario:';
                    echo '</td>';
                    echo '<td><span class="comment-area">';
                        echo '<table cellpadding="0" cellspacing="0" border="0">';
                            echo '<tr>';
                                echo '<td><img src="images/bdr/bgs.inputleavecomment.textarea.topleft.png" /></td>';
                                echo '<td style="background-image:url(images/bdr/bgs.inputleavecomment.textarea.topmiddle.png); background-repeat: repeat-x; background-position: 0px 0px;"></td>';
                                echo '<td><img src="images/bdr/bgs.inputleavecomment.textarea.topright.png" /></td>';
                            echo '</tr>';
                            echo '<tr>';
                                echo '<td style="background-image:url(images/bdr/bgs.inputleavecomment.textarea.leftmiddle.png); background-repeat: repeat-y; background-position: 0px 0px; width: 5px;"></td>';
                                echo '<td style="background-image:url(images/bdr/bgs.inputleavecomment.textarea.centermiddle.png); background-repeat: repeat; background-position: 0px 0px; width: 5px;">';
                                    echo '<textarea class="text-input" name="lc_usercomment" title="Escribe aqu&iacute; tu comentario.">';
                                        if ( $_POST['lc_usersubmit'] == 'true' )
                                            echo trim($_POST['lc_usercomment']);
                                    echo '</textarea>';
                                echo '</td>';
                                echo '<td style="background-image:url(images/bdr/bgs.inputleavecomment.textarea.rightmiddle.png); background-repeat: repeat-y; background-position: 0px 0px; width: 5px;"></td>';
                            echo '</tr>';
                            echo '<tr>';
                                echo '<td><img src="images/bdr/bgs.inputleavecomment.textarea.bottomleft.png" /></td>';
                                echo '<td style="background-image:url(images/bdr/bgs.inputleavecomment.textarea.bottommiddle.png); background-repeat: repeat-x; background-position: 0px 0px;"></td>';
                                echo '<td><img src="images/bdr/bgs.inputleavecomment.textarea.bottomright.png" /></td>';
                            echo '</tr>';
                        echo '</table>';
                    echo '</span></td>';
                echo '</tr>';
                echo '<tr>';
                    echo '<td colspan="2" align="right" style="vertical-align:middle;">';
                        echo '<span style="float: left; font-size: 10px;">Los campos marcados con (<span class="advertice-red">*</span>) son obligatorios. Captcha es obligatorio</span>';
                        echo '<button type="submit" name="lc_usersubmit" value="true"><span class="left-button"><span class="right-button">Publicar comentario</span></span></button>';
                    echo '</td>';
                echo '</tr>';
            echo '</table>';
        echo '</form>';
    }
?>
