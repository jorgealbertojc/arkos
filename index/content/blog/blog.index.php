<?php
	
	echo '<table cellpadding="0" cellspacing="0" border="0" class="contenttable">';
		echo '<tr>';
			echo '<td>';
				switch($_GET['call']){
					case 'search': include_once 'blog.search.php'; break;
					case 'category': include_once 'blog.category.php'; break;
					case 'show_post': include_once 'blog.showpost.php'; break;
					default: include_once 'blog.allposts.php';
				}
			echo '</td>';
			echo '<td><span class="content-break"></span></td>';
			echo '<td>';
				
				// incluimos la barra media
				include_once 'blog.middlecontent.php';
				
			echo '</td>';
			echo '<td><span class="content-break"></span></td>';
			echo '<td><span class="mdlrgt-content">';
				
				// incluimos la barra final
				include_once 'blog.rightcontent.php';
				
			echo '</span></td>';
		echo '</tr>';
	echo '</table>';
?>
