<?
	include_once 'includes/config.inc.php';
	include_once 'includes/functions.inc.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><? echo SITE_NAME; ?></title>
		<style type="text/css">
		*{
			margin-top: 0px;
			margin-bottom: 0px;
			margin-left: 0px;
			margin-right: 0px;
			border-top: 0px;
			border-bottom: 0px;
			border-left: 0px;
			border-right: 0px;
			padding-top: 0px;
			padding-bottom: 0px;
			padding-left: 0px;
			padding-right: 0px;
			font-family: "Lucida Sans Unicode", sans-serif;
			text-decoration: none;
			line-height: 18px;
		}
		body{
			font-size: 11px;
			color: #405a6f;
			background-color: #fff;
			word-spacing: 3px;
			/*background-image:url('../images/bgs/bgs.body.jpg');
			background-repeat: no-repeat;
			background-position: 50% 0px;
			background-attachment: scroll;*/
		}
		.inner05{
			display: block;
			padding: 5px;
		}
		.inner10{
			display: block;
			padding: 10px;
		}
		.inner20{
			display: block;
			padding: 20px;
		}
		#allerrors{
			display: block;
			position: fixed;
			top: 0px;
			left: 0px;
			height: 100%;
			width:100%;
		}
		#error{
			display: block;
			width: 500px;
			background-color: #fafafa;
			border: 1px solid #eaeaea;
			text-align: left;
			-webkit-border-radius: 5px;
			-moz-border-radius: 5px;
		}
		.red-error{
			display: block;
			color: #ed1c24;
			margin-bottom: 10px;
		}
		.blue-error{
			display: block;
			color: #0099ff;
			margin-bottom: 10px;
		}
		.chick{
			font-family: Calibri;
			font-size: 15px;
		}
		.maschick{
			font-family: Calibri;
			font-size: 12px;
		}
		</style>
	</head>
	<body><div id="allerrors"><table cellpadding="0" cellspacing="0" border="0" width="100%"><tr><td valign="middle" align="center">
		<div id="error"><span class="inner20">
			<b class="red-error chick">P&aacute;gina no encontrada &#124; Error 404</b>
			<span class="maschick">Lo sentimos, pero parece que no pudimos encontrar el sitio que estas buscando.</span>
		</span></div>
	</td></tr></table></div></body>
</html>