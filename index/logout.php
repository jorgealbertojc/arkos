<?php
	session_start();
	if ( isset($_SESSION['session_user_id']) && isset($_SESSION['session_user_name']) ){
		header("Cache-control: private");
		session_unset();
		session_destroy();
		header('Location: ' . urldecode($_GET['redirect']) . ((strpos($_GET['redirect'],'?'))?'&':'?') . 'advflag=true&advtype=session&advoption=close&advres=true');
	}
	else
		header('Location: ' . urldecode($_GET['redirect']) . ((strpos($_GET['redirect'],'?'))?'&':'?') . 'advflag=true&advtype=session&advoption=close&advres=false');
	exit(0);
?>
