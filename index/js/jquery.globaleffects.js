$(document).ready(function() {
	
	// SexyLightbox
	SexyLightbox.initialize({color:'white', dir: 'sexyimages'});
	
	// Tooltip - Comments, Category, Blog Files
	$('a.comments[title]').qtip({style: {border: {width: 0,radius: 3},padding: '3px 9px',textAlign: 'left',tip: {corner: true,color: false,size: {width: 4,height: 8},opacity: 0},name: 'green'},position: {corner: {tooltip: 'rightMiddle',target: 'leftMiddle'}}});
	// tooltip - Blogroll
	$('a.blogedroll[title]').qtip({style: {border: {width: 0,radius: 3},padding: '3px 9px',textAlign: 'left',tip: {corner: true,color: false,size: {width: 4,height: 8},opacity: 0},name: 'blue'},position: {corner: {tooltip: 'rightMiddle',target: 'leftMiddle'}}});
	// tooltip - Gallery
	$('a.gallerybg[title]').qtip({style: {border: {width: 0,radius: 3},padding: '3px 9px',textAlign: 'left',tip: {corner: true,color: false,size: {width: 4,height: 8},opacity: 0},name: 'cream'},position: {corner: {tooltip: 'rightMiddle',target: 'leftMiddle'}}});
	
	// tooltip - comments form
	$('.middle-input[title], .text-input[title]').qtip({show: {when: {target: false,event: 'focus'},effect: {type: 'fade',length: 300},delay: 0,solo: false,ready: false},hide: {when: {target: false,event: 'unfocus'},effect: {type: 'fade',length: 300},delay: 0,fixed: false},style: {border: {width: 0,radius: 3},padding: '3px 9px',textAlign: 'left',tip: {corner: true,color: false,size: {width: 8,height: 4},opacity: 0},name: 'cream'},position: {corner: {tooltip: 'bottomLeft',target: 'topRight'}}});
	// .comment-url[title]
	$('a.comment-url[title]').qtip({style: {border: {width: 0,radius: 3},padding: '3px 9px',textAlign: 'left',tip: {corner: true,color: false,size: {width: 8,height: 4},opacity: 0},name: 'mytheme'},position: {corner: {tooltip: 'bottomLeft',target: 'topRight'}}});
	$('.gallery-preview[title]').qtip({style: {border: {width: 0,radius: 3},padding: '3px 9px',textAlign: 'left',tip: {corner: true,color: false,size: {width: 8,height: 4},opacity: 0},name: 'green'},position: {corner: {tooltip: 'bottomMiddle',target: 'topMiddle'}}});
	
	
	
	// messages barbox
	var timeout = 15000;
	var msjtimeout = 1000;
	
	$('.messages').css('display','none');
	$('.messages').slideDown(500);
	setTimeout(function(){
		$('.messages').fadeTo(msjtimeout,0);
	},timeout);
	setTimeout(function(){
		$('.messages').slideUp(msjtimeout);
	},timeout+msjtimeout);
	
	$('.messages').click(function(){
		$('.messages').slideUp(msjtimeout);
	});
	
	// End: messages barbox
	
	// twitter items slideshow
	$('#twitterslide').innerfade({
		speed: 1000,
		timeout: 10000,
		type: 'sequence'//,
		//containerheight: '120px'
	});
	// End: twitter items slideshow
	
	$('#content-loginform').hide(0);
	$('.open-loginform').click(function(){
		$('#content-loginform').slideDown(1500);
	});
	$('.close-loginform').click(function(){
		$('#content-loginform').slideUp(1500);
	});
	
	$('.advertices').hide(0);
	$('.advertices').show(0);
	


	

	
});

// Accordion
ddaccordion.init({
	headerclass: "blogfiles-head",						//Shared CSS class name of headers group
	contentclass: "blogfilecontents",						//Shared CSS class name of contents group
	revealtype: "click",								//Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
	mouseoverdelay: 200,								//if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
	collapseprev: true,								//Collapse previous content (so only one open at any time)? true/false 
	defaultexpanded: [0],								//index of content(s) open by default [index1, index2, etc]. [] denotes no content.
	onemustopen: false,								//Specify whether at least one header should be open always (so never all headers closed)
	animatedefault: false,								//Should contents open by default be animated into view?
	persiststate: true,								//persist state of opened contents within browser session?
	toggleclass: ["colsedfile", "openfile"],					//Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
	togglehtml: ["none", "", ""],							//Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
	animatespeed: "fast",								//speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
	oninit:function(expandedindices){						//custom code to run when headers have initalized
		//do nothing
	},
	onopenclose:function(header, index, state, isuseractivated){	//custom code to run whenever a header is opened or closed
		//do nothing
	}
});