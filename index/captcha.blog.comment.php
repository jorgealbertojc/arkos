<?php 
	/*
		
		El n�mero est� encryptado en Base64.
		Ejemplo:
			Para el n�mero 1542, $key valdr�a: MTU0Mg==
			
		Obtener n�meros usando funciones de Base64:
			Para codificar: base64_encode()
			Para decodificar: base64_decode()
			
		Uso del script:
			<img src="imagen.php?key=MTU0Mg==">
			
			o bien:
			
			http://localhost/imagen.php?key=MTU0Mg==
			
			Siendo "imagen.php" es script contendor del c�digo citado en este archivo.
			
		Dudas?
			-> http://www.php.net/manual/es/ref.image.php
			-> http://www.php.net/manual/es/function.base64-encode.php
			-> http://www.php.net/manual/es/function.base64-decode.php
			-> http://www.php.net/manual/es/function.header.php

		**************************************************************
		* Ejemplo escrito por: J. Arturo Ruz C.
		* Correo electr�nico: a r t u r o r u z @ m s n . c o m
		* No olviden escribir e intercambiar ideas ;)
		**************************************************************

	/********************************************************************************************************
	* IMPORTANTE: Este ejemplo est� adecuado para el uso de 4 caracteres como m�ximo para la cadena "$key". *
	********************************************************************************************************/
	
	session_start();
	
	function randomText($length) {
	  $pattern = "1234567890AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";
	  for($i=0;$i<$length;$i++) {
	    $key .= $pattern{rand( 0 , strlen($pattern) - 1)};
	  }
	  return $key;
	}
	
	
	$fondo = 'images/bgs/bgs.captcha.png';
	$font = 'fonts/aller.ttf';
	$_SESSION['tmptxt'] = randomText(5);
	
	//if(!isset($_GET['key'])) { $n=rand(1000,9999); } else { $n = $_SESSION['tmptxt']; }
	$n = $_SESSION['tmptxt'];
	// Se establece el cabecero del documento, en este ejemplo ser� del tipo Imagen GIF
	// Nota: El archivo GIF tiene mejor compresi�n ;)
	header('Content-Type: image/png');
	// Se genera el �rea del gr�fico
	//$grafico = imagecreate(130, 50);
	$grafico = imagecreatefrompng($fondo);
	// El primer color establecido ser� el color de fondo
	$fondo = imagecolorallocate($grafico, 0, 34, 68);
	// El siguiente color establecido ser� el color del texto
	$color = imagecolorallocate($grafico, 64, 90, 12);
	// Se establece el margen inicial para cada caracter escrito
	$margen = 5;
	// Se obtiene cada caracter de la cadena usando el loop for()
	for($x = 0; $x < strlen($n); $x++) {
		// Se extrae la cadena usando la funci�n substr()
		$c = substr($n,$x,1);
		// La inclinaci�n ser� 10 o -10 segun sea si el n�mero de caracter es par o no
		if(($x % 2)==0) { $rend = 10; } else { $rend = -10; }
		// Se escribe el caracter en el gr�fico
		imagettftext($grafico, 16, $rend, $margen, 20, $color, $font, $c);
		// Se incrementa el margen del siguiente caracter a escribir, en caso de existir
		$margen += 13;
	}
	// Se obtiene el gr�fico para mostrar en el navegador
	imagepng($grafico);
	// Destruye la imagen creada liberando la memoria
	imagedestroy($grafico);
	
	
?>