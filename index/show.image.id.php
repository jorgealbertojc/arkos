<?
	
	if ( isset($_GET['imageId']) && !preg_match('/[^0-9]+/',$_GET['imageId']) ){
		
		include_once '../includes/config.inc.php';
		include_once '../includes/functions.inc.php';
		if ( $idcnx_images = connect() ){
			$sql_image = 'SELECT image_id,image_dir FROM web_image_gallery WHERE image_id = ' . $_GET['imageId'] . ' LIMIT 1;';
			$res_image = exeQuery($sql_image);
			if ( mysql_num_rows($res_image) == 1 ){
				$image = mysql_fetch_array($res_image);
				if ( file_exists($image['image_dir']) ){
					$nombre = $image['image_dir'];
					if ( isset($_GET['w']) && !preg_match('/[^0-9]+/',$_GET['w']) )
						$anchura = (int)$_GET['w'];
					else
						$anchura = 80;
					if ( isset($_GET['h']) && !preg_match('/[^0-9]+/',$_GET['h']) )
						$hmax = (int)$_GET['h'];
					else
						$hmax = 80;
				}
				else{
					$nombre = 'images/ico/ico.nopicture.png';
					$anchura = 80;
					$hmax = 80;
				}
			}
			else{
				$nombre = 'images/ico/ico.nopicture.png';
				$anchura = 80;
				$hmax = 80;
			}
			mysql_close($idcnx_images);
		}
		else{
			$nombre = 'images/ico/ico.nopicture.png';
			$anchura = 80;
			$hmax = 80;
		}
		$datos = getimagesize($nombre);
		if($datos[2]==1){
			$img = @imagecreatefromgif($nombre);
		}
		if($datos[2]==2){
			$img = @imagecreatefromjpeg($nombre);
		}
		if($datos[2]==3){
			$img = @imagecreatefrompng($nombre);
		}
		$ratio = ($datos[0] / $anchura);
		$altura = ($datos[1] / $ratio);
		if($altura>$hmax){
			$anchura2=$hmax*$anchura/$altura;
			$altura=$hmax;
			$anchura=$anchura2;
		}
		$thumb = imagecreatetruecolor($anchura,$altura);
		imagecopyresampled($thumb, $img, 0, 0, 0, 0, $anchura, $altura, $datos[0], $datos[1]);
		if($datos[2]==1){
			header("Content-type: image/gif"); imagegif($thumb);
		}
		if($datos[2]==2){
			header("Content-type: image/jpeg");imagejpeg($thumb);
		}
		if($datos[2]==3){
			header("Content-type: image/png");imagepng($thumb);
		}
		imagedestroy($thumb);
	}
?>