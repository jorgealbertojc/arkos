<?php
	/**
	*	Acciones que se deben realizan a la base de datos
	*	- ALTER TABLE com_comments ADD com_url VARCHAR(200) NOT NULL AFTER com_email;
	*	
	*
	*	Acciones que se deben modificar en el manejador
	*	- Modificar la respuesta a comentarios...	
	*/
	
	
	
	session_start();
	/*
	// NO TOCAR
	header('Accept-Ranges: bytes');
	$ExpStr = 'Expires: ' . gmdate("D, d M Y H:i:s", time() + 14400) . " GMT"; // 14400 = 4 horas
	header($ExpStr);
	header("Cache-Control: maxage=14400");
	header("Cache-Control: public, must-revalidate");
	header("Cache-Control: public");
	header("pragma: public");
	header("Content-Transfer-Encoding:gzip;q=1.0,identity;q=0.5,*;q=0");
	header("Cache-Control: cache");
	header("Pragma: cache");
	header('Content-Type: text/html; charset=iso-8859-1');
	setlocale(LC_TIME, 'es_VE'); # Localiza en español es_Venezuela
	// FIN NO TOCAR
	// elimina los espacios en blanco
	
	function compress_page($buffer) { $search = array('/\>[^\S ]+/s','/[^\S ]+\</s','/(\s)+/s'); $replace = array('> ',' <','\\1'); return preg_replace($search, $replace, $buffer); }
	ob_gzhandler(ob_start("compress_page"));
	ob_start('ob_gzhandler');*/
	
	include_once '../includes/config.inc.php';
	include_once '../includes/functions.inc.php';
	
	if ( $idcnx_site = connect() ){
		
		if ( file_exists('functions.my.php') )
			include_once 'functions.my.php';
		
		echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
		echo '<html xmlns="http://www.w3.org/1999/xhtml">';
		echo '<head>';
		echo '<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />';
		echo '<meta name="description" content="Desarrollo Web, Dise�o Gr�fico, Tecnolog�as de Internet, Curiosos de las TIC\'s, Desarrollo de Hardware, Planeaci�n de Proyectos de Software" />';
		echo '<meta name="keywords" content="arkos, noem, arenom, dise�o web, desarrollo web, rock metal, sitios web, tiendas virtuales, dise�o grafico, tecnolog�as de informaci�n" />';
		echo '<meta name="page-topic" content="Noticias, Desarrollos, Dise�os, Curiosidades, Tecnolog�as de la Informaci�n" />';
		echo '<meta name="author" content="Arkos Noem Arenom" />';
		echo '<meta name="generator" content="HTML done by hand - PSPad Editor" />';
		echo '<meta name="content-language" content="es-MX" />';
		echo '<meta name="robots" content="index,follow" />';
		echo '<meta name="google-site-verification" content="iIHQlKyzc2tJoSD8ik9j_IYD-ZxvGgnm536USvYeDTE" />';
		echo '<title>';
			if ( isset($_GET['call']) && $_GET['call'] == 'show_post' && isset($_GET['post_id']) && strlen(trim($_GET['post_id'])) > 0 ){
				if ( !preg_match('/[^0-9]+/',$_GET['post_id']) ){
					$sql_titlepost = 'SELECT post_title FROM web_posts WHERE post_id = ' . $_GET['post_id'] . ' AND post_published != 0 LIMIT 1';
					$res_titlepost = exeQuery($sql_titlepost);
					if ( mysql_num_rows($res_titlepost) == 1 ){
						$titlepost = mysql_fetch_array($res_titlepost);
						echo $titlepost['post_title'];
					}
					else{
						echo '&lt;Post whitout title&gt;';
					}
					echo ' :: ';
				}
			}
			echo SITE_NAME;
		echo '</title>';
		
		echo '<link rel="alternate" media="screen" type="application/rss+xml" href="http://' . $_SERVER['SERVER_NAME'] . INDEX . '../feed/" title="RSS 2.0 - All" />';
		echo '<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />';
		echo '<link rel="stylesheet" type="text/css" href="css/sexylightbox.css" media="all" />';
		echo '<link rel="shortcut icon" href="images/ico/favicon.ico" />';
		echo '<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>';
		echo '<script type="text/javascript" src="js/jquery.qtip-1.0.0-rc3.js"></script>';
		echo '<script type="text/javascript" src="js/jquery.ddaccordion.js"></script>';
		echo '<script type="text/javascript" src="js/jquery.innerfade.js"></script>';
		echo '<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>';
		echo '<script type="text/javascript" src="js/sexylightbox.v2.3.jquery.min.js"></script>';
		echo '<script type="text/javascript" src="js/jquery.globaleffects.js"></script>';
		echo '<script type="text/javascript" src="../data/highlighter/scripts/shCore.js"></script>';
		echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushBash.js"></script>';
		echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushCpp.js"></script>';
		echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushCSharp.js"></script>';
		echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushCss.js"></script>';
		echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushDelphi.js"></script>';
		echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushDiff.js"></script>';
		echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushGroovy.js"></script>';
		echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushJava.js"></script>';
		echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushJScript.js"></script>';
		echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushPhp.js"></script>';
		echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushPlain.js"></script>';
		echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushPython.js"></script>';
		echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushRuby.js"></script>';
		echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushScala.js"></script>';
		echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushSql.js"></script>';
		echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushVb.js"></script>';
		echo '<script type="text/javascript" src="../data/highlighter/scripts/shBrushXml.js"></script>';
		echo '<script type="text/javascript">';
			echo 'SyntaxHighlighter.config.clipboardSwf = \'../data/highlighter/scripts/clipboard.swf\';';
			echo 'SyntaxHighlighter.defaults[\'toolbar\'] = false;';
			echo 'SyntaxHighlighter.all();';
		echo '</script>';
		echo '</head>';
		echo '<body>';
		if ( !isset($_SESSION['session_user_id']) && !isset($_SESSION['session_user_name']) ){
			// Validating the login form
			if ( $_POST['login_usersubmit'] == 'true' ){
				echo '<div class="advertices" align="center"><span class="inner-adv">';
				if ( strlen(trim($_POST['login_username'])) <= 0 || strlen(trim($_POST['login_userpass'])) <= 0 )
					echo '<div class="messages box-error">Usuario o Contrase&ntilde;a invalidos.</div>';
				else{
					if ( strlen(trim($_POST['login_username'])) > 0 && strlen(trim($_POST['login_userpass'])) > 0 ){
						
						$sql_loginuser = 'SELECT user_id,user_name,user_admin,user_admin_type FROM web_users WHERE user_name="' . $_POST['login_username'] . '" AND user_passwd="' . md5($_POST['login_userpass']) . '" LIMIT 1;';
						$res_loginuser = exeQuery($sql_loginuser);
						if ( mysql_num_rows($res_loginuser) == 1 ){
							$loginuser = mysql_fetch_array($res_loginuser);
							$_SESSION['session_user_id']=			$loginuser['user_id'];
							$_SESSION['session_user_name']=		$loginuser['user_name'];
							$_SESSION['session_user_admin']=		$loginuser['user_admin'];
							$_SESSION['session_user_admin_type']=	$loginuser['user_admin_type'];
							echo '<div class="messages box-success">Espera un momento por favor&#133;</div>';
							echo '<script type="text/javascript">window.top.location.href="admin.php";</script>';
						}
						else
							echo '<div class="messages box-error">Usuario o Contrase&ntilde;a invalidos.</div>';
						
					}
				}
				echo '</span></div>';
			}
			// End: Validating the login form
			
			echo '<div id="content-loginform" class="content-login"><table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%"><tr><td valign="middle" align="center">';
				echo '<span class="logincontent"><span class="inner20">';
					echo '<form method="post" action="' . $_SERVER['REQUEST_URI'] . '" class="loginform" id="logininputs">';
						echo '<table cellpadding="0" cellspacing="0" border="0">';
							echo '<tr>';
								echo '<td>';
									echo 'Usuario';
								echo '</td>';
								echo '<td>';
									echo '<span class="left-input"><span class="right-input"><input type="text" name="login_username" class="middle-input loginusername" /></span></span>';
								echo '</td>';
							echo '</tr>';
							
							echo '<tr>';
								echo '<td>';
									echo 'Passwd';
								echo '</td>';
								echo '<td>';
									echo '<span class="left-input"><span class="right-input"><input type="password" name="login_userpass" class="middle-input loginuserpass" /></span></span>';
								echo '</td>';
							echo '</tr>';
							
							echo '<tr>';
								echo '<td colspan="2" align="right">';
									echo '<button type="submit" name="login_usersubmit" value="true" class="login_button"><span class="left-button"><span class="right-button">Iniciar sesi&oacute;n</span></span></button>';
									echo '<button type="reset" class="close-loginform"><span class="left-button"><span class="right-button">Cancelar</span></span></button>';
								echo '</td>';
							echo '</tr>';
							
						echo '</table>';
					echo '</form>';
				echo '</span></span>';
			echo '</td></tr></table></div>';
		}
		
		
		
		/**
		*
		*	Hacemos la llamada a la funcion que muestra las advertencias dentro
		*	del marco del navegador, siempre que existan las variables(enviadas
		*	por metodo GET): advflag(bool), advtype(string), advoption(opcion de variables),
		*	advres(resultado de la ejecuci�n)
		*/
		
		// admin_adverticement();
		
		// Fin de la funcion de advertencias y avisos
		
		


		/**
		* 
		* Cerrado por reparaciones
		* 
		* Aviso de cerrado del sitio
		*/
//		echo '<span class="closed-for-repairs" tabindex="1"><table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%"><tr><td height="100%" width="100%" valign="middle" align="middle">';

//			echo '<span class="repairs-container">';

//				echo '<h2>Cerrado por reparaciones</h2>';
//
		//		echo 'Por el momento el sitio se encuentra en remodelaci�n, cuando volvamos tendremos nuevas cosas para ustedes.';

				/*
				echo '<span class="socialmedia">';
					echo '<a href="javascript: void(0);" class="icon facebook"></a>';
					echo '<a href="javascript: void(0);" class="icon twitter"></a>';
				echo '</span>';
				*/

		//	echo '</span>';

		//echo '</td></tr></table></span>';
		// end: closed-for-repairs



		
		
		echo '<div align="center">';
	
			// Top banner
			echo '<div id="topbanner"><span class="inbanner">';
				echo '<span class="useropt">';
				
					// User options
					
					if ( isset($_SESSION['session_user_id']) && isset($_SESSION['session_user_name']) ){
						echo '<a href="javascript:void(0);" class="user no-bold">' . userid_completename($_SESSION['session_user_id']) . '</a> &nbsp; &nbsp; &#124; ';
						echo '<a href="' . INDEX . 'admin.php?option=redirect&optvar=Administration+Tools" class="rss">Admin site</a> &#124;';
						echo '<a href="logout.php?redirect=' . urlencode($_SERVER['REQUEST_URI']) . '" class="logout">Logout</a>';
					}
					else{
						echo 'Bienvenido <b>Invitado</b> &nbsp; &nbsp; &#124; ';
						// 	- login button
						echo '<a href="javascript:void(0);" class="user open-loginform">Login</a> &#124;';
						//	- Feed RSS button
						echo '<a href="' . INDEX . '../feed/" class="rss">Feed RSS</a>';
					}
					
					// Search form
					echo '<form method="get" action="' . INDEX . '" class="search">';
						// Hidden field for option page
						echo '<input type="hidden" name="option" value="search" />';
						
						// Border class
						echo '<span class="input-search"><span>';
							echo '<input type="hidden" name="call" value="search" />';
							echo '<input type="text" name="swords" class="typeinput" ';
								if (isset($_GET['option']) && $_GET['option'] == 'search' && isset($_GET['swords']) && strlen(trim($_GET['swords'])) > 0 )
									echo 'value="' . $_GET['swords'] . '"';
							echo ' />';
							echo '<button type="submit" name="searching" value="true"><img src="images/ico/search.png" /></button>';
						echo '</span></span>';
					echo '</form>';
					// End: Search form
				echo '</span>';
				
				// Main menu
				echo '<ul class="topmenu">';
					echo '<li style="margin-left: 0px;"><a href="' . INDEX . '" class="graphite blog"></a></li>';
					echo '<li><a href="' . INDEX . '?option=gallery" class="graphite gallery"></a></li>';
					echo '<li><a href="' . INDEX . '?option=portfolio" class="graphite portfolio"></a></li>';
					echo '<li style="background: none;"><a href="' . INDEX . '?option=contact" class="graphite contact"></a></li>';
				echo '</ul>';
				// End: Main menu
			echo '</span></div>';
			// End: Top banner
			
			echo '<div id="banner"><span class="bnrcontent">';
				echo '<div id="fade-top"></div>';
					echo '<div id="mainbanner"><span class="main-bannercontent">';
						echo '<span class="logotype"></span>';
						echo '<span class="pagenatio ';
							switch($_GET['option']){
								case 'search':
									echo 'search';
									break;
								case 'contact':
									echo 'contact';
									break;
								case 'portfolio':
									echo 'portfolio';
									break;
								case 'gallery':
									echo 'gallery';
									break;
								case 'index':
								default:
									echo 'blog';
							}
						echo '"></span>';
						echo '<span class="break"></span>';
						
						
						include_once 'rssfeed/magpierss-0.61/rss_fetch.inc';
						$url = 'http://twitter.com/statuses/user_timeline/42882253.rss';
						$rss = fetch_rss($url);
						
						echo '<span class="twitter">';
							echo '<span class="tooltip">';
								echo '<table cellpadding="0" cellspacing="0" border="0">';
									echo '<tr>';
										echo '<td><img src="images/bdr/bdr.twitter.topleft.png" height="5" width="5" border="0" /></td>';
										echo '<td style="height: 5px; background-image:url(images/bdr/bdr.twitter.centermiddle.png); background-position: 0px 0px; background-repeat: repeat-x;" height="5"></td>';
										echo '<td><img src="images/bdr/bdr.twitter.topright.png" height="5" width="5" border="0" /></td>';
									echo '</tr>';
									echo '<tr>';
										echo '<td style="width: 5px; background-image:url(images/bdr/bdr.twitter.centermiddle.png); background-position: 0px 0px; background-repeat: repeat-y;" width="5"></td>';
										echo '<td style="width: 5px; background-image:url(images/bdr/bdr.twitter.centermiddle.png); background-position: 0px 0px; background-repeat: repeat;">';
											echo '<span class="feedtwitter"><span class="textantialicing">';
												//echo 'Ha ocurrido un error con el acceso a twitter, por favor intentelo m&aacute;s tarde.';
												echo '<b>' . $rss->channel['title'] . '</b>';
												echo '<span class="twitteritem"><ul id="twitterslide">';
												foreach ($rss->items as $item ) {
														echo '<li>';
															echo '<span class="tweetitle">';
															echo '<a href="' . $item['link'] . '" target="_blank">&#64;' . substr($item['title'],0,strpos($item['title'],':')) . ':</a>';
																echo '<span class="tweet_content">';
																	echo utf8_decode(clean_tweet(substr($item['title'],strpos($item['title'],':')+1)));
																echo '</span>';
															echo '</span>';
															echo '<span class="pubdate">' . str_replace($arraydateen,$arraydatees,date('l d  \d\e F \d\e Y &#64; h:i a',strtotime($item['pubdate'])));
														echo '</li>';
												}
												echo '</span>';
											echo '</ul></span>';
										echo '</td>';
										echo '<td style="width: 5px; background-image:url(images/bdr/bdr.twitter.centermiddle.png); background-position: 0px 0px; background-repeat: repeat-y;" width="5"></td>';
									echo '</tr>';
									echo '<tr>';
										echo '<td><img src="images/bdr/bdr.twitter.bottomleft.png" height="5" width="5" border="0" /></td>';
										echo '<td style="height: 5px; background-image:url(images/bdr/bdr.twitter.centermiddle.png); background-position: 0px 0px; background-repeat: repeat-x;" height="5"></td>';
										echo '<td><img src="images/bdr/bdr.twitter.bottomright.png" height="5" width="5" border="0" /></td>';
									echo '</tr>';
								echo '</table>';
							echo '</span>';
						echo '</span>';
						
												
					echo '</span></div>';
				echo '<div id="fade-bottom"></div>';
			echo '</span></div>';
			
			echo '<div id="pagecontent"><span class="inner20">';
				
				switch($_GET['option']){
					case 'contact':
						include_once 'content/contact/contact.index.php';
						break;
					case 'portfolio':
						include_once 'content/portfolio/portfolio.index.php';
						break;
					case 'gallery':
						include_once 'content/gallery/gallery.index.php';
						break;
					case 'index':
					default:
						include_once 'content/blog/blog.index.php';
				}
				
			echo '</span></div>';
			
			echo '<div id="footerContent">';
				echo '<div id="fade-top"></div>';
				echo '<div id="footer"><span class="delimiter">';
					echo '<span class="patrocinio"><span class="middleAdv">';
						echo '<a href="http://000webhost.com" class="patItem webhost" target="_blank"></a>';
						echo '<a href="http://mediafire.com" class="patItem mediafire" target="_blank"></a>';
						echo '<a href="http://jquery.com" class="patItem jquery" target="_blank"></a>';
						echo '<a href="http://mootools.net" class="patItem mootools" target="_blank"></a>';
						//echo '<br />Arkos Noem Arenom &copy; 2010 &#124; LEAF Proffesionals&trade;';
					echo '</span></span>';
					
					echo '<span class="newsLetter">';
						
						echo 'Arkos Noem Arenom, el logo Arkos Noem Arenom, LEAF Professionals y el logo LEAF Professionals, son marcas registradas de Jorge Alberto Jaime en M&eacute;xico y otros paises.';
						
					echo '</span>';
					
				echo '</span></div>';
			echo '</div>';
			echo '<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push([\'_setAccount\', \'UA-19519047-1\']);
  _gaq.push([\'_trackPageview\']);

  (function() {
    var ga = document.createElement(\'script\'); ga.type = \'text/javascript\'; ga.async = true;
    ga.src = (\'https:\' == document.location.protocol ? \'https://ssl\' : \'http://www\') + \'.google-analytics.com/ga.js\';
    var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>';
		echo '</div></body>';
		echo '</html>';
		// ob_end_flush();
	
		mysql_close($idcnx_site);
	}
	else
		echo 'Conection no stablished';
	
/*<!--
	- Tipo de letra
		* 10 Lucida Sans Unicode (Normal|Bold)
		* 16|20|24 Georgia (Normal|Bold)
		* Aller (Bold|Light)
	
	- Conjunto de iconos
		* Ekisho
		* Google
		* icon_5010
		* icon-set
		* Knob_Buttons_Toolbar_icons_by_iTweek
		* Lovely_website_icons_pack_1_by_LazyCrazy
		* Lovely_website_icons_pack_2_by_LazyCrazy
		* Macs_Archigraphs_PNG
		* Maji_ico
		* Mashup
		* Newsfire
		* Newspaper_icon_by_cemagraphics
		* must_have_icon_set
		* Phuzion_Icon_Pack
		* PNG
		* rss icons
		* Quartz Icon Pack
		* c9-d.com_rinoa_icon_set1
		* crystal_projects
		* device icon set
		* devicons
		* drf
		* volcano
		* Windows 7 Beta Icon Pack
		* Windows_Seven_icon_pack_by_ziomekorko
		* woofunction-icons
		* xedia-icons
	
	- Botones
		* button-download.zip
-->*/
?>
