<?php
	
	$today = date('Y-m-d H:i:s');
	
	echo date('l d F, Y &#64; h:m:s a', (strtotime($today) - 10800));
	
?>
