<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Error 404: Documento no encontrado</title>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$('#error *').fadeTo(0,0);
				setTimeout(function(){
					$('#error *').fadeTo(1000,1);
				},300);
				
			});
		</script>
		<style type="text/css">
		body{
			font-family: "Trebuchet MS", Arial, Sans-serif;
			font-size: 11px;
			color: #606060;
			background-color: #fff;
			margin: 40px;
		}
		#error{
			display: block;
			background-color: #eaeaea;
			border: 1px solid #ed1c24;
			padding: 20px;
			font-style: italic;
			-webkit-border-radius: 5px;
			-moz-border-radius: 5px;
		}
		#error span.title{
			display: block;
			font-size: 20px;
			font-weight: bold;
		}
		</style>
	</head>
	<body>
		<div id="error">
			<span class="title">Error 404</span>
			<span>Documento no encotrado</span>
		</div>
	</body>
</html>